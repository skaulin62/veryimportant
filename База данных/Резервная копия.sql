USE [master]
GO
/****** Object:  Database [Клининг]    Script Date: 06.06.2022 18:33:50 ******/
Create database Клининг
-- Переводим БД в single-user mode
ALTER DATABASE Клининг
SET SINGLE_USER WITH ROLLBACK IMMEDIATE
GO
-- Меняем кодировку COLLATION
ALTER DATABASE Клининг
COLLATE Cyrillic_General_CI_AS 
GO
-- Переводим БД обратно в multi-user mode
ALTER DATABASE Клининг
SET MULTI_USER WITH ROLLBACK IMMEDIATE
GO  
ALTER DATABASE [Клининг] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Клининг] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Клининг] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Клининг] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Клининг] SET ARITHABORT OFF 
GO
ALTER DATABASE [Клининг] SET AUTO_CLOSE ON 
GO
ALTER DATABASE [Клининг] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Клининг] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Клининг] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Клининг] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Клининг] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Клининг] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Клининг] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Клининг] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Клининг] SET  ENABLE_BROKER 
GO
ALTER DATABASE [Клининг] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Клининг] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Клининг] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Клининг] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Клининг] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Клининг] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Клининг] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Клининг] SET RECOVERY FULL 
GO
ALTER DATABASE [Клининг] SET  MULTI_USER 
GO
ALTER DATABASE [Клининг] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Клининг] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Клининг] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Клининг] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [Клининг] SET DELAYED_DURABILITY = DISABLED 
GO
USE [Клининг]
GO
/****** Object:  Table [dbo].[Заказы]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Заказы](
	[Номер_заказа] [int] NOT NULL,
	[Код_клиента] [int] NULL,
	[Код_уборщика] [int] NULL,
	[Код_оператора] [int] NULL,
	[Код_услуги] [int] NULL,
	[Общая_стоимость] [money] NOT NULL,
	[Количество] [int] NOT NULL,
	[Дата_подтверждения_уборки] [datetime] NULL,
	[Дата_оформления_заказа] [datetime] NULL,
	[Качество_уборки] [int] NULL,
	[Дружелюбность_персонала] [int] NULL,
	[Примечание] [varchar](600) NULL,
	[Дата_уборки] [datetime] NULL,
	[Отзыв] [varchar](500) NULL,
	[Статус] [varchar](100) NULL,
PRIMARY KEY CLUSTERED 
(
	[Номер_заказа] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Категории_услуг]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Категории_услуг](
	[Код_категории] [int] IDENTITY(1,1) NOT NULL,
	[Наименование] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Код_категории] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Клиенты]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Клиенты](
	[Код_клиента] [int] IDENTITY(1,1) NOT NULL,
	[Фамилия] [varchar](255) NOT NULL,
	[Имя] [varchar](255) NOT NULL,
	[Отчество] [varchar](255) NULL,
	[Паспортные_данные] [varchar](10) NOT NULL,
	[Номер_телефона] [varchar](11) NOT NULL,
	[Дата_рождения] [date] NOT NULL,
	[Логин] [varchar](32) NULL,
	[Пароль] [varchar](64) NOT NULL,
	[Дата_регистрации] [date] NULL,
	[Адрес] [varchar](500) NULL,
PRIMARY KEY CLUSTERED 
(
	[Код_клиента] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Роли]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Роли](
	[Код_роли] [int] IDENTITY(1,1) NOT NULL,
	[Наименование] [varchar](255) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[Код_роли] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Сотрудники]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Сотрудники](
	[Код_сотрудника] [int] IDENTITY(1,1) NOT NULL,
	[Фамилия] [varchar](255) NOT NULL,
	[Имя] [varchar](255) NOT NULL,
	[Отчество] [varchar](255) NULL,
	[Паспортные_данные] [varchar](10) NOT NULL,
	[Номер_телефона] [varchar](11) NOT NULL,
	[Дата_рождения] [date] NOT NULL,
	[Логин] [varchar](32) NOT NULL,
	[Пароль] [varchar](64) NOT NULL,
	[Дата_регистрации] [date] NULL,
	[Удален] [bit] NULL,
	[Код_роли] [int] NULL,
PRIMARY KEY CLUSTERED 
(
	[Код_сотрудника] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Услуги]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Услуги](
	[Код_услуги] [int] IDENTITY(1,1) NOT NULL,
	[Наименование] [varchar](255) NOT NULL,
	[Описание] [varchar](1000) NOT NULL,
	[Стоимость] [money] NOT NULL,
	[Дата_добавление_услуги] [date] NULL,
	[Код_категории] [int] NULL,
	[Изображение] [varchar](255) NULL,
PRIMARY KEY CLUSTERED 
(
	[Код_услуги] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  View [dbo].[currentOrder]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[currentOrder]
([order], [client], cleaner, phone, [service], cost, [count], [datetime], [status])
as
select Номер_заказа, Клиенты.Код_клиента, (Сотрудники.Фамилия + ' ' + Сотрудники.Имя), Сотрудники.Номер_телефона, 
Услуги.Наименование, Общая_стоимость, Количество, Дата_уборки, Статус
from Заказы join Клиенты on Заказы.Код_клиента=Клиенты.Код_клиента
join Сотрудники on Заказы.Код_уборщика=Сотрудники.Код_сотрудника
join Услуги on Заказы.Код_услуги=Услуги.Код_услуги
GO
/****** Object:  View [dbo].[diagramm]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create view [dbo].[diagramm]
([service], [count])
as
select Услуги.Наименование, count(Номер_заказа)
from Заказы join Услуги on Заказы.Код_услуги=Услуги.Код_услуги
group by Услуги.Наименование
GO
/****** Object:  View [dbo].[EmployeeRole]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[EmployeeRole]
(id, surname, [name], lastname, [login], [role], [delete])
as
select Сотрудники.Код_сотрудника, Сотрудники.Фамилия, Сотрудники.Имя, Сотрудники.Отчество, Сотрудники.Логин, Роли.Наименование, Сотрудники.Удален
from Сотрудники join Роли on Сотрудники.Код_роли=Роли.Код_роли
where Роли.Наименование = 'Уборщик' or Роли.Наименование = 'Оператор'
GO
/****** Object:  View [dbo].[orders]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[orders]
(id_order, id_cleaner, client, cleaner, operator, [service], cost, [count], date_confirm_service, date_reg_order, quality_service, quality_personal, [description], date_service, comment)
as
select Номер_заказа, Уборщики.Код_сотрудника, (Клиенты.Фамилия + ' ' + Клиенты.Имя), (Уборщики.Фамилия + ' ' + Уборщики.Имя), (Операторы.Фамилия + ' ' + Операторы.Имя),
Услуги.Наименование, Заказы.Общая_стоимость, Заказы.Количество, Дата_подтверждения_уборки, Дата_оформления_заказа, Качество_уборки, Дружелюбность_персонала, Примечание, Дата_уборки, Отзыв
from Сотрудники as Уборщики join Заказы on Уборщики.Код_сотрудника=Заказы.Код_уборщика
join Сотрудники as Операторы on Операторы.Код_сотрудника=Заказы.Код_оператора
join Клиенты on Клиенты.Код_клиента=Заказы.Код_клиента
join Услуги on Услуги.Код_услуги=Заказы.Код_услуги
GO
/****** Object:  View [dbo].[orders_decline]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE view [dbo].[orders_decline]
(id_order, id_client, client, [service], cost, [count], date_confirm, date_reg_order, quality_service, quality_personal, [description], date_service, comment, [status], phone, [address], id_cleaner)
as
select Номер_заказа, Клиенты.Код_клиента, (Клиенты.Фамилия + ' ' + Клиенты.Имя), Услуги.Наименование, Общая_стоимость, Количество, Дата_подтверждения_уборки, Дата_оформления_заказа, Качество_уборки, Дружелюбность_персонала, Примечание, Дата_уборки, Отзыв, Статус, Клиенты.Номер_телефона, Клиенты.Адрес, Заказы.Код_уборщика
from Заказы join Клиенты on Заказы.Код_клиента=Клиенты.Код_клиента
join Услуги on Заказы.Код_услуги=Услуги.Код_услуги
GO
/****** Object:  View [dbo].[report]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[report]
([Номер заказа], Клиент, Уборщик, Оператор, Услуга, [Общая стоимость], [Количество кв. м.], [Дата подтверждения],[Дата оформления], [Качество уборки], [Дружелюбность персонала], Примечание, [Дата уборки], Отзыв, Статус)
as
select Номер_заказа, (Клиенты.Фамилия + ' ' + Клиенты.Имя), (Уборщики.Фамилия + ' ' + Уборщики.Имя), (Операторы.Фамилия + ' ' + Операторы.Имя), Услуги.Наименование,
Convert(int,Заказы.Общая_стоимость), Заказы.Количество, Дата_подтверждения_уборки, Дата_оформления_заказа, Качество_уборки, Дружелюбность_персонала, Примечание, Дата_уборки, Отзыв, Статус
from Сотрудники as Уборщики join Заказы on Уборщики.Код_сотрудника=Заказы.Код_уборщика
join Сотрудники as Операторы on Операторы.Код_сотрудника=Заказы.Код_оператора
join Клиенты on Клиенты.Код_клиента=Заказы.Код_клиента
join Услуги on Услуги.Код_услуги=Заказы.Код_услуги
where Статус = 'Выполнен'
GO
/****** Object:  View [dbo].[ServicesCategories]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[ServicesCategories]
(id, [service], [description], cost,[image], category)
as
select Услуги.Код_услуги, Услуги.Наименование, Услуги.Описание, Услуги.Стоимость,Услуги.Изображение, Категории_услуг.Наименование
from Услуги join Категории_услуг on Услуги.Код_категории=Категории_услуг.Код_категории
GO
/****** Object:  View [dbo].[UnionAccounts]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create view [dbo].[UnionAccounts]
(id, surname, [name], lastname,passport, phone, birthday, [login], [password], [role], [delete])
as 
select Код_клиента, Фамилия, Имя, Отчество, Паспортные_данные,  Номер_телефона, Дата_рождения, Номер_телефона, Пароль, 'Клиент', 0
from Клиенты
union 
Select Код_сотрудника, Фамилия, Имя, Отчество, Паспортные_данные, Номер_телефона, Дата_рождения, Логин, Пароль, Роли.Наименование, Удален
from Сотрудники join Роли on Сотрудники.Код_роли=Роли.Код_роли
GO
INSERT [dbo].[Заказы] ([Номер_заказа], [Код_клиента], [Код_уборщика], [Код_оператора], [Код_услуги], [Общая_стоимость], [Количество], [Дата_подтверждения_уборки], [Дата_оформления_заказа], [Качество_уборки], [Дружелюбность_персонала], [Примечание], [Дата_уборки], [Отзыв], [Статус]) VALUES (264416213, 3, 1, 4, 17, 1000.0000, 10, CAST(N'2022-05-27T10:05:16.000' AS DateTime), CAST(N'2022-05-26T19:22:04.930' AS DateTime), 2, 5, N'нет', CAST(N'2022-05-27T10:00:00.000' AS DateTime), N'Отлично', N'Выполнен')
GO
INSERT [dbo].[Заказы] ([Номер_заказа], [Код_клиента], [Код_уборщика], [Код_оператора], [Код_услуги], [Общая_стоимость], [Количество], [Дата_подтверждения_уборки], [Дата_оформления_заказа], [Качество_уборки], [Дружелюбность_персонала], [Примечание], [Дата_уборки], [Отзыв], [Статус]) VALUES (289879405, 3, 3, 4, 17, 2000.0000, 20, CAST(N'2022-05-29T18:05:00.000' AS DateTime), CAST(N'2022-05-28T13:47:59.907' AS DateTime), 1, 1, N'нет', CAST(N'2022-05-29T18:00:00.000' AS DateTime), NULL, N'Выполнен')
GO
SET IDENTITY_INSERT [dbo].[Категории_услуг] ON 
GO
INSERT [dbo].[Категории_услуг] ([Код_категории], [Наименование]) VALUES (1, N'Уборка')
GO
INSERT [dbo].[Категории_услуг] ([Код_категории], [Наименование]) VALUES (2, N'Мойка окон')
GO
INSERT [dbo].[Категории_услуг] ([Код_категории], [Наименование]) VALUES (3, N'Химчистка')
GO
INSERT [dbo].[Категории_услуг] ([Код_категории], [Наименование]) VALUES (4, N'Дезинфекция')
GO
SET IDENTITY_INSERT [dbo].[Категории_услуг] OFF
GO
SET IDENTITY_INSERT [dbo].[Клиенты] ON 
GO
INSERT [dbo].[Клиенты] ([Код_клиента], [Фамилия], [Имя], [Отчество], [Паспортные_данные], [Номер_телефона], [Дата_рождения], [Логин], [Пароль], [Дата_регистрации], [Адрес]) VALUES (3, N'Шарапов', N'Дмитрий ', N'Евгеньевич', N'1231231231', N'79088065984', CAST(N'1994-05-02' AS Date), NULL, N'76f2533763bc67e1263bfe99c17520da', CAST(N'2022-02-19' AS Date), N'Ул. Демьяна Бедного, д. 91, кв. 2')
GO
SET IDENTITY_INSERT [dbo].[Клиенты] OFF
GO
SET IDENTITY_INSERT [dbo].[Роли] ON 
GO
INSERT [dbo].[Роли] ([Код_роли], [Наименование]) VALUES (1, N'Директор')
GO
INSERT [dbo].[Роли] ([Код_роли], [Наименование]) VALUES (2, N'Администратор')
GO
INSERT [dbo].[Роли] ([Код_роли], [Наименование]) VALUES (3, N'Уборщик')
GO
INSERT [dbo].[Роли] ([Код_роли], [Наименование]) VALUES (4, N'Оператор')
GO
SET IDENTITY_INSERT [dbo].[Роли] OFF
GO
SET IDENTITY_INSERT [dbo].[Сотрудники] ON 
GO
INSERT [dbo].[Сотрудники] ([Код_сотрудника], [Фамилия], [Имя], [Отчество], [Паспортные_данные], [Номер_телефона], [Дата_рождения], [Логин], [Пароль], [Дата_регистрации], [Удален], [Код_роли]) VALUES (1, N'Попов', N'Моисей', N'Проклович', N'8209231234', N'89088065979', CAST(N'2000-02-20' AS Date), N'popov5123', N'834936c07c6089e80594769e5d429145', CAST(N'2022-02-20' AS Date), 0, 3)
GO
INSERT [dbo].[Сотрудники] ([Код_сотрудника], [Фамилия], [Имя], [Отчество], [Паспортные_данные], [Номер_телефона], [Дата_рождения], [Логин], [Пароль], [Дата_регистрации], [Удален], [Код_роли]) VALUES (2, N'Александров', N'Трофим', N'Фролович', N'1024812894', N'89088065980', CAST(N'1992-12-09' AS Date), N'alex_trofim124', N'b1d49d4ddae7e25dc608add30c92de36', CAST(N'2022-02-20' AS Date), 0, 2)
GO
INSERT [dbo].[Сотрудники] ([Код_сотрудника], [Фамилия], [Имя], [Отчество], [Паспортные_данные], [Номер_телефона], [Дата_рождения], [Логин], [Пароль], [Дата_регистрации], [Удален], [Код_роли]) VALUES (3, N'Гаврилов', N'Пантелей', N'Георгиевич', N'6218923273', N'89088065981', CAST(N'2000-09-25' AS Date), N'Gavrilov_45', N'd0ef15e9f0bd20741b5c68f32c50d0a1', CAST(N'2022-02-20' AS Date), 0, 3)
GO
INSERT [dbo].[Сотрудники] ([Код_сотрудника], [Фамилия], [Имя], [Отчество], [Паспортные_данные], [Номер_телефона], [Дата_рождения], [Логин], [Пароль], [Дата_регистрации], [Удален], [Код_роли]) VALUES (4, N'Хохлов', N'Модест', N'Тихонович', N'1942842847', N'89088064982', CAST(N'1987-08-23' AS Date), N'xoxol_modest60', N'f1d2413f2fb155941137346eb17804a8', CAST(N'2022-02-20' AS Date), 0, 4)
GO
SET IDENTITY_INSERT [dbo].[Сотрудники] OFF
GO
SET IDENTITY_INSERT [dbo].[Услуги] ON 
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (17, N'Генеральная уборка', N'В уборку входит: обеспыливание стен, потолков по периметру; чистка батарей, радиаторов, конвекторов; протирка поверхностей мебели; удаление пыли со всех предметов интерьера и люстры; сухая чистка мягкой мебели пылесосом; сухая уборка пылесосом ковров, ковровых покрытий и других напольных покрытий; обезжиривание всех поверхностей в кухне; дезинфекция и очистка всех поверхностей в сан. узлах; полировка зеркал и зеркальных поверхностей; мойка пола и плинтусов с отодвиганием легких предметов интерьера; мойка дверей и дверных проемов).', 100.0000, CAST(N'2022-02-10' AS Date), 1, N'services\service001.jpg')
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (18, N'Уборка квартиры после ремонта', N'В уборку входит: обеспыливание потолка и стен; обеспыливание осветительных приборов*; мойка мебели снаружи и внутри; чистка радиаторов; удаление загрязнений от строительных смесей; мойка дверей, пола, плинтусов.', 140.0000, CAST(N'2022-02-10' AS Date), 1, N'services/service002.jpg')
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (19, N'Регулярная уборка квартиры', N'В уборку входит: влажная уборка открытых горизонтальных поверхностей; полировка зеркал; сухая чистка мебели и ковров; дезинфекция сантехники; мойка пола, плинтусов.', 65.0000, CAST(N'2022-02-10' AS Date), 1, N'services/service003.jpg')
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (20, N'Уборка туалета и ванной комнаты', N'В уборку входит: чистка и дезинфекция стен и потолка; чистка сантехники; чистка хромированных изделий; полировка зеркал; мойка мебели; мойка двери, пола, плинтусов;', 500.0000, CAST(N'2022-02-10' AS Date), 1, N'services/service004.jpg')
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (21, N'Уборка кухни', N'В уборку входит: обеспыливание стен, потолков по периметру; чистка радиаторов; мойка раковины, смесителя, посуды (одна раковина); мытье/обезжиривание кухонного фартука, вытяжки снаружи; мойка всех столов, мебели,мойка кухонной техники: кухонной плиты, внешних поверхностей бытовой техники, фасадов кухонных шкафов внутри и снаружи; мытье пола и плинтусов (отодвинем не тяжелые предметы интерьера); мытье дверей и дверных проемов.', 200.0000, CAST(N'2022-02-10' AS Date), 1, N'services/service005.jpg')
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (22, N'Очистка бассейна, бани, сауны', N'Загрязнение домашнего бассейна довольно распространённая проблема', 150.0000, CAST(N'2022-02-10' AS Date), 1, N'services/service007.jpg')
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (23, N'Уборка детской', N'Уборка детской комнаты должна проводиться качественно и регулярно — это залог здоровья маленького человека, который в ней проводит большую часть своего времени.', 100.0000, CAST(N'2022-02-10' AS Date), 1, N'services/service008.jpg')
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (24, N'Уборка после праздников', N'Назвали гостей в дом — готовьтесь к уборке после праздника. Мы всегда готовы прийти на помощь и взять огонь постпраздничной уборки на себя!', 100.0000, CAST(N'2022-02-10' AS Date), 1, N'services/service009.jpg')
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (25, N'Мытье окон', N'Избавим вас от бесконечных разводов на стеклах, потраченного времени и рисков. Гарантируем оперативное и качественное выполнение работ на любой высоте.', 100.0000, CAST(N'2022-02-10' AS Date), 2, N'services/service010.jpg')
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (26, N'Мойка балконов и лоджий', N'Ввиду особенностей размещения лоджий и балконов выполнить внешний клининг сложно и небезопасно. Мы используем специальные инструменты, которые позволяют добираться до самых отдаленных участков.', 100.0000, CAST(N'2022-02-10' AS Date), 2, N'services/service011.jpg')
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (27, N'Химчистка мебели и ковров', N'После чистки мягкая мебель будет снова радовать вас чистотой и свежестью.', 150.0000, CAST(N'2022-02-10' AS Date), 3, N'services/service012.jpg')
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (28, N'Химчистка ковров', N'Химчистка ковров с длинным и коротким ворсом, химчистка ковролина.', 175.0000, CAST(N'2022-02-10' AS Date), 3, N'services/service013.jpg')
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (29, N'Чистка штор', N'Чистка штор поможет сохранить одному из главных украшений интерьера первозданный лоск и красоту.', 70.0000, CAST(N'2022-02-10' AS Date), 3, N'services/service014.jpg')
GO
INSERT [dbo].[Услуги] ([Код_услуги], [Наименование], [Описание], [Стоимость], [Дата_добавление_услуги], [Код_категории], [Изображение]) VALUES (35, N'Дезинфекция квартир', N'Самая популярная услуга по дезинфицированию жилого помещения – так называемая заключительная дезинфекция. Она проводится однократно после того, как квартиру покинул человек, болевший заразным заболеванием – например, открытой формой туберкулеза. Только специалисты с профессиональными средствами способны обезопасить жильцов квартиры.', 50.0000, CAST(N'2022-02-10' AS Date), 4, N'services/service015.jpg
')
GO
SET IDENTITY_INSERT [dbo].[Услуги] OFF
GO
ALTER TABLE [dbo].[Заказы] ADD  CONSTRAINT [DF_Заказы_Дата_оформления_заказа]  DEFAULT (getdate()) FOR [Дата_оформления_заказа]
GO
ALTER TABLE [dbo].[Клиенты] ADD  DEFAULT (CONVERT([date],getdate())) FOR [Дата_регистрации]
GO
ALTER TABLE [dbo].[Сотрудники] ADD  DEFAULT (CONVERT([date],getdate())) FOR [Дата_регистрации]
GO
ALTER TABLE [dbo].[Сотрудники] ADD  DEFAULT ((0)) FOR [Удален]
GO
ALTER TABLE [dbo].[Услуги] ADD  DEFAULT (CONVERT([date],getdate())) FOR [Дата_добавление_услуги]
GO
ALTER TABLE [dbo].[Заказы]  WITH CHECK ADD FOREIGN KEY([Код_клиента])
REFERENCES [dbo].[Клиенты] ([Код_клиента])
GO
ALTER TABLE [dbo].[Заказы]  WITH CHECK ADD FOREIGN KEY([Код_оператора])
REFERENCES [dbo].[Сотрудники] ([Код_сотрудника])
GO
ALTER TABLE [dbo].[Заказы]  WITH CHECK ADD FOREIGN KEY([Код_уборщика])
REFERENCES [dbo].[Сотрудники] ([Код_сотрудника])
GO
ALTER TABLE [dbo].[Заказы]  WITH CHECK ADD FOREIGN KEY([Код_услуги])
REFERENCES [dbo].[Услуги] ([Код_услуги])
GO
ALTER TABLE [dbo].[Сотрудники]  WITH CHECK ADD FOREIGN KEY([Код_роли])
REFERENCES [dbo].[Роли] ([Код_роли])
GO
ALTER TABLE [dbo].[Услуги]  WITH CHECK ADD FOREIGN KEY([Код_категории])
REFERENCES [dbo].[Категории_услуг] ([Код_категории])
GO
/****** Object:  StoredProcedure [dbo].[AddClient]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[AddClient]
@surname varchar(255), @name varchar(255), @lastname varchar(255), @passport varchar(10),@phone varchar(11), @birthday varchar(255), @password varchar(64), @address varchar(500)
as
insert into Клиенты(Фамилия, Имя, Отчество, Паспортные_данные, Номер_телефона, Дата_рождения, Пароль, Адрес)
values(@surname, @name, @lastname, @passport, @phone, convert(date,@birthday,105), @password, @address)
GO
/****** Object:  Trigger [dbo].[calc_cost]    Script Date: 06.06.2022 18:33:50 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create trigger [dbo].[calc_cost]
on [dbo].[Заказы]
after update
as
declare @id_order int, @count_metris int, @id_service int,@cost_service money, @total_cost money

set @id_order = (Select Номер_заказа from inserted)
set @count_metris = (select Количество from inserted)
set @id_service = (select Код_услуги from inserted)
set @cost_service = (Select Стоимость from Услуги where Код_услуги=@id_service)
set @total_cost = @count_metris * @cost_service

update Заказы
set Общая_стоимость = @total_cost
where Номер_заказа = @id_order
GO
ALTER TABLE [dbo].[Заказы] ENABLE TRIGGER [calc_cost]
GO
USE [master]
GO
ALTER DATABASE [Клининг] SET  READ_WRITE 
GO
