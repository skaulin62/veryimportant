﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Cleaning;

namespace UnitTestProject1
{
    // проверка работы ф-и валидация пароля
    [TestClass]
    public class UnitTest1
    {

        // ожидаемый результат: пароль не прошел валидацию, т.к длина строки < 8
        [TestMethod]
        public void TestMethod1()
        {
            string password = "qwerty";
            bool result = new PhoneVerificationAndValidateOtherFun().IsValidatePassword(password);
            Assert.IsFalse(result);
        }
        // результат: пароль не прошел валидацию, т.к нет мин. 1-ой цифры и 1-ой буквы верх. регистра
        [TestMethod]
        public void TestMethod2()
        {
            string password = "qwerty_qwerty";
            bool result = new PhoneVerificationAndValidateOtherFun().IsValidatePassword(password);
            Assert.IsFalse(result);
        }
        // ожидаемый результат: пароль не прошел валидацию, т.к нет мин. 1-ой буквы верх. регистра 
        [TestMethod]
        public void TestMethod3()
        {
            string password = "qwerty1234";
            bool result = new PhoneVerificationAndValidateOtherFun().IsValidatePassword(password);
            Assert.IsFalse(result);
        }
        // ожидаемый результат: пароль прошел валидацию, т.к удовлетворяет всем условиям
        [TestMethod]
        public void TestMethod4()
        {
            string password = "Qwerty1234";
            bool result = new PhoneVerificationAndValidateOtherFun().IsValidatePassword(password);
            Assert.IsTrue(result);
        }
    }
    

    // проверка работы ф-и хэширования паролей
    [TestClass]
    public class UnitTest2
    {

        // ожидаемый результат: пароль захэшировался корректно
        [TestMethod]
        public void TestMethod1()
        {
            string password = "DimaSharapov123!";
            password = PhoneVerificationAndValidateOtherFun.GetHash(password);
            // заранее подготовленная хэш-строка для данного пароля
            string result = "ef35ae51586159ab2b86f25ab01164c7"; 
            Assert.AreEqual(result, password);
        }

        // ожидаемый результат: ф-я хэширует пароль очень надеждо
        [TestMethod]
        public void TestMethod2()
        {
            string password = "Password12345";
            password = PhoneVerificationAndValidateOtherFun.GetHash(password);
            // хэш строка сгенерированная другим генератором
            string result = "d534b96c9c231037a98126891ec898eb";
            Assert.AreNotEqual(result, password);
        }


    }


    // проверка работы ф-и генерации уникального id заказа
    [TestClass]
    public class UnitTest3
    {

        // ожидаемый результат: идентификатор генерируется без повторений с макс. вероятностью в 0,00001%
        [TestMethod]
        public void TestMethod1()
        {
            string order = new PhoneVerificationAndValidateOtherFun().GenerateOrderId();
            bool resultUnique = true;
            // цикл, с помощью которого попробуем найти совпадение сгенерированного id
            for(long i = 0; i <= 100000000;i++)
            {
                Random random = new Random();
                string guess = random.Next(0, 999999999).ToString();
                if (order == guess) resultUnique = false;
            }
            Assert.IsTrue(resultUnique);
          
        }

        // результат: проверка на защищенность хэширования
        [TestMethod]
        public void TestMethod2()
        {
            
        }


    }




}
