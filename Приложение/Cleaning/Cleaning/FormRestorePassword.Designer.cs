﻿
namespace Cleaning
{
    partial class FormRestorePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRestorePassword));
            this.ButtonToFormSignIn = new System.Windows.Forms.Button();
            this.ImageLogoApp = new System.Windows.Forms.PictureBox();
            this.LabelCaption = new System.Windows.Forms.Label();
            this.ButtonCloseForm = new System.Windows.Forms.PictureBox();
            this.guna2BorderlessForm1 = new Guna.UI2.WinForms.Guna2BorderlessForm(this.components);
            this.InputPhone1 = new System.Windows.Forms.Panel();
            this.ButtonGetCodeRestore = new Guna.UI2.WinForms.Guna2Button();
            this.label4 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.TextPhoneRestore = new Guna.UI2.WinForms.Guna2TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.InputCodeConfirm2 = new System.Windows.Forms.Panel();
            this.ButtonAgainCodeRestore = new Guna.UI2.WinForms.Guna2Button();
            this.ButtonVerificationCode = new Guna.UI2.WinForms.Guna2Button();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.TextCodeRestore = new Guna.UI2.WinForms.Guna2TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.InputNewPassword3 = new System.Windows.Forms.Panel();
            this.SwitchHideViewPassword = new System.Windows.Forms.PictureBox();
            this.ButtonChangePassword = new Guna.UI2.WinForms.Guna2Button();
            this.label5 = new System.Windows.Forms.Label();
            this.panel6 = new System.Windows.Forms.Panel();
            this.TextNewPassword = new Guna.UI2.WinForms.Guna2TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ImageLogoApp)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCloseForm)).BeginInit();
            this.InputPhone1.SuspendLayout();
            this.InputCodeConfirm2.SuspendLayout();
            this.InputNewPassword3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SwitchHideViewPassword)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonToFormSignIn
            // 
            this.ButtonToFormSignIn.AutoSize = true;
            this.ButtonToFormSignIn.FlatAppearance.BorderSize = 0;
            this.ButtonToFormSignIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonToFormSignIn.Font = new System.Drawing.Font("Oswald", 13F, System.Drawing.FontStyle.Underline);
            this.ButtonToFormSignIn.Location = new System.Drawing.Point(199, 790);
            this.ButtonToFormSignIn.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonToFormSignIn.Name = "ButtonToFormSignIn";
            this.ButtonToFormSignIn.Size = new System.Drawing.Size(90, 51);
            this.ButtonToFormSignIn.TabIndex = 29;
            this.ButtonToFormSignIn.Text = "НАЗАД";
            this.ButtonToFormSignIn.UseVisualStyleBackColor = true;
            this.ButtonToFormSignIn.Click += new System.EventHandler(this.ButtonToFormSignIn_Click);
            // 
            // ImageLogoApp
            // 
            this.ImageLogoApp.BackColor = System.Drawing.Color.Transparent;
            this.ImageLogoApp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ImageLogoApp.Image = global::Cleaning.Properties.Resources.IconAppCleaning2;
            this.ImageLogoApp.Location = new System.Drawing.Point(145, 189);
            this.ImageLogoApp.Margin = new System.Windows.Forms.Padding(2);
            this.ImageLogoApp.Name = "ImageLogoApp";
            this.ImageLogoApp.Size = new System.Drawing.Size(198, 99);
            this.ImageLogoApp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ImageLogoApp.TabIndex = 31;
            this.ImageLogoApp.TabStop = false;
            // 
            // LabelCaption
            // 
            this.LabelCaption.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelCaption.AutoSize = true;
            this.LabelCaption.Font = new System.Drawing.Font("Oswald", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelCaption.Location = new System.Drawing.Point(82, 74);
            this.LabelCaption.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LabelCaption.Name = "LabelCaption";
            this.LabelCaption.Size = new System.Drawing.Size(325, 51);
            this.LabelCaption.TabIndex = 30;
            this.LabelCaption.Text = "В О С С Т А Н О В Л Е Н И Е";
            // 
            // ButtonCloseForm
            // 
            this.ButtonCloseForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonCloseForm.Image = global::Cleaning.Properties.Resources.close1;
            this.ButtonCloseForm.Location = new System.Drawing.Point(452, 11);
            this.ButtonCloseForm.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonCloseForm.Name = "ButtonCloseForm";
            this.ButtonCloseForm.Size = new System.Drawing.Size(25, 25);
            this.ButtonCloseForm.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ButtonCloseForm.TabIndex = 35;
            this.ButtonCloseForm.TabStop = false;
            this.ButtonCloseForm.Click += new System.EventHandler(this.ButtonCloseForm_Click);
            // 
            // guna2BorderlessForm1
            // 
            this.guna2BorderlessForm1.AnimationInterval = 100;
            this.guna2BorderlessForm1.AnimationType = Guna.UI2.WinForms.Guna2BorderlessForm.AnimateWindowType.AW_VER_POSITIVE;
            this.guna2BorderlessForm1.ContainerControl = this;
            this.guna2BorderlessForm1.DockForm = false;
            this.guna2BorderlessForm1.DockIndicatorTransparencyValue = 1D;
            this.guna2BorderlessForm1.DragStartTransparencyValue = 1D;
            this.guna2BorderlessForm1.ResizeForm = false;
            this.guna2BorderlessForm1.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.guna2BorderlessForm1.TransparentWhileDrag = true;
            // 
            // InputPhone1
            // 
            this.InputPhone1.Controls.Add(this.ButtonGetCodeRestore);
            this.InputPhone1.Controls.Add(this.label4);
            this.InputPhone1.Controls.Add(this.panel2);
            this.InputPhone1.Controls.Add(this.TextPhoneRestore);
            this.InputPhone1.Controls.Add(this.label3);
            this.InputPhone1.Location = new System.Drawing.Point(102, 386);
            this.InputPhone1.Margin = new System.Windows.Forms.Padding(2);
            this.InputPhone1.Name = "InputPhone1";
            this.InputPhone1.Size = new System.Drawing.Size(282, 204);
            this.InputPhone1.TabIndex = 40;
            // 
            // ButtonGetCodeRestore
            // 
            this.ButtonGetCodeRestore.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonGetCodeRestore.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonGetCodeRestore.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonGetCodeRestore.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonGetCodeRestore.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ButtonGetCodeRestore.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.ButtonGetCodeRestore.Font = new System.Drawing.Font("Bebas Neue", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonGetCodeRestore.ForeColor = System.Drawing.Color.White;
            this.ButtonGetCodeRestore.Location = new System.Drawing.Point(0, 159);
            this.ButtonGetCodeRestore.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonGetCodeRestore.Name = "ButtonGetCodeRestore";
            this.ButtonGetCodeRestore.Size = new System.Drawing.Size(282, 45);
            this.ButtonGetCodeRestore.TabIndex = 41;
            this.ButtonGetCodeRestore.Text = "Далее";
            this.ButtonGetCodeRestore.Click += new System.EventHandler(this.guna2Button1_Click);
            // 
            // label4
            // 
            this.label4.Dock = System.Windows.Forms.DockStyle.Top;
            this.label4.Font = new System.Drawing.Font("Bebas Neue", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(152)))), ((int)(((byte)(152)))));
            this.label4.Location = new System.Drawing.Point(0, 76);
            this.label4.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(282, 36);
            this.label4.TabIndex = 40;
            this.label4.Text = "Введите тот номер телефона, который\r\nвы указывали при регистрации";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel2
            // 
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 72);
            this.panel2.Margin = new System.Windows.Forms.Padding(2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(282, 4);
            this.panel2.TabIndex = 39;
            // 
            // TextPhoneRestore
            // 
            this.TextPhoneRestore.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextPhoneRestore.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextPhoneRestore.DefaultText = "";
            this.TextPhoneRestore.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextPhoneRestore.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextPhoneRestore.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPhoneRestore.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPhoneRestore.Dock = System.Windows.Forms.DockStyle.Top;
            this.TextPhoneRestore.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPhoneRestore.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextPhoneRestore.ForeColor = System.Drawing.Color.Black;
            this.TextPhoneRestore.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPhoneRestore.Location = new System.Drawing.Point(0, 30);
            this.TextPhoneRestore.Margin = new System.Windows.Forms.Padding(4, 122, 4, 2);
            this.TextPhoneRestore.MaxLength = 11;
            this.TextPhoneRestore.Name = "TextPhoneRestore";
            this.TextPhoneRestore.PasswordChar = '\0';
            this.TextPhoneRestore.PlaceholderText = "7...";
            this.TextPhoneRestore.SelectedText = "";
            this.TextPhoneRestore.Size = new System.Drawing.Size(282, 42);
            this.TextPhoneRestore.TabIndex = 38;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Top;
            this.label3.Font = new System.Drawing.Font("Bebas Neue", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(0, 0);
            this.label3.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(0, 0, 0, 8);
            this.label3.Size = new System.Drawing.Size(151, 30);
            this.label3.TabIndex = 37;
            this.label3.Text = "Номер телефона";
            // 
            // InputCodeConfirm2
            // 
            this.InputCodeConfirm2.Controls.Add(this.ButtonAgainCodeRestore);
            this.InputCodeConfirm2.Controls.Add(this.ButtonVerificationCode);
            this.InputCodeConfirm2.Controls.Add(this.label1);
            this.InputCodeConfirm2.Controls.Add(this.panel4);
            this.InputCodeConfirm2.Controls.Add(this.TextCodeRestore);
            this.InputCodeConfirm2.Controls.Add(this.label2);
            this.InputCodeConfirm2.Location = new System.Drawing.Point(102, 386);
            this.InputCodeConfirm2.Margin = new System.Windows.Forms.Padding(2);
            this.InputCodeConfirm2.Name = "InputCodeConfirm2";
            this.InputCodeConfirm2.Size = new System.Drawing.Size(282, 204);
            this.InputCodeConfirm2.TabIndex = 42;
            // 
            // ButtonAgainCodeRestore
            // 
            this.ButtonAgainCodeRestore.Animated = true;
            this.ButtonAgainCodeRestore.BorderColor = System.Drawing.Color.Transparent;
            this.ButtonAgainCodeRestore.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAgainCodeRestore.DefaultAutoSize = true;
            this.ButtonAgainCodeRestore.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonAgainCodeRestore.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonAgainCodeRestore.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonAgainCodeRestore.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonAgainCodeRestore.Dock = System.Windows.Forms.DockStyle.Top;
            this.ButtonAgainCodeRestore.FillColor = System.Drawing.Color.White;
            this.ButtonAgainCodeRestore.Font = new System.Drawing.Font("Bebas Neue", 7.8F);
            this.ButtonAgainCodeRestore.ForeColor = System.Drawing.Color.Orange;
            this.ButtonAgainCodeRestore.Location = new System.Drawing.Point(0, 112);
            this.ButtonAgainCodeRestore.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonAgainCodeRestore.Name = "ButtonAgainCodeRestore";
            this.ButtonAgainCodeRestore.Size = new System.Drawing.Size(282, 28);
            this.ButtonAgainCodeRestore.TabIndex = 104;
            this.ButtonAgainCodeRestore.Text = "Отправить повторно через 30";
            this.ButtonAgainCodeRestore.Visible = false;
            this.ButtonAgainCodeRestore.Click += new System.EventHandler(this.ButtonAgainCodeRestore_Click);
            // 
            // ButtonVerificationCode
            // 
            this.ButtonVerificationCode.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonVerificationCode.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonVerificationCode.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonVerificationCode.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonVerificationCode.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ButtonVerificationCode.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.ButtonVerificationCode.Font = new System.Drawing.Font("Bebas Neue", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonVerificationCode.ForeColor = System.Drawing.Color.White;
            this.ButtonVerificationCode.Location = new System.Drawing.Point(0, 159);
            this.ButtonVerificationCode.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonVerificationCode.Name = "ButtonVerificationCode";
            this.ButtonVerificationCode.Size = new System.Drawing.Size(282, 45);
            this.ButtonVerificationCode.TabIndex = 41;
            this.ButtonVerificationCode.Text = "Далее";
            this.ButtonVerificationCode.Click += new System.EventHandler(this.guna2Button2_Click);
            // 
            // label1
            // 
            this.label1.Dock = System.Windows.Forms.DockStyle.Top;
            this.label1.Font = new System.Drawing.Font("Bebas Neue", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(152)))), ((int)(((byte)(152)))));
            this.label1.Location = new System.Drawing.Point(0, 76);
            this.label1.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(282, 36);
            this.label1.TabIndex = 40;
            this.label1.Text = "Введите 4-значный код, отправленный на ******.";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel4
            // 
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(0, 72);
            this.panel4.Margin = new System.Windows.Forms.Padding(2);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(282, 4);
            this.panel4.TabIndex = 39;
            // 
            // TextCodeRestore
            // 
            this.TextCodeRestore.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextCodeRestore.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextCodeRestore.DefaultText = "";
            this.TextCodeRestore.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextCodeRestore.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextCodeRestore.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextCodeRestore.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextCodeRestore.Dock = System.Windows.Forms.DockStyle.Top;
            this.TextCodeRestore.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextCodeRestore.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextCodeRestore.ForeColor = System.Drawing.Color.Black;
            this.TextCodeRestore.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextCodeRestore.Location = new System.Drawing.Point(0, 30);
            this.TextCodeRestore.Margin = new System.Windows.Forms.Padding(4, 122, 4, 2);
            this.TextCodeRestore.MaxLength = 4;
            this.TextCodeRestore.Name = "TextCodeRestore";
            this.TextCodeRestore.PasswordChar = '\0';
            this.TextCodeRestore.PlaceholderText = "например, 1111";
            this.TextCodeRestore.SelectedText = "";
            this.TextCodeRestore.Size = new System.Drawing.Size(282, 42);
            this.TextCodeRestore.TabIndex = 38;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Top;
            this.label2.Font = new System.Drawing.Font("Bebas Neue", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(0, 0);
            this.label2.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(0, 0, 0, 8);
            this.label2.Size = new System.Drawing.Size(265, 30);
            this.label2.TabIndex = 37;
            this.label2.Text = "Код подтверждения телефона";
            // 
            // InputNewPassword3
            // 
            this.InputNewPassword3.Controls.Add(this.SwitchHideViewPassword);
            this.InputNewPassword3.Controls.Add(this.ButtonChangePassword);
            this.InputNewPassword3.Controls.Add(this.label5);
            this.InputNewPassword3.Controls.Add(this.panel6);
            this.InputNewPassword3.Controls.Add(this.TextNewPassword);
            this.InputNewPassword3.Controls.Add(this.label6);
            this.InputNewPassword3.Location = new System.Drawing.Point(102, 386);
            this.InputNewPassword3.Margin = new System.Windows.Forms.Padding(2);
            this.InputNewPassword3.Name = "InputNewPassword3";
            this.InputNewPassword3.Size = new System.Drawing.Size(282, 204);
            this.InputNewPassword3.TabIndex = 43;
            // 
            // SwitchHideViewPassword
            // 
            this.SwitchHideViewPassword.Image = global::Cleaning.Properties.Resources.hidden;
            this.SwitchHideViewPassword.Location = new System.Drawing.Point(250, 40);
            this.SwitchHideViewPassword.Margin = new System.Windows.Forms.Padding(2);
            this.SwitchHideViewPassword.Name = "SwitchHideViewPassword";
            this.SwitchHideViewPassword.Size = new System.Drawing.Size(22, 22);
            this.SwitchHideViewPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.SwitchHideViewPassword.TabIndex = 94;
            this.SwitchHideViewPassword.TabStop = false;
            // 
            // ButtonChangePassword
            // 
            this.ButtonChangePassword.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonChangePassword.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonChangePassword.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonChangePassword.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonChangePassword.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ButtonChangePassword.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(97)))), ((int)(((byte)(97)))), ((int)(((byte)(97)))));
            this.ButtonChangePassword.Font = new System.Drawing.Font("Bebas Neue", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ButtonChangePassword.ForeColor = System.Drawing.Color.White;
            this.ButtonChangePassword.Location = new System.Drawing.Point(0, 159);
            this.ButtonChangePassword.Margin = new System.Windows.Forms.Padding(2);
            this.ButtonChangePassword.Name = "ButtonChangePassword";
            this.ButtonChangePassword.Size = new System.Drawing.Size(282, 45);
            this.ButtonChangePassword.TabIndex = 41;
            this.ButtonChangePassword.Text = "Изменить пароль";
            this.ButtonChangePassword.Click += new System.EventHandler(this.ButtonChangePassword_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Top;
            this.label5.Font = new System.Drawing.Font("Bebas Neue", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(152)))), ((int)(((byte)(152)))), ((int)(((byte)(152)))));
            this.label5.Location = new System.Drawing.Point(0, 76);
            this.label5.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(323, 34);
            this.label5.TabIndex = 40;
            this.label5.Text = "Придумайте надежный  пароль, и запомните его, \nа лучше запишите в блокнот.";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // panel6
            // 
            this.panel6.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel6.Location = new System.Drawing.Point(0, 72);
            this.panel6.Margin = new System.Windows.Forms.Padding(2);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(282, 4);
            this.panel6.TabIndex = 39;
            // 
            // TextNewPassword
            // 
            this.TextNewPassword.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextNewPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextNewPassword.DefaultText = "";
            this.TextNewPassword.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextNewPassword.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextNewPassword.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextNewPassword.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextNewPassword.Dock = System.Windows.Forms.DockStyle.Top;
            this.TextNewPassword.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextNewPassword.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextNewPassword.ForeColor = System.Drawing.Color.Black;
            this.TextNewPassword.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextNewPassword.Location = new System.Drawing.Point(0, 30);
            this.TextNewPassword.Margin = new System.Windows.Forms.Padding(4, 122, 4, 2);
            this.TextNewPassword.MaxLength = 16;
            this.TextNewPassword.Name = "TextNewPassword";
            this.TextNewPassword.PasswordChar = '\0';
            this.TextNewPassword.PlaceholderText = "Введите пароль";
            this.TextNewPassword.SelectedText = "";
            this.TextNewPassword.Size = new System.Drawing.Size(282, 42);
            this.TextNewPassword.TabIndex = 38;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Top;
            this.label6.Font = new System.Drawing.Font("Bebas Neue", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(0, 0);
            this.label6.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.label6.Name = "label6";
            this.label6.Padding = new System.Windows.Forms.Padding(0, 0, 0, 8);
            this.label6.Size = new System.Drawing.Size(232, 30);
            this.label6.TabIndex = 37;
            this.label6.Text = "Придумайте новый пароль";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // FormRestorePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(120F, 120F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(488, 840);
            this.Controls.Add(this.InputNewPassword3);
            this.Controls.Add(this.InputCodeConfirm2);
            this.Controls.Add(this.InputPhone1);
            this.Controls.Add(this.ButtonCloseForm);
            this.Controls.Add(this.ImageLogoApp);
            this.Controls.Add(this.LabelCaption);
            this.Controls.Add(this.ButtonToFormSignIn);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximumSize = new System.Drawing.Size(488, 852);
            this.MinimumSize = new System.Drawing.Size(488, 832);
            this.Name = "FormRestorePassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormRestorePassword";
            ((System.ComponentModel.ISupportInitialize)(this.ImageLogoApp)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCloseForm)).EndInit();
            this.InputPhone1.ResumeLayout(false);
            this.InputPhone1.PerformLayout();
            this.InputCodeConfirm2.ResumeLayout(false);
            this.InputCodeConfirm2.PerformLayout();
            this.InputNewPassword3.ResumeLayout(false);
            this.InputNewPassword3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SwitchHideViewPassword)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtonToFormSignIn;
        private System.Windows.Forms.PictureBox ImageLogoApp;
        private System.Windows.Forms.Label LabelCaption;
        private System.Windows.Forms.PictureBox ButtonCloseForm;
        private Guna.UI2.WinForms.Guna2BorderlessForm guna2BorderlessForm1;
        private System.Windows.Forms.Panel InputPhone1;
        private Guna.UI2.WinForms.Guna2TextBox TextPhoneRestore;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel2;
        private Guna.UI2.WinForms.Guna2Button ButtonGetCodeRestore;
        private System.Windows.Forms.Panel InputCodeConfirm2;
        private Guna.UI2.WinForms.Guna2Button ButtonVerificationCode;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private Guna.UI2.WinForms.Guna2TextBox TextCodeRestore;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel InputNewPassword3;
        private Guna.UI2.WinForms.Guna2Button ButtonChangePassword;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Panel panel6;
        private Guna.UI2.WinForms.Guna2TextBox TextNewPassword;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.PictureBox SwitchHideViewPassword;
        private Guna.UI2.WinForms.Guna2Button ButtonAgainCodeRestore;
        private System.Windows.Forms.Timer timer1;
    }
}