﻿
namespace Cleaning
{
    partial class FormRegistrationOrder
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormRegistrationOrder));
            this.label6 = new System.Windows.Forms.Label();
            this.TextCostOrder = new Guna.UI2.WinForms.Guna2TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TextServiceOrder = new Guna.UI2.WinForms.Guna2TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LabelService = new System.Windows.Forms.Label();
            this.ButtonRegistrationOrder = new Guna.UI2.WinForms.Guna2Button();
            this.TextDescription = new Guna.UI2.WinForms.Guna2TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.TextCountOrder = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.DateOrder = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.ComboBoxTimeOrder = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2BorderlessForm1 = new Guna.UI2.WinForms.Guna2BorderlessForm(this.components);
            this.label7 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.ButtonCloseForm = new System.Windows.Forms.PictureBox();
            this.LabelOrderId = new System.Windows.Forms.Label();
            this.LabelTotalCost = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCloseForm)).BeginInit();
            this.SuspendLayout();
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.label6.Location = new System.Drawing.Point(235, 299);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(66, 25);
            this.label6.TabIndex = 188;
            this.label6.Text = " ₽ / м²";
            // 
            // TextCostOrder
            // 
            this.TextCostOrder.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextCostOrder.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextCostOrder.DefaultText = "";
            this.TextCostOrder.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextCostOrder.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextCostOrder.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextCostOrder.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextCostOrder.Enabled = false;
            this.TextCostOrder.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextCostOrder.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextCostOrder.ForeColor = System.Drawing.Color.Black;
            this.TextCostOrder.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextCostOrder.Location = new System.Drawing.Point(51, 290);
            this.TextCostOrder.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextCostOrder.MaxLength = 4;
            this.TextCostOrder.Name = "TextCostOrder";
            this.TextCostOrder.PasswordChar = '\0';
            this.TextCostOrder.PlaceholderText = "";
            this.TextCostOrder.SelectedText = "";
            this.TextCostOrder.Size = new System.Drawing.Size(181, 38);
            this.TextCostOrder.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label2.Location = new System.Drawing.Point(45, 262);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(168, 24);
            this.label2.TabIndex = 187;
            this.label2.Text = "Стоимость услуги";
            // 
            // TextServiceOrder
            // 
            this.TextServiceOrder.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextServiceOrder.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextServiceOrder.DefaultText = "";
            this.TextServiceOrder.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextServiceOrder.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextServiceOrder.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextServiceOrder.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextServiceOrder.Enabled = false;
            this.TextServiceOrder.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextServiceOrder.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextServiceOrder.ForeColor = System.Drawing.Color.Black;
            this.TextServiceOrder.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextServiceOrder.Location = new System.Drawing.Point(49, 209);
            this.TextServiceOrder.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextServiceOrder.MaxLength = 32;
            this.TextServiceOrder.Name = "TextServiceOrder";
            this.TextServiceOrder.PasswordChar = '\0';
            this.TextServiceOrder.PlaceholderText = "";
            this.TextServiceOrder.SelectedText = "";
            this.TextServiceOrder.Size = new System.Drawing.Size(427, 38);
            this.TextServiceOrder.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label3.Location = new System.Drawing.Point(45, 180);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(205, 24);
            this.label3.TabIndex = 185;
            this.label3.Text = "Наименование услуги";
            // 
            // panel1
            // 
            this.panel1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.panel1.Location = new System.Drawing.Point(51, 153);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(65, 2);
            this.panel1.TabIndex = 184;
            // 
            // LabelService
            // 
            this.LabelService.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LabelService.AutoSize = true;
            this.LabelService.Font = new System.Drawing.Font("Noto Sans", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelService.Location = new System.Drawing.Point(43, 50);
            this.LabelService.Name = "LabelService";
            this.LabelService.Size = new System.Drawing.Size(226, 82);
            this.LabelService.TabIndex = 183;
            this.LabelService.Text = "Оформление\r\nзаказа";
            // 
            // ButtonRegistrationOrder
            // 
            this.ButtonRegistrationOrder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonRegistrationOrder.Animated = true;
            this.ButtonRegistrationOrder.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonRegistrationOrder.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonRegistrationOrder.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonRegistrationOrder.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonRegistrationOrder.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.ButtonRegistrationOrder.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.ButtonRegistrationOrder.ForeColor = System.Drawing.Color.White;
            this.ButtonRegistrationOrder.Location = new System.Drawing.Point(51, 782);
            this.ButtonRegistrationOrder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonRegistrationOrder.Name = "ButtonRegistrationOrder";
            this.ButtonRegistrationOrder.Size = new System.Drawing.Size(427, 53);
            this.ButtonRegistrationOrder.TabIndex = 6;
            this.ButtonRegistrationOrder.Text = "Оформить";
            this.ButtonRegistrationOrder.Click += new System.EventHandler(this.guna2Button1_Click);
            // 
            // TextDescription
            // 
            this.TextDescription.AutoScroll = true;
            this.TextDescription.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextDescription.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextDescription.DefaultText = "";
            this.TextDescription.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextDescription.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextDescription.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextDescription.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextDescription.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextDescription.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextDescription.ForeColor = System.Drawing.Color.Black;
            this.TextDescription.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextDescription.Location = new System.Drawing.Point(51, 535);
            this.TextDescription.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextDescription.MaxLength = 500;
            this.TextDescription.Multiline = true;
            this.TextDescription.Name = "TextDescription";
            this.TextDescription.PasswordChar = '\0';
            this.TextDescription.PlaceholderText = "...";
            this.TextDescription.SelectedText = "";
            this.TextDescription.Size = new System.Drawing.Size(427, 129);
            this.TextDescription.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label1.Location = new System.Drawing.Point(45, 506);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 24);
            this.label1.TabIndex = 194;
            this.label1.Text = "Примечание";
            // 
            // TextCountOrder
            // 
            this.TextCountOrder.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextCountOrder.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextCountOrder.DefaultText = "";
            this.TextCountOrder.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextCountOrder.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextCountOrder.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextCountOrder.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextCountOrder.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextCountOrder.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextCountOrder.ForeColor = System.Drawing.Color.Black;
            this.TextCountOrder.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextCountOrder.Location = new System.Drawing.Point(51, 375);
            this.TextCountOrder.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextCountOrder.MaxLength = 3;
            this.TextCountOrder.Name = "TextCountOrder";
            this.TextCountOrder.PasswordChar = '\0';
            this.TextCountOrder.PlaceholderText = "20";
            this.TextCountOrder.SelectedText = "";
            this.TextCountOrder.Size = new System.Drawing.Size(143, 38);
            this.TextCountOrder.TabIndex = 2;
            this.TextCountOrder.TextChanged += new System.EventHandler(this.TextCountOrder_TextChanged);
            this.TextCountOrder.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextCountOrder_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label4.Location = new System.Drawing.Point(45, 346);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(213, 24);
            this.label4.TabIndex = 196;
            this.label4.Text = "Количество кв. метров";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label5.Location = new System.Drawing.Point(45, 425);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(296, 24);
            this.label5.TabIndex = 198;
            this.label5.Text = "Выберите удобное время и дату";
            // 
            // DateOrder
            // 
            this.DateOrder.Animated = true;
            this.DateOrder.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.DateOrder.BorderThickness = 1;
            this.DateOrder.Checked = true;
            this.DateOrder.FillColor = System.Drawing.Color.White;
            this.DateOrder.Font = new System.Drawing.Font("Noto Sans", 9F);
            this.DateOrder.ForeColor = System.Drawing.Color.Black;
            this.DateOrder.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.DateOrder.Location = new System.Drawing.Point(51, 454);
            this.DateOrder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.DateOrder.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.DateOrder.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.DateOrder.Name = "DateOrder";
            this.DateOrder.Size = new System.Drawing.Size(243, 38);
            this.DateOrder.TabIndex = 3;
            this.DateOrder.Value = new System.DateTime(2022, 2, 19, 20, 17, 27, 947);
            // 
            // ComboBoxTimeOrder
            // 
            this.ComboBoxTimeOrder.BackColor = System.Drawing.Color.Transparent;
            this.ComboBoxTimeOrder.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.ComboBoxTimeOrder.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ComboBoxTimeOrder.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxTimeOrder.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.ComboBoxTimeOrder.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.ComboBoxTimeOrder.Font = new System.Drawing.Font("Noto Sans", 9F);
            this.ComboBoxTimeOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.ComboBoxTimeOrder.ItemHeight = 30;
            this.ComboBoxTimeOrder.Items.AddRange(new object[] {
            "10:00",
            "12:00",
            "14:00",
            "16:00",
            "18:00"});
            this.ComboBoxTimeOrder.Location = new System.Drawing.Point(336, 454);
            this.ComboBoxTimeOrder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ComboBoxTimeOrder.Name = "ComboBoxTimeOrder";
            this.ComboBoxTimeOrder.Size = new System.Drawing.Size(139, 36);
            this.ComboBoxTimeOrder.TabIndex = 4;
            // 
            // guna2BorderlessForm1
            // 
            this.guna2BorderlessForm1.ContainerControl = this;
            this.guna2BorderlessForm1.DockIndicatorTransparencyValue = 1D;
            this.guna2BorderlessForm1.DragStartTransparencyValue = 1D;
            this.guna2BorderlessForm1.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.guna2BorderlessForm1.TransparentWhileDrag = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.White;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.label7.Location = new System.Drawing.Point(117, 722);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(325, 34);
            this.label7.TabIndex = 201;
            this.label7.Text = "Убедитесь, что адрес указанный при регистрации \r\nявляется местом выполнением рабо" +
    "т";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Cleaning.Properties.Resources.exclamantation;
            this.pictureBox1.Location = new System.Drawing.Point(77, 722);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(35, 34);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 202;
            this.pictureBox1.TabStop = false;
            // 
            // ButtonCloseForm
            // 
            this.ButtonCloseForm.Image = global::Cleaning.Properties.Resources.close1;
            this.ButtonCloseForm.Location = new System.Drawing.Point(483, 12);
            this.ButtonCloseForm.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonCloseForm.Name = "ButtonCloseForm";
            this.ButtonCloseForm.Size = new System.Drawing.Size(25, 25);
            this.ButtonCloseForm.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ButtonCloseForm.TabIndex = 191;
            this.ButtonCloseForm.TabStop = false;
            this.ButtonCloseForm.Click += new System.EventHandler(this.ButtonCloseForm_Click);
            // 
            // LabelOrderId
            // 
            this.LabelOrderId.AutoSize = true;
            this.LabelOrderId.BackColor = System.Drawing.Color.White;
            this.LabelOrderId.Font = new System.Drawing.Font("Ubuntu", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelOrderId.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.LabelOrderId.Location = new System.Drawing.Point(323, 64);
            this.LabelOrderId.Name = "LabelOrderId";
            this.LabelOrderId.Size = new System.Drawing.Size(153, 21);
            this.LabelOrderId.TabIndex = 203;
            this.LabelOrderId.Text = "Заказ: 612322513";
            this.LabelOrderId.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelTotalCost
            // 
            this.LabelTotalCost.AutoSize = true;
            this.LabelTotalCost.Font = new System.Drawing.Font("Noto Sans", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelTotalCost.Location = new System.Drawing.Point(44, 676);
            this.LabelTotalCost.Name = "LabelTotalCost";
            this.LabelTotalCost.Size = new System.Drawing.Size(295, 32);
            this.LabelTotalCost.TabIndex = 204;
            this.LabelTotalCost.Text = "Общая стоимость: 0 руб.";
            // 
            // FormRegistrationOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(520, 880);
            this.Controls.Add(this.LabelTotalCost);
            this.Controls.Add(this.LabelOrderId);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ComboBoxTimeOrder);
            this.Controls.Add(this.DateOrder);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TextCountOrder);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextDescription);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtonRegistrationOrder);
            this.Controls.Add(this.ButtonCloseForm);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TextCostOrder);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TextServiceOrder);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LabelService);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximumSize = new System.Drawing.Size(520, 880);
            this.MinimumSize = new System.Drawing.Size(520, 880);
            this.Name = "FormRegistrationOrder";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormRegistrationOrder";
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCloseForm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label label6;
        private Guna.UI2.WinForms.Guna2TextBox TextCostOrder;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2TextBox TextServiceOrder;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LabelService;
        private System.Windows.Forms.PictureBox ButtonCloseForm;
        private Guna.UI2.WinForms.Guna2Button ButtonRegistrationOrder;
        private Guna.UI2.WinForms.Guna2TextBox TextDescription;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2TextBox TextCountOrder;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private Guna.UI2.WinForms.Guna2DateTimePicker DateOrder;
        private Guna.UI2.WinForms.Guna2ComboBox ComboBoxTimeOrder;
        private Guna.UI2.WinForms.Guna2BorderlessForm guna2BorderlessForm1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label LabelOrderId;
        private System.Windows.Forms.Label LabelTotalCost;
    }
}