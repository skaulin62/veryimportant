﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class CardOrderWaitComment : UserControl
    {
        public CardOrderWaitComment(string order, string service, string count, string cost, string datetime)
        {
            InitializeComponent();
            LabelInfo.Text = "Заказ: " + order + " • " + service + " • " + count + " кв. м • " + cost + " руб.";
            LabelDateTime.Text = datetime.Substring(0, 16);
            ButtonSendComment.Name = order;


        }

        private void ButtonSendComment_Click(object sender, EventArgs e)
        {
            if(RatingQualityPersonal.Value == 0 || RatingQualityService.Value == 0)
            {
                MessageBox.Show("Перед отправкой отзыва, оцените уборку по критериям!", "Предупреждение");
            } else
            {
                using(SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand($"update Заказы set Качество_уборки = '{(Math.Round(RatingQualityService.Value)).ToString()}', Дружелюбность_персонала = '{(Math.Round(RatingQualityPersonal.Value)).ToString()}', Отзыв = '{TextComment.Text}', Статус = 'Выполнен' Where Номер_заказа = '{(sender as Guna.UI2.WinForms.Guna2Button).Name}'", connection);

                    try
                    {
                        connection.Open();
                        sqlCommand.ExecuteNonQuery();
                        MessageBox.Show("Отзыв отправлен", "Уведомление");
                        this.Hide();
                    } catch
                    {
                        MessageBox.Show("Не получилось отправить отзыв", "Ошибка");
                    }
                }
            }
        }
    }
}
