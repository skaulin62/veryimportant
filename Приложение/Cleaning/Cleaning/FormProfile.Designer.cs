﻿
namespace Cleaning
{
    partial class FormProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label8 = new System.Windows.Forms.Label();
            this.TextSurname = new Guna.UI2.WinForms.Guna2TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TextName = new Guna.UI2.WinForms.Guna2TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.TextLastname = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.TextAddress = new Guna.UI2.WinForms.Guna2TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.TextPassword = new Guna.UI2.WinForms.Guna2TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.TextLoginPhone = new Guna.UI2.WinForms.Guna2TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.TextSeriusPassport = new Guna.UI2.WinForms.Guna2TextBox();
            this.TextNumberPassport = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2Button1 = new Guna.UI2.WinForms.Guna2Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label9 = new System.Windows.Forms.Label();
            this.guna2Button2 = new Guna.UI2.WinForms.Guna2Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.SwitchHideViewPassword = new System.Windows.Forms.PictureBox();
            this.LabelDate = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SwitchHideViewPassword)).BeginInit();
            this.SuspendLayout();
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label8.Location = new System.Drawing.Point(53, 288);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(227, 24);
            this.label8.TabIndex = 187;
            this.label8.Text = "Номер телефона / логин";
            this.label8.Click += new System.EventHandler(this.label8_Click);
            // 
            // TextSurname
            // 
            this.TextSurname.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextSurname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSurname.DefaultText = "";
            this.TextSurname.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextSurname.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextSurname.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextSurname.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextSurname.Enabled = false;
            this.TextSurname.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextSurname.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextSurname.ForeColor = System.Drawing.Color.Black;
            this.TextSurname.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextSurname.Location = new System.Drawing.Point(441, 74);
            this.TextSurname.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextSurname.MaxLength = 32;
            this.TextSurname.Name = "TextSurname";
            this.TextSurname.PasswordChar = '\0';
            this.TextSurname.PlaceholderText = "Иванов";
            this.TextSurname.SelectedText = "";
            this.TextSurname.Size = new System.Drawing.Size(331, 38);
            this.TextSurname.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label3.Location = new System.Drawing.Point(336, 79);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 24);
            this.label3.TabIndex = 185;
            this.label3.Text = "Фамилия";
            // 
            // TextName
            // 
            this.TextName.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextName.DefaultText = "";
            this.TextName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextName.Enabled = false;
            this.TextName.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextName.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextName.ForeColor = System.Drawing.Color.Black;
            this.TextName.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextName.Location = new System.Drawing.Point(441, 118);
            this.TextName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextName.MaxLength = 32;
            this.TextName.Name = "TextName";
            this.TextName.PasswordChar = '\0';
            this.TextName.PlaceholderText = "Иван";
            this.TextName.SelectedText = "";
            this.TextName.Size = new System.Drawing.Size(331, 38);
            this.TextName.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label2.Location = new System.Drawing.Point(382, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(47, 24);
            this.label2.TabIndex = 189;
            this.label2.Text = "Имя";
            // 
            // TextLastname
            // 
            this.TextLastname.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextLastname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextLastname.DefaultText = "";
            this.TextLastname.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextLastname.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextLastname.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextLastname.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextLastname.Enabled = false;
            this.TextLastname.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextLastname.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextLastname.ForeColor = System.Drawing.Color.Black;
            this.TextLastname.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextLastname.Location = new System.Drawing.Point(441, 162);
            this.TextLastname.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextLastname.MaxLength = 32;
            this.TextLastname.Name = "TextLastname";
            this.TextLastname.PasswordChar = '\0';
            this.TextLastname.PlaceholderText = "...";
            this.TextLastname.SelectedText = "";
            this.TextLastname.Size = new System.Drawing.Size(331, 38);
            this.TextLastname.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label4.Location = new System.Drawing.Point(335, 167);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 24);
            this.label4.TabIndex = 191;
            this.label4.Text = "Отчество";
            // 
            // TextAddress
            // 
            this.TextAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextAddress.AutoScroll = true;
            this.TextAddress.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextAddress.DefaultText = "";
            this.TextAddress.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextAddress.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextAddress.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextAddress.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextAddress.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextAddress.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextAddress.ForeColor = System.Drawing.Color.Black;
            this.TextAddress.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextAddress.Location = new System.Drawing.Point(54, 373);
            this.TextAddress.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextAddress.MaxLength = 300;
            this.TextAddress.Multiline = true;
            this.TextAddress.Name = "TextAddress";
            this.TextAddress.PasswordChar = '\0';
            this.TextAddress.PlaceholderText = "...";
            this.TextAddress.SelectedText = "";
            this.TextAddress.Size = new System.Drawing.Size(1021, 177);
            this.TextAddress.TabIndex = 8;
            this.TextAddress.TextChanged += new System.EventHandler(this.guna2TextBox4_TextChanged);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label5.Location = new System.Drawing.Point(53, 342);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(179, 24);
            this.label5.TabIndex = 194;
            this.label5.Text = "Адрес проживания";
            // 
            // TextPassword
            // 
            this.TextPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextPassword.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextPassword.DefaultText = "";
            this.TextPassword.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextPassword.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextPassword.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPassword.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPassword.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPassword.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextPassword.ForeColor = System.Drawing.Color.Black;
            this.TextPassword.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPassword.Location = new System.Drawing.Point(763, 282);
            this.TextPassword.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextPassword.MaxLength = 32;
            this.TextPassword.Name = "TextPassword";
            this.TextPassword.PasswordChar = '\0';
            this.TextPassword.PlaceholderText = "LKO@P#31dsa";
            this.TextPassword.SelectedText = "";
            this.TextPassword.Size = new System.Drawing.Size(312, 38);
            this.TextPassword.TabIndex = 6;
            this.TextPassword.TextChanged += new System.EventHandler(this.guna2TextBox5_TextChanged);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label6.Location = new System.Drawing.Point(608, 288);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 24);
            this.label6.TabIndex = 196;
            this.label6.Text = "Новый пароль";
            // 
            // TextLoginPhone
            // 
            this.TextLoginPhone.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextLoginPhone.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextLoginPhone.DefaultText = "";
            this.TextLoginPhone.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextLoginPhone.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextLoginPhone.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextLoginPhone.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextLoginPhone.Enabled = false;
            this.TextLoginPhone.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextLoginPhone.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextLoginPhone.ForeColor = System.Drawing.Color.Black;
            this.TextLoginPhone.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextLoginPhone.Location = new System.Drawing.Point(304, 282);
            this.TextLoginPhone.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextLoginPhone.MaxLength = 32;
            this.TextLoginPhone.Name = "TextLoginPhone";
            this.TextLoginPhone.PasswordChar = '\0';
            this.TextLoginPhone.PlaceholderText = "7... / login123";
            this.TextLoginPhone.SelectedText = "";
            this.TextLoginPhone.Size = new System.Drawing.Size(283, 38);
            this.TextLoginPhone.TabIndex = 5;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label7.Location = new System.Drawing.Point(344, 211);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(84, 24);
            this.label7.TabIndex = 199;
            this.label7.Text = "Паспорт";
            // 
            // TextSeriusPassport
            // 
            this.TextSeriusPassport.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextSeriusPassport.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSeriusPassport.DefaultText = "";
            this.TextSeriusPassport.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextSeriusPassport.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextSeriusPassport.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextSeriusPassport.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextSeriusPassport.Enabled = false;
            this.TextSeriusPassport.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextSeriusPassport.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextSeriusPassport.ForeColor = System.Drawing.Color.Black;
            this.TextSeriusPassport.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextSeriusPassport.Location = new System.Drawing.Point(441, 206);
            this.TextSeriusPassport.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextSeriusPassport.MaxLength = 32;
            this.TextSeriusPassport.Name = "TextSeriusPassport";
            this.TextSeriusPassport.PasswordChar = '\0';
            this.TextSeriusPassport.PlaceholderText = "1234";
            this.TextSeriusPassport.SelectedText = "";
            this.TextSeriusPassport.Size = new System.Drawing.Size(117, 38);
            this.TextSeriusPassport.TabIndex = 3;
            // 
            // TextNumberPassport
            // 
            this.TextNumberPassport.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextNumberPassport.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextNumberPassport.DefaultText = "";
            this.TextNumberPassport.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextNumberPassport.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextNumberPassport.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextNumberPassport.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextNumberPassport.Enabled = false;
            this.TextNumberPassport.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextNumberPassport.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextNumberPassport.ForeColor = System.Drawing.Color.Black;
            this.TextNumberPassport.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextNumberPassport.Location = new System.Drawing.Point(566, 206);
            this.TextNumberPassport.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextNumberPassport.MaxLength = 32;
            this.TextNumberPassport.Name = "TextNumberPassport";
            this.TextNumberPassport.PasswordChar = '\0';
            this.TextNumberPassport.PlaceholderText = "123456";
            this.TextNumberPassport.SelectedText = "";
            this.TextNumberPassport.Size = new System.Drawing.Size(206, 38);
            this.TextNumberPassport.TabIndex = 4;
            // 
            // guna2Button1
            // 
            this.guna2Button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2Button1.Animated = true;
            this.guna2Button1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.guna2Button1.Font = new System.Drawing.Font("Noto Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2Button1.ForeColor = System.Drawing.Color.White;
            this.guna2Button1.Location = new System.Drawing.Point(763, 566);
            this.guna2Button1.Name = "guna2Button1";
            this.guna2Button1.Size = new System.Drawing.Size(312, 38);
            this.guna2Button1.TabIndex = 9;
            this.guna2Button1.Text = "Сохранить";
            this.guna2Button1.Click += new System.EventHandler(this.guna2Button1_Click);
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.White;
            this.panel1.Controls.Add(this.label9);
            this.panel1.Location = new System.Drawing.Point(804, 73);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(271, 170);
            this.panel1.TabIndex = 203;
            // 
            // label9
            // 
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Noto Sans", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label9.Location = new System.Drawing.Point(0, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(271, 170);
            this.label9.TabIndex = 0;
            this.label9.Text = "При необходимости вы \r\nможете изменить \r\nпароль для входа\r\nили поменять адрес \r\nп" +
    "роживания";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // guna2Button2
            // 
            this.guna2Button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2Button2.Animated = true;
            this.guna2Button2.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button2.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button2.FillColor = System.Drawing.Color.DimGray;
            this.guna2Button2.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.guna2Button2.ForeColor = System.Drawing.Color.White;
            this.guna2Button2.Location = new System.Drawing.Point(763, 328);
            this.guna2Button2.Name = "guna2Button2";
            this.guna2Button2.Size = new System.Drawing.Size(312, 38);
            this.guna2Button2.TabIndex = 7;
            this.guna2Button2.Text = "Поменять";
            this.guna2Button2.Click += new System.EventHandler(this.guna2Button2_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Cleaning.Properties.Resources.profile1;
            this.pictureBox1.Location = new System.Drawing.Point(53, 53);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(236, 204);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // SwitchHideViewPassword
            // 
            this.SwitchHideViewPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SwitchHideViewPassword.Image = global::Cleaning.Properties.Resources.hidden;
            this.SwitchHideViewPassword.Location = new System.Drawing.Point(1044, 291);
            this.SwitchHideViewPassword.Name = "SwitchHideViewPassword";
            this.SwitchHideViewPassword.Size = new System.Drawing.Size(22, 22);
            this.SwitchHideViewPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.SwitchHideViewPassword.TabIndex = 205;
            this.SwitchHideViewPassword.TabStop = false;
            this.SwitchHideViewPassword.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SwitchHideViewPassword_MouseDown);
            this.SwitchHideViewPassword.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SwitchHideViewPassword_MouseUp);
            // 
            // LabelDate
            // 
            this.LabelDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LabelDate.AutoSize = true;
            this.LabelDate.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelDate.Location = new System.Drawing.Point(54, 581);
            this.LabelDate.Name = "LabelDate";
            this.LabelDate.Size = new System.Drawing.Size(252, 23);
            this.LabelDate.TabIndex = 206;
            this.LabelDate.Text = "Дата регистрации: 12.03.2022";
            // 
            // FormProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1153, 640);
            this.Controls.Add(this.LabelDate);
            this.Controls.Add(this.SwitchHideViewPassword);
            this.Controls.Add(this.guna2Button2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.guna2Button1);
            this.Controls.Add(this.TextNumberPassport);
            this.Controls.Add(this.TextSeriusPassport);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.TextLoginPhone);
            this.Controls.Add(this.TextPassword);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.TextAddress);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TextLastname);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextName);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TextSurname);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pictureBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormProfile";
            this.Padding = new System.Windows.Forms.Padding(50);
            this.Text = "FormProfile";
            this.Load += new System.EventHandler(this.FormProfile_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SwitchHideViewPassword)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label label8;
        private Guna.UI2.WinForms.Guna2TextBox TextSurname;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2TextBox TextName;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2TextBox TextLastname;
        private System.Windows.Forms.Label label4;
        private Guna.UI2.WinForms.Guna2TextBox TextAddress;
        private System.Windows.Forms.Label label5;
        private Guna.UI2.WinForms.Guna2TextBox TextPassword;
        private System.Windows.Forms.Label label6;
        private Guna.UI2.WinForms.Guna2TextBox TextLoginPhone;
        private System.Windows.Forms.Label label7;
        private Guna.UI2.WinForms.Guna2TextBox TextSeriusPassport;
        private Guna.UI2.WinForms.Guna2TextBox TextNumberPassport;
        private Guna.UI2.WinForms.Guna2Button guna2Button1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label9;
        private Guna.UI2.WinForms.Guna2Button guna2Button2;
        private System.Windows.Forms.PictureBox SwitchHideViewPassword;
        private System.Windows.Forms.Label LabelDate;
    }
}