﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Cleaning
{
    public class Account
    {
        public Account(int id, string surname, string name, string lastname, string passport, string phone, string birthday, string login, string password, string role)
        {
            this.id = id;
            this.surname = surname;
            this.name = name;
            this.lastname = lastname;
            this.passport = passport;
            this.phone = phone;
            this.birthday = birthday;
            this.login = login;
            this.password = password;
            this.role = role;
        }

        public int id { get; set; }
        public string surname { get; set; }
        public string name { get; set; }
        public string lastname { get; set; }
        public string passport { get; set; }
        public string phone { get; set; }
        public string birthday { get; set; }
   
        public string login { get; set; }
        public string password { get; set; }
        public string role { get; set; }

    }
}
