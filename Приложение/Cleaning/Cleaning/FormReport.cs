﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft;
namespace Cleaning
{
    public partial class FormReport : Form
    {
        public FormReport()
        {
            InitializeComponent();
            LoadIndicator();
            FillDiagram();
            FillReportTable("select * from report");
            
        }

        private void FillReportTable(string query)
        {
            try 
            { 
            ReportTable.DataSource = Connection.GetTable(query).Tables[0];
            }
            catch
            {
                MessageBox.Show("Данные не были подгружены!", "Ошибка");
            }
        }
        private void LoadIndicator()
        {
            try
            {
                LabelCountOrderDay.Text = Connection.GetTable("select count(Номер_заказа) from Заказы where Дата_уборки between dateadd(dd, 0, getdate()) and dateadd(day,1, getdate()) ").Tables[0].Rows[0][0].ToString();
                LabelCountOrderWeek.Text = Connection.GetTable("select count(Номер_заказа) from Заказы where Дата_уборки between dateadd(dd, -6, getdate()) and dateadd(day,1, getdate()) ").Tables[0].Rows[0][0].ToString();
                LabelCountOrderMonth.Text = Connection.GetTable("select count(Номер_заказа) from Заказы where Дата_уборки between dateadd(dd, -29, getdate()) and dateadd(day,1, getdate()) ").Tables[0].Rows[0][0].ToString();

                LabelCountClients.Text = Connection.GetTable("select count(*) from Клиенты").Tables[0].Rows[0][0].ToString() + " чел.";
                LabelAvgCost.Text = Connection.GetTable("select Convert(int,avg(Общая_стоимость)) from Заказы where Статус = 'Выполнен'").Tables[0].Rows[0][0].ToString() + " руб.";
                LabelSummCost.Text = Connection.GetTable($"select Convert(int,sum(Общая_стоимость)) from Заказы where Статус = 'Выполнен'").Tables[0].Rows[0][0].ToString() + " руб.";

            }catch
            {
                MessageBox.Show("Данные не были подгружены!", "Ошибка");
            }

        }
        private void FillDiagram()
        {
            try
            {
                DataSet dataSet = Connection.GetTable("select * from diagramm");

                for (int i = 0; i < dataSet.Tables[0].Rows.Count; i++)
                {
                    diagramm.Series.FindByName("Услуги").Points.AddXY(dataSet.Tables[0].Rows[i][0].ToString(), Convert.ToInt32(dataSet.Tables[0].Rows[i][1]));


                }
            }
            catch
            {
                MessageBox.Show("Данные не были подгружены!", "Ошибка");
            }

           
        }
       
        private void ButtonReport_Click(object sender, EventArgs e)
        {
            try
            {
                // creating Excel Application  
                Microsoft.Office.Interop.Excel._Application app = new Microsoft.Office.Interop.Excel.Application();
                // creating new WorkBook within Excel application  
                Microsoft.Office.Interop.Excel._Workbook workbook = app.Workbooks.Add(Type.Missing);
                // creating new Excelsheet in workbook  
                Microsoft.Office.Interop.Excel._Worksheet worksheet = null;
                // see the excel sheet behind the program  
                app.Visible = true;
           
                worksheet = workbook.ActiveSheet;
                //  name of active sheet  
                worksheet.Name = "Отчет по заказам";
                // storing header part in Excel  
                for (int i = 1; i < ReportTable.Columns.Count + 1; i++)
                {
                    worksheet.Cells[1, i] = ReportTable.Columns[i - 1].HeaderText;
                }
                // storing Each row and column value to excel sheet  
                for (int i = 0; i < ReportTable.Rows.Count; i++)
                {
                    for (int j = 0; j < ReportTable.Columns.Count; j++)
                    {
                        worksheet.Cells[i + 2, j + 1] = ReportTable.Rows[i].Cells[j].Value.ToString();
                    }
                }
            } catch
            {
                MessageBox.Show("Что-то пошло не так, возможно у вас нет Excel", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            
            
            
            //// Exit from the application  
            //app.Quit();
        }

        private void guna2DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            FillReportTable($"select * from report where [Дата уборки] between convert(datetime,'{DateStart.Value.ToShortDateString()}',105) and convert(datetime, '{DateEnd.Value.ToShortDateString()}',105)");
        }

        private void DateEnd_ValueChanged(object sender, EventArgs e)
        {
            FillReportTable($"select * from report where [Дата уборки] between convert(datetime,'{DateStart.Value.ToShortDateString()}',105) and convert(datetime, '{DateEnd.Value.ToShortDateString()}',105)");
        }
    }
}
