﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class FormServices : Form
    {
        public FormServices(string account)
        {
            InitializeComponent();
            this.account = account;
            FillComboBox(ComboBoxCategoryServices, "select Наименование From Категории_услуг");
            FillCardsServices("select * from ServicesCategories Where category = 'Уборка' order by id desc");
          
        }
        string account;
        private void FillComboBox(ComboBox list, string query)
        {
            try
            {
                DataSet table = Connection.GetTable(query);
                list.Items.Clear();
                for (int i = 0; i < table.Tables[0].Rows.Count; i++)
                {
                    list.Items.Add(table.Tables[0].Rows[i][0].ToString());
                }
            }
            catch
            {
                MessageBox.Show("Данные не были подгружены!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FillCardsServices(string query)
        {
            try
            {
                DataSet table = Connection.GetTable(query);
                panel2.Controls.Clear();
                for (int i = 0; i < table.Tables[0].Rows.Count; i++)
                {
                    Panel panel = new Panel() { Height = 50, Dock = DockStyle.Top, BackColor = Color.White };
                    CardServices cardServices = new CardServices(
                        table.Tables[0].Rows[i][0].ToString(),
                          table.Tables[0].Rows[i][1].ToString(),
                            table.Tables[0].Rows[i][2].ToString(),
                            table.Tables[0].Rows[i][4].ToString(),
                              Convert.ToInt32(table.Tables[0].Rows[i][3]).ToString() + " ₽ / м²",
                              account

                        );
                    cardServices.Dock = DockStyle.Top;
                    panel2.Controls.Add(panel);
                    panel2.Controls.Add(cardServices);
                }
            }
            catch
            {
                MessageBox.Show("Данные не были подгружены!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

         private void FormServices_Load(object sender, EventArgs e)
        {
         
        }

        private void ComboBoxCategoryServices_SelectedIndexChanged(object sender, EventArgs e)
        {
            LabelService.Text = ComboBoxCategoryServices.Text;
            FillCardsServices($"select * from ServicesCategories where category = '{ComboBoxCategoryServices.Text}' order by id desc");
            
        }
    }
}
