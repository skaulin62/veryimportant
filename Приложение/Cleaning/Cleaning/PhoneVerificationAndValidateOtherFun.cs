﻿

using Dadata;
using Dadata.Model;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Cleaning
{
    public class PhoneVerificationAndValidateOtherFun
    {
        // отправка кода по номеру телефона
        // можно отправить код только на подтвержденные на сервисе номера
        //public string GetAndSendCodeConfirm(string phone)
        //{
        //    string code = null;
        //    Random random = new Random();
        //    code = random.Next(1111, 9999).ToString();
        //    string accountSid =  Environment.GetEnvironmentVariable("AC4e875809351acbebc0a84da5499fd784");
        //    string authToken = Environment.GetEnvironmentVariable("715dd58e4cd11124cf89eb64000bca34");

        //    TwilioClient.Init(accountSid, authToken);

        //    var message = MessageResource.Create(
        //        body: "Ваш код подтверждения: " + code,
        //        from: new Twilio.Types.PhoneNumber("+18454030736"),
        //        to: new Twilio.Types.PhoneNumber("+" + phone)
        //    );
        //    return code;
        //}


         public string GetAndSendCodeConfirm(string phone)
        {
            string code = null;
            Random random = new Random();
            code = random.Next(1111, 9999).ToString();
            string send = $"Cleaning! Ваш код подтверждения: {code}";
            string to = phone;                
            string _from = "";
            string apikey = "6X04QS9K74BKC30032168PG393H7Z52XYC5YS676B6O85YX1D690H6K29L0TWK9F";
            string url = "http://smspilot.ru/api.php" +
                "?send=" + Uri.EscapeUriString(send) +
                "&to=" + to +
                "&from=" + _from +
                "&apikey=" + apikey +
                "&format=v";

            HttpWebResponse myHttpWebResponse;
            string status = string.Empty;
            try
            {
                HttpWebRequest myHttpWebRequest = (HttpWebRequest)HttpWebRequest.Create(url);
           
                // выполняем запрос

                myHttpWebResponse = (HttpWebResponse)myHttpWebRequest.GetResponse();
                //status = myHttpWebResponse.Headers["status"].ToString();
                StreamReader streamReader = new StreamReader(myHttpWebResponse.GetResponseStream());
                status = streamReader.ReadToEnd();

            } catch
            {
                return "-1";
            }
         
            if(status == "Invalid phone" || status == "Antispam (use business account, templates or whitlist)" || status == "Invalid phone length")
            {
                return null;
            }
   
      
            return code;
          
       
        }
        /// <summary>
        /// валидация пароль
        /// </summary>
        /// <param name="password"></param>
        /// <returns></returns>
        public bool IsValidatePassword(string password)
        {
            if (password.Length < 8) return false;
            if (!password.Any(Char.IsDigit)) return false;
            if (!password.Any(Char.IsUpper)) return false;
            //if (!Regex.IsMatch(password, "[!@#$%^&*]")) return false;
            return true;
        }
        /// <summary>
        /// генератор номер заказа
        /// </summary>
        /// <returns></returns>
        public string GenerateOrderId()
        {
            char[] storeReverseString = DateTime.Now.Ticks.ToString().ToCharArray();
            Array.Reverse(storeReverseString);

            string id_order = DateTime.Now.Day.ToString() + new string(storeReverseString).Substring(0, 7);

            return id_order;
        }
        /// <summary>
        /// Функция для получения хеш-строки
        /// </summary>
        /// <param name="str"></param>
        /// <returns></returns>
        public static string GetHash(string str)
        {
            //переводим строку в байт-массим  
            byte[] bytes = Encoding.Unicode.GetBytes(str);

            //создаем объект для получения средст шифрования  
            MD5CryptoServiceProvider CSP = new MD5CryptoServiceProvider();

            //вычисляем хеш-представление в байтах  
            byte[] byteHash = CSP.ComputeHash(bytes);

            string hash = string.Empty;

            //формируем одну цельную строку из массива  
            foreach (byte b in byteHash)
                hash += string.Format("{0:x2}", b);

            return hash;
        }

        async public static Task<Address> FindAddress(string address)
        {
       
      
            var url = "https://cleaner.dadata.ru/api/v1/clean/address";
            var token = "bc158bc8f592db15610af482990c29820b60dc49";
            var secret = "c9b69f0ec44a61d239fa80579d83b0dc4307b16e";

           
            var api = new CleanClientAsync(token, secret);
            var result = await api.Clean<Address>($"{address}");



            return result;
        }

        async public static Task<SuggestResponse<Address>> FindAddress2(string address)
        {
            var token = "bc158bc8f592db15610af482990c29820b60dc49";
         
            try
            {
              
                var api = new SuggestClientAsync(token);
                var result = await api.SuggestAddress(address);
                return result;
            } catch
            {
                return null;
            }



          
        }
    }
}
