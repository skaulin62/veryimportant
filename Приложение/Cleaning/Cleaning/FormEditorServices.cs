﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class FormEditorServices : Form
    {
        public FormEditorServices()
        {
            InitializeComponent();
            try
            {
                FillComboBox(ComboBoxCategory, "select Наименование from Категории_услуг");
                FillCardsServices("select * from ServicesCategories");
            }
            catch
            {
                MessageBox.Show("Данные не были подгружены!", "Ошибка");
            }
        }
        private void FillComboBox(ComboBox list, string query)
        {
            DataSet table = Connection.GetTable(query);
            list.Items.Clear();
            for (int i = 0; i < table.Tables[0].Rows.Count; i++)
            {
                list.Items.Add(table.Tables[0].Rows[i][0].ToString());
            }
        }

        private void FillCardsServices(string query)
        {
            DataSet table = Connection.GetTable(query);
            guna2Panel1.Controls.Clear();
            for (int i = 0; i < table.Tables[0].Rows.Count; i++)
            {
                Panel panel = new Panel() { Height = 10, Dock = DockStyle.Top, BackColor = Color.White };
                CardEditServices cardEditServices = new CardEditServices(
                    table.Tables[0].Rows[i][0].ToString(),
                    "Услуга: " + table.Tables[0].Rows[i][1].ToString(),
                    "Категория: " + table.Tables[0].Rows[i][5].ToString()
                    );


                cardEditServices.Dock = DockStyle.Top;
                guna2Panel1.Controls.Add(panel);
                guna2Panel1.Controls.Add(cardEditServices);
            }
        }

        private void FormEditorServices_Load(object sender, EventArgs e)
        {

        }

        private void guna2TextBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), @"[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextDescription_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), @"[а-яА-Я,.0-9()]") || (Keys)e.KeyChar == Keys.Back || (Keys)e.KeyChar == Keys.Space) return;
            e.Handled = true;
        }

        private void TextNameService_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), @"[а-яА-Я]") || (Keys)e.KeyChar == Keys.Back || (Keys)e.KeyChar == Keys.Space) return;
            e.Handled = true;
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if(TextCategory.Text == "" || TextCategory.Text.Length < 5)
            {
                MessageBox.Show("Не корректно введенное наименование", "Предупреждение");
             } else
            {
                if (Connection.GetTable($"select * from Категории_услуг Where Наименование = '{TextCategory.Text}'").Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Категория с таким наименованием уже существует!", "Предупреждение");
                }   else
                {
                    DialogResult dialogResult = MessageBox.Show("Вы точно хотите добавить категорию", "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                        {
                            SqlCommand sqlCommand = new SqlCommand($"insert into Категории_услуг values('{TextCategory.Text}')", connection); ;
                            try
                            {
                                connection.Open();
                                sqlCommand.ExecuteNonQuery();
                                MessageBox.Show("Категория добавлена!", "Уведомление");
                                TextCategory.Text = "";
                                FillComboBox(ComboBoxCategory, "select Наименование from Категории_услуг");
                            }
                            catch 
                            {
                                MessageBox.Show("Не удалось добавить категорию!", "Ошибка");
                              
                            }
                        }
                    }
                }    
            }
        }

        private void guna2Button3_Click(object sender, EventArgs e)
        {
            // добавление услуги

            if(TextNameService.Text == "" || TextNameService.Text.Length < 5 || TextDescription.Text == "" || TextDescription.Text.Length < 30 || TextCost.Text == "" || Convert.ToDouble(TextCost.Text) < 40 || ComboBoxCategory.Text == "")
            {
                MessageBox.Show("Заполните поля корректно!", "Предупреждение");
            }
            else
            {
                if(Connection.GetTable($"select * from Услуги Where Наименование = '{TextNameService.Text}'").Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Услуга с таким наименованием уже существует!", "Предупреждение");
                } else
                {
                    DialogResult dialogResult = MessageBox.Show("Вы точно хотите добавить услугу", "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                        {
                            int id_cat = Connection.GetIdRowOfObject($"select Код_категории from Категории_услуг where Наименование = '{ComboBoxCategory.Text}'");
                            SqlCommand sqlCommand = new SqlCommand("insert into Услуги(Наименование, Описание, Стоимость, Код_категории)" +
                                $"values('{TextNameService.Text}','{TextDescription.Text}','{TextCost.Text}','{id_cat.ToString()}')", connection);
                            try
                            {
                                connection.Open();
                                sqlCommand.ExecuteNonQuery();
                                MessageBox.Show("Услуга добавлена!", "Уведомление");
                                TextNameService.Text = "";
                                TextDescription.Text = "";
                                TextCost.Text = "";
                                ComboBoxCategory.Text = "";
                                FillCardsServices("select * from ServicesCategories");
                            }
                            catch
                            {

                                MessageBox.Show("Не удалось добавить услугу", "Ошибка");
                            }
                        }
                    }
                }
               
            }
        }
    }
}
