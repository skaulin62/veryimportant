﻿
namespace Cleaning
{
    partial class CardOrderWaitComment
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelDateTime = new System.Windows.Forms.Label();
            this.LabelInfo = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.RatingQualityPersonal = new Guna.UI2.WinForms.Guna2RatingStar();
            this.label8 = new System.Windows.Forms.Label();
            this.TextComment = new Guna.UI2.WinForms.Guna2TextBox();
            this.RatingQualityService = new Guna.UI2.WinForms.Guna2RatingStar();
            this.ButtonSendComment = new Guna.UI2.WinForms.Guna2Button();
            this.SuspendLayout();
            // 
            // LabelDateTime
            // 
            this.LabelDateTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelDateTime.AutoSize = true;
            this.LabelDateTime.BackColor = System.Drawing.Color.Transparent;
            this.LabelDateTime.Font = new System.Drawing.Font("Noto Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelDateTime.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.LabelDateTime.Location = new System.Drawing.Point(666, 16);
            this.LabelDateTime.Name = "LabelDateTime";
            this.LabelDateTime.Size = new System.Drawing.Size(169, 27);
            this.LabelDateTime.TabIndex = 22;
            this.LabelDateTime.Text = "2020-02-03 12:00";
            // 
            // LabelInfo
            // 
            this.LabelInfo.AutoSize = true;
            this.LabelInfo.BackColor = System.Drawing.Color.Transparent;
            this.LabelInfo.Font = new System.Drawing.Font("Noto Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelInfo.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.LabelInfo.Location = new System.Drawing.Point(22, 16);
            this.LabelInfo.Name = "LabelInfo";
            this.LabelInfo.Size = new System.Drawing.Size(615, 27);
            this.LabelInfo.TabIndex = 18;
            this.LabelInfo.Text = "Заказ: 216123152 • Генеральная уборка • 145 кв. м • 10000 руб.";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Noto Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(34, 52);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(268, 27);
            this.label5.TabIndex = 24;
            this.label5.Text = "Оцените качество уборки";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Noto Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(352, 52);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(371, 27);
            this.label7.TabIndex = 26;
            this.label7.Text = "Оцените дружелюбность персонала";
            // 
            // RatingQualityPersonal
            // 
            this.RatingQualityPersonal.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.RatingQualityPersonal.FillColor = System.Drawing.Color.Transparent;
            this.RatingQualityPersonal.Location = new System.Drawing.Point(356, 84);
            this.RatingQualityPersonal.Name = "RatingQualityPersonal";
            this.RatingQualityPersonal.RatingColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.RatingQualityPersonal.Size = new System.Drawing.Size(183, 37);
            this.RatingQualityPersonal.TabIndex = 25;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.Color.Transparent;
            this.label8.Font = new System.Drawing.Font("Noto Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(34, 128);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(169, 27);
            this.label8.TabIndex = 27;
            this.label8.Text = "Оставьте отзыв";
            // 
            // TextComment
            // 
            this.TextComment.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TextComment.AutoScroll = true;
            this.TextComment.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextComment.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextComment.DefaultText = "";
            this.TextComment.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextComment.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextComment.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextComment.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextComment.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextComment.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextComment.ForeColor = System.Drawing.Color.Black;
            this.TextComment.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextComment.Location = new System.Drawing.Point(38, 157);
            this.TextComment.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TextComment.MaxLength = 200;
            this.TextComment.Multiline = true;
            this.TextComment.Name = "TextComment";
            this.TextComment.PasswordChar = '\0';
            this.TextComment.PlaceholderText = "...";
            this.TextComment.SelectedText = "";
            this.TextComment.Size = new System.Drawing.Size(610, 54);
            this.TextComment.TabIndex = 28;
            // 
            // RatingQualityService
            // 
            this.RatingQualityService.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.RatingQualityService.FillColor = System.Drawing.Color.Transparent;
            this.RatingQualityService.Location = new System.Drawing.Point(38, 84);
            this.RatingQualityService.Name = "RatingQualityService";
            this.RatingQualityService.RatingColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.RatingQualityService.Size = new System.Drawing.Size(183, 37);
            this.RatingQualityService.TabIndex = 29;
            // 
            // ButtonSendComment
            // 
            this.ButtonSendComment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonSendComment.Animated = true;
            this.ButtonSendComment.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonSendComment.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonSendComment.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonSendComment.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonSendComment.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.ButtonSendComment.Font = new System.Drawing.Font("Noto Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonSendComment.ForeColor = System.Drawing.Color.White;
            this.ButtonSendComment.Location = new System.Drawing.Point(671, 157);
            this.ButtonSendComment.Name = "ButtonSendComment";
            this.ButtonSendComment.Size = new System.Drawing.Size(167, 54);
            this.ButtonSendComment.TabIndex = 30;
            this.ButtonSendComment.Text = "Отправить";
            this.ButtonSendComment.Click += new System.EventHandler(this.ButtonSendComment_Click);
            // 
            // CardOrderWaitComment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.ButtonSendComment);
            this.Controls.Add(this.RatingQualityService);
            this.Controls.Add(this.TextComment);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.RatingQualityPersonal);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.LabelDateTime);
            this.Controls.Add(this.LabelInfo);
            this.Name = "CardOrderWaitComment";
            this.Size = new System.Drawing.Size(852, 224);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelDateTime;
        private System.Windows.Forms.Label LabelInfo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private Guna.UI2.WinForms.Guna2RatingStar RatingQualityPersonal;
        private System.Windows.Forms.Label label8;
        private Guna.UI2.WinForms.Guna2TextBox TextComment;
        private Guna.UI2.WinForms.Guna2RatingStar RatingQualityService;
        private Guna.UI2.WinForms.Guna2Button ButtonSendComment;
    }
}
