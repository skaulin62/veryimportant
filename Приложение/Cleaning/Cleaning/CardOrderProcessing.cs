﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class CardOrderProcessing : UserControl
    {
        public CardOrderProcessing(string id_order, string service, string cost, string count, string status, string datetime)
        {
            InitializeComponent();
            guna2Button1.Name = id_order;
            label2.Text = id_order;
            label1.Text = service;
            label3.Text = count + " кв. м";
            label4.Text = (Convert.ToInt32(cost)).ToString() + " руб.";
            label5.Text = status;
            label6.Text = datetime.Substring(0, 16);
        }

        private void CardOrderProcessing_Load(object sender, EventArgs e)
        {

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
         
        }
    }
}
