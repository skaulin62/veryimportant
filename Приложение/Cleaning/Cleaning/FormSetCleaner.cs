﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class FormSetCleaner : Form
    {
        public FormSetCleaner(Account account)
        {
            InitializeComponent();
            this.account = account;
            FillComboBox(ComboBoxCleaner, "select (surname + ' ' + name) from EmployeeRole where role = 'Уборщик' and [delete] = 0");
            FillCardSetCleaner("select * from orders_decline where [status] = 'В обработке'");
        }
        Account account;

        private void FillComboBox(ComboBox list, string query)
        {
           
            try
            {
                DataSet table = Connection.GetTable(query);
                list.Items.Clear();
                for (int i = 0; i < table.Tables[0].Rows.Count; i++)
                {
                    list.Items.Add(table.Tables[0].Rows[i][0].ToString());
                }
            }
           
            catch
            {
                MessageBox.Show("Данные не были подгружены!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
           
        }

        private void FillCardSetCleaner(string query)
        {
            DataSet table;
            try
            {
                table = Connection.GetTable(query);
                panel1.Controls.Clear();
                for (int i = 0; i < table.Tables[0].Rows.Count; i++)
                {
                    Panel panel = new Panel() { Height = 20, Dock = DockStyle.Top, BackColor = Color.White };
                    CardOperator cardOperator = new CardOperator(
                        table.Tables[0].Rows[i][0].ToString(),
                        table.Tables[0].Rows[i][2].ToString(),
                        table.Tables[0].Rows[i][14].ToString(),
                        table.Tables[0].Rows[i][3].ToString(),
                        table.Tables[0].Rows[i][5].ToString(),
                        Convert.ToInt32(table.Tables[0].Rows[i][4]).ToString(),
                        table.Tables[0].Rows[i][11].ToString(),
                        table.Tables[0].Rows[i][10].ToString(),
                        table.Tables[0].Rows[i][15].ToString()
                        );
                    Guna.UI2.WinForms.Guna2Button guna2Button = cardOperator.Controls.Find(table.Tables[0].Rows[i][0].ToString(), true).FirstOrDefault() as Guna.UI2.WinForms.Guna2Button;
                    guna2Button.Click += new EventHandler(ButtonSetCleaner);
                    cardOperator.Dock = DockStyle.Top;
                    panel1.Controls.Add(panel);
                    panel1.Controls.Add(cardOperator);
                }
            } catch
            {
                MessageBox.Show("Данные не были подгружены!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
               
            
          
            
        }

        private void ButtonSetCleaner(object sender, EventArgs e)
        {
            if(ComboBoxCleaner.Text == "")
            {
                MessageBox.Show("Выберите уборщика!", "Предупреждение");
            } else
            {
                string order = (sender as Guna.UI2.WinForms.Guna2Button).Name;
                string id_cleaner = "";
                try
                {
                    id_cleaner = Connection.GetIdRowOfObject($"select id from EmployeeRole where role = 'Уборщик' and (surname + ' ' + name) = '{ComboBoxCleaner.Text}'").ToString();
                } catch
                {
                    MessageBox.Show("Данные не были подгружены!", "Ошибка");
                }
               
                string datetime = (sender as Guna.UI2.WinForms.Guna2Button).Tag.ToString();

                DataSet checkEmployment = new DataSet();
                try
                {
                    checkEmployment = Connection.GetTable($"select * from Заказы Where Код_уборщика = '{id_cleaner}' and Дата_уборки = '{datetime}'");
                } catch
                {
                    MessageBox.Show("Данные не были подгружены!", "Ошибка");
                }

                if(checkEmployment.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Данный уборщик занят на данное время, укажите другого!", "Предупреждение");
                } else
                {
                    DialogResult dialogResult = MessageBox.Show("Уверены, что хотите назначить уборщика?", "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                        {

                            SqlCommand sqlCommand = new SqlCommand($"update Заказы set Код_уборщика = '{id_cleaner}', Код_оператора = '{account.id}', Статус = 'Ожидание выполнения' where Номер_заказа = '{order}'", connection);

                            try
                            {
                                connection.Open();
                                sqlCommand.ExecuteNonQuery();
                                MessageBox.Show("Уборщик назначен!", "Уведомление");
                                FillCardSetCleaner("select * from orders_decline where [status] = 'В обработке'");
                            }
                            catch
                            {
                                MessageBox.Show("Не удалось назначить уборщика!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                    }
                }

               
            }

        }
    }
}
