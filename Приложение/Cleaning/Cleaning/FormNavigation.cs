﻿using Cleaning.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class FormNavigation : Form
    {

        
        public FormNavigation(Account account)
        {
            InitializeComponent();
            this.account = account;
            LabelWelcome.Text = "Добро пожаловать, \n" + this.account.surname + " " + this.account.name;
            DisableButtonMenu();
            if (this.account.role == "Клиент")
            {
            
                ButtonClientOrders.Visible = true;
                ButtonClientServices.Visible = true;

            } else if (this.account.role == "Администратор")
            {
                ButtonAdminEmployees.Visible = true;
                ButtonAdminServices.Visible = true;
                ButtonAdminStatisticAndReport.Visible = true;
            } else if (this.account.role == "Уборщик")
            {
                ButtonCleanerConfirmOrder.Visible = true;
            } else if (this.account.role == "Оператор")
            {
                ButtonOperatorSetCleaner.Visible = true;
            }
    
        }

        Account account = null;

    
          
        private void guna2ImageButton1_Click(object sender, EventArgs e)
        {
            FormSignIn formSignIn = new FormSignIn();
            formSignIn.Show();
            this.Hide();
        }

        private void guna2ImageButton3_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void guna2ImageButton2_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized) {
                this.WindowState = FormWindowState.Normal;
                guna2ImageButton2.Image = Properties.Resources.expend1;
            } 
            else
            {
                guna2ImageButton2.Image = Properties.Resources.expend2;
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormSignIn formSignIn = new FormSignIn();
            formSignIn.Show();
        }

        private void FormNavigation_Load(object sender, EventArgs e)
        {

        }
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        private void ButtonClientOrders_Click(object sender, EventArgs e)
        {
            OpenForm(new FormMyOrders(account));
        }
        Form currentForm = null;
        private void OpenForm(Form form)
        {

            if (currentForm != form) currentForm = null; 
                currentForm = form;
            currentForm.Dock = DockStyle.Fill;
                currentForm.TopLevel = false;
                currentForm.TopMost = true;
                currentForm.Show();



            PanelContent.Controls.Clear();
            PanelContent.Controls.Add(currentForm);
        }

        private void ButtonClientServices_Click(object sender, EventArgs e)
        {
            OpenForm(new FormServices(this.account.id.ToString()));
        }

        private void DisableButtonMenu()
        {
            ButtonClientOrders.Visible = false;
            ButtonClientServices.Visible = false;
            ButtonAdminEmployees.Visible = false;
            ButtonAdminServices.Visible = false;
            ButtonAdminStatisticAndReport.Visible = false;
            ButtonOperatorSetCleaner.Visible = false;
            ButtonCleanerConfirmOrder.Visible = false;
        }

        private void ButtonAdminEmployees_Click(object sender, EventArgs e)
        {
            OpenForm(new FormEditorEmployee());
        }

        private void ButtonAdminServices_Click(object sender, EventArgs e)
        {
            OpenForm(new FormEditorServices());
        }

        private void LabelProfile_Click(object sender, EventArgs e)
        {
            if(account.role == "Клиент")
            {
                OpenForm(new FormProfile(account));
            }
         
        }

        private void ButtonAdminStatisticAndReport_Click(object sender, EventArgs e)
        {
            OpenForm(new FormReport());
        }

        private void ButtonOperatorSetCleaner_Click(object sender, EventArgs e)
        {
            OpenForm(new FormSetCleaner(account));
        }

        private void ButtonCleanerConfirmOrder_Click(object sender, EventArgs e)
        {
            OpenForm(new FormCleanerConfirm(account));
        }

        private void FormNavigation_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.Hide();
            FormSignIn formSignIn = new FormSignIn();
            formSignIn.Show();
        }
    }
}
