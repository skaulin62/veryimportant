﻿
namespace Cleaning
{
    partial class FormEditorEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtonAddEmployee = new Guna.UI2.WinForms.Guna2Button();
            this.label7 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LabelService = new System.Windows.Forms.Label();
            this.guna2TextBox2 = new Guna.UI2.WinForms.Guna2TextBox();
            this.TextSurname = new Guna.UI2.WinForms.Guna2TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.TextName = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.ComboBoxRole = new Guna.UI2.WinForms.Guna2ComboBox();
            this.TextPhone = new Guna.UI2.WinForms.Guna2TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.guna2TextBox4 = new Guna.UI2.WinForms.Guna2TextBox();
            this.TextLastname = new Guna.UI2.WinForms.Guna2TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.TextLogin = new Guna.UI2.WinForms.Guna2TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.TextPassword = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            this.label6 = new System.Windows.Forms.Label();
            this.Date = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.label5 = new System.Windows.Forms.Label();
            this.TextNumberPassport = new Guna.UI2.WinForms.Guna2TextBox();
            this.TextSeriosPassport = new Guna.UI2.WinForms.Guna2TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.SwitchHideViewPassword = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.SwitchHideViewPassword)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonAddEmployee
            // 
            this.ButtonAddEmployee.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonAddEmployee.Animated = true;
            this.ButtonAddEmployee.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonAddEmployee.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonAddEmployee.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonAddEmployee.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonAddEmployee.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.ButtonAddEmployee.Font = new System.Drawing.Font("Noto Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonAddEmployee.ForeColor = System.Drawing.Color.White;
            this.ButtonAddEmployee.Location = new System.Drawing.Point(798, 228);
            this.ButtonAddEmployee.Name = "ButtonAddEmployee";
            this.ButtonAddEmployee.Size = new System.Drawing.Size(203, 38);
            this.ButtonAddEmployee.TabIndex = 10;
            this.ButtonAddEmployee.Text = "Добавить";
            this.ButtonAddEmployee.Click += new System.EventHandler(this.ButtonAddEmployee_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Noto Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(793, 127);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(113, 26);
            this.label7.TabIndex = 167;
            this.label7.Text = "Должность";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.panel1.Location = new System.Drawing.Point(101, 97);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(65, 3);
            this.panel1.TabIndex = 164;
            // 
            // LabelService
            // 
            this.LabelService.AutoSize = true;
            this.LabelService.Font = new System.Drawing.Font("Noto Sans", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelService.Location = new System.Drawing.Point(93, 40);
            this.LabelService.Name = "LabelService";
            this.LabelService.Size = new System.Drawing.Size(212, 41);
            this.LabelService.TabIndex = 163;
            this.LabelService.Text = "Сотрудники";
            // 
            // guna2TextBox2
            // 
            this.guna2TextBox2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.guna2TextBox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox2.DefaultText = "";
            this.guna2TextBox2.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox2.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox2.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox2.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.guna2TextBox2.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox2.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox2.Location = new System.Drawing.Point(162, 246);
            this.guna2TextBox2.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.guna2TextBox2.MaxLength = 32;
            this.guna2TextBox2.Name = "guna2TextBox2";
            this.guna2TextBox2.PasswordChar = '\0';
            this.guna2TextBox2.PlaceholderText = "150";
            this.guna2TextBox2.SelectedText = "";
            this.guna2TextBox2.Size = new System.Drawing.Size(0, 0);
            this.guna2TextBox2.TabIndex = 162;
            // 
            // TextSurname
            // 
            this.TextSurname.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextSurname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSurname.DefaultText = "";
            this.TextSurname.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextSurname.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextSurname.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextSurname.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextSurname.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextSurname.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextSurname.ForeColor = System.Drawing.Color.Black;
            this.TextSurname.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextSurname.Location = new System.Drawing.Point(94, 156);
            this.TextSurname.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextSurname.MaxLength = 32;
            this.TextSurname.Name = "TextSurname";
            this.TextSurname.PasswordChar = '\0';
            this.TextSurname.PlaceholderText = "Иванов";
            this.TextSurname.SelectedText = "";
            this.TextSurname.Size = new System.Drawing.Size(283, 38);
            this.TextSurname.TabIndex = 0;
            this.TextSurname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextService_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label3.Location = new System.Drawing.Point(89, 127);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(98, 26);
            this.label3.TabIndex = 157;
            this.label3.Text = "Фамилия";
            // 
            // TextName
            // 
            this.TextName.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextName.DefaultText = "";
            this.TextName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextName.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextName.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextName.ForeColor = System.Drawing.Color.Black;
            this.TextName.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextName.Location = new System.Drawing.Point(94, 228);
            this.TextName.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextName.MaxLength = 32;
            this.TextName.Name = "TextName";
            this.TextName.PasswordChar = '\0';
            this.TextName.PlaceholderText = "Иван";
            this.TextName.SelectedText = "";
            this.TextName.Size = new System.Drawing.Size(283, 38);
            this.TextName.TabIndex = 1;
            this.TextName.TextChanged += new System.EventHandler(this.TextName_TextChanged);
            this.TextName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextName_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label4.Location = new System.Drawing.Point(89, 198);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 26);
            this.label4.TabIndex = 169;
            this.label4.Text = "Имя";
            // 
            // ComboBoxRole
            // 
            this.ComboBoxRole.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboBoxRole.BackColor = System.Drawing.Color.Transparent;
            this.ComboBoxRole.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.ComboBoxRole.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ComboBoxRole.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxRole.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.ComboBoxRole.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.ComboBoxRole.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ComboBoxRole.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.ComboBoxRole.ItemHeight = 30;
            this.ComboBoxRole.Location = new System.Drawing.Point(798, 156);
            this.ComboBoxRole.Name = "ComboBoxRole";
            this.ComboBoxRole.Size = new System.Drawing.Size(203, 36);
            this.ComboBoxRole.TabIndex = 9;
            // 
            // TextPhone
            // 
            this.TextPhone.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.TextPhone.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextPhone.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextPhone.DefaultText = "";
            this.TextPhone.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextPhone.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextPhone.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPhone.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPhone.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPhone.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextPhone.ForeColor = System.Drawing.Color.Black;
            this.TextPhone.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPhone.Location = new System.Drawing.Point(94, 373);
            this.TextPhone.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextPhone.MaximumSize = new System.Drawing.Size(283, 38);
            this.TextPhone.MaxLength = 11;
            this.TextPhone.MinimumSize = new System.Drawing.Size(283, 38);
            this.TextPhone.Name = "TextPhone";
            this.TextPhone.PasswordChar = '\0';
            this.TextPhone.PlaceholderText = "7...";
            this.TextPhone.SelectedText = "";
            this.TextPhone.Size = new System.Drawing.Size(283, 38);
            this.TextPhone.TabIndex = 3;
            this.TextPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextPhone_KeyPress);
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label1.Location = new System.Drawing.Point(89, 343);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(169, 26);
            this.label1.TabIndex = 174;
            this.label1.Text = "Номер телефона";
            // 
            // guna2TextBox4
            // 
            this.guna2TextBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.guna2TextBox4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.guna2TextBox4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox4.DefaultText = "";
            this.guna2TextBox4.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.guna2TextBox4.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox4.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.Location = new System.Drawing.Point(162, 391);
            this.guna2TextBox4.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.guna2TextBox4.MaxLength = 32;
            this.guna2TextBox4.Name = "guna2TextBox4";
            this.guna2TextBox4.PasswordChar = '\0';
            this.guna2TextBox4.PlaceholderText = "150";
            this.guna2TextBox4.SelectedText = "";
            this.guna2TextBox4.Size = new System.Drawing.Size(0, 0);
            this.guna2TextBox4.TabIndex = 173;
            // 
            // TextLastname
            // 
            this.TextLastname.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextLastname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextLastname.DefaultText = "";
            this.TextLastname.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextLastname.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextLastname.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextLastname.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextLastname.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextLastname.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextLastname.ForeColor = System.Drawing.Color.Black;
            this.TextLastname.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextLastname.Location = new System.Drawing.Point(94, 300);
            this.TextLastname.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextLastname.MaxLength = 32;
            this.TextLastname.Name = "TextLastname";
            this.TextLastname.PasswordChar = '\0';
            this.TextLastname.PlaceholderText = "Иванович";
            this.TextLastname.SelectedText = "";
            this.TextLastname.Size = new System.Drawing.Size(283, 38);
            this.TextLastname.TabIndex = 2;
            this.TextLastname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextLastname_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label2.Location = new System.Drawing.Point(89, 271);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(99, 26);
            this.label2.TabIndex = 171;
            this.label2.Text = "Отчество";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Ubuntu Light", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.label11.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label11.Location = new System.Drawing.Point(279, 277);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(98, 16);
            this.label11.TabIndex = 176;
            this.label11.Text = "(дополнительно)";
            // 
            // TextLogin
            // 
            this.TextLogin.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextLogin.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextLogin.DefaultText = "";
            this.TextLogin.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextLogin.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextLogin.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextLogin.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextLogin.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextLogin.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextLogin.ForeColor = System.Drawing.Color.Black;
            this.TextLogin.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextLogin.Location = new System.Drawing.Point(404, 156);
            this.TextLogin.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextLogin.MaxLength = 32;
            this.TextLogin.Name = "TextLogin";
            this.TextLogin.PasswordChar = '\0';
            this.TextLogin.PlaceholderText = "login123";
            this.TextLogin.SelectedText = "";
            this.TextLogin.Size = new System.Drawing.Size(366, 38);
            this.TextLogin.TabIndex = 7;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label8.Location = new System.Drawing.Point(399, 127);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(68, 26);
            this.label8.TabIndex = 184;
            this.label8.Text = "Логин";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label9.Location = new System.Drawing.Point(399, 197);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(82, 26);
            this.label9.TabIndex = 186;
            this.label9.Text = "Пароль";
            // 
            // TextPassword
            // 
            this.TextPassword.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextPassword.DefaultText = "";
            this.TextPassword.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextPassword.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextPassword.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPassword.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPassword.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPassword.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextPassword.ForeColor = System.Drawing.Color.Black;
            this.TextPassword.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPassword.Location = new System.Drawing.Point(404, 228);
            this.TextPassword.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextPassword.MaxLength = 32;
            this.TextPassword.Name = "TextPassword";
            this.TextPassword.PasswordChar = '\0';
            this.TextPassword.PlaceholderText = "LKO@P#31dsa";
            this.TextPassword.SelectedText = "";
            this.TextPassword.Size = new System.Drawing.Size(366, 38);
            this.TextPassword.TabIndex = 8;
            // 
            // guna2Panel1
            // 
            this.guna2Panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2Panel1.AutoScroll = true;
            this.guna2Panel1.Location = new System.Drawing.Point(404, 332);
            this.guna2Panel1.Name = "guna2Panel1";
            this.guna2Panel1.Size = new System.Drawing.Size(597, 219);
            this.guna2Panel1.TabIndex = 187;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label6.Location = new System.Drawing.Point(93, 484);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(155, 26);
            this.label6.TabIndex = 192;
            this.label6.Text = "Дата рождения";
            // 
            // Date
            // 
            this.Date.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Date.Animated = true;
            this.Date.BackColor = System.Drawing.Color.Transparent;
            this.Date.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.Date.BorderThickness = 1;
            this.Date.Checked = true;
            this.Date.FillColor = System.Drawing.Color.White;
            this.Date.Font = new System.Drawing.Font("Noto Sans", 10.2F);
            this.Date.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.Date.Location = new System.Drawing.Point(94, 513);
            this.Date.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.Date.MaximumSize = new System.Drawing.Size(283, 38);
            this.Date.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.Date.MinimumSize = new System.Drawing.Size(283, 38);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(283, 38);
            this.Date.TabIndex = 6;
            this.Date.Value = new System.DateTime(2022, 2, 13, 0, 59, 13, 775);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label5.Location = new System.Drawing.Point(89, 414);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 26);
            this.label5.TabIndex = 190;
            this.label5.Text = "Паспорт";
            // 
            // TextNumberPassport
            // 
            this.TextNumberPassport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.TextNumberPassport.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextNumberPassport.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextNumberPassport.DefaultText = "";
            this.TextNumberPassport.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextNumberPassport.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextNumberPassport.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextNumberPassport.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextNumberPassport.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextNumberPassport.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextNumberPassport.ForeColor = System.Drawing.Color.Black;
            this.TextNumberPassport.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextNumberPassport.Location = new System.Drawing.Point(213, 443);
            this.TextNumberPassport.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextNumberPassport.MaximumSize = new System.Drawing.Size(164, 38);
            this.TextNumberPassport.MaxLength = 6;
            this.TextNumberPassport.MinimumSize = new System.Drawing.Size(164, 38);
            this.TextNumberPassport.Name = "TextNumberPassport";
            this.TextNumberPassport.PasswordChar = '\0';
            this.TextNumberPassport.PlaceholderText = "Номер";
            this.TextNumberPassport.SelectedText = "";
            this.TextNumberPassport.Size = new System.Drawing.Size(164, 38);
            this.TextNumberPassport.TabIndex = 5;
            this.TextNumberPassport.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextNumberPassport_KeyPress);
            // 
            // TextSeriosPassport
            // 
            this.TextSeriosPassport.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.TextSeriosPassport.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextSeriosPassport.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSeriosPassport.DefaultText = "";
            this.TextSeriosPassport.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextSeriosPassport.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextSeriosPassport.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextSeriosPassport.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextSeriosPassport.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextSeriosPassport.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextSeriosPassport.ForeColor = System.Drawing.Color.Black;
            this.TextSeriosPassport.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextSeriosPassport.Location = new System.Drawing.Point(94, 443);
            this.TextSeriosPassport.Margin = new System.Windows.Forms.Padding(4, 3, 4, 3);
            this.TextSeriosPassport.MaximumSize = new System.Drawing.Size(111, 38);
            this.TextSeriosPassport.MaxLength = 4;
            this.TextSeriosPassport.MinimumSize = new System.Drawing.Size(111, 38);
            this.TextSeriosPassport.Name = "TextSeriosPassport";
            this.TextSeriosPassport.PasswordChar = '\0';
            this.TextSeriosPassport.PlaceholderText = "Серия";
            this.TextSeriosPassport.SelectedText = "";
            this.TextSeriosPassport.Size = new System.Drawing.Size(111, 38);
            this.TextSeriosPassport.TabIndex = 4;
            this.TextSeriosPassport.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextSeriosPassport_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Noto Sans", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label12.Location = new System.Drawing.Point(398, 287);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(274, 33);
            this.label12.TabIndex = 194;
            this.label12.Text = "Список сотрудников";
            // 
            // SwitchHideViewPassword
            // 
            this.SwitchHideViewPassword.Image = global::Cleaning.Properties.Resources.hidden;
            this.SwitchHideViewPassword.Location = new System.Drawing.Point(740, 235);
            this.SwitchHideViewPassword.Name = "SwitchHideViewPassword";
            this.SwitchHideViewPassword.Size = new System.Drawing.Size(22, 22);
            this.SwitchHideViewPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.SwitchHideViewPassword.TabIndex = 195;
            this.SwitchHideViewPassword.TabStop = false;
            this.SwitchHideViewPassword.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SwitchHideViewPassword_MouseDown);
            this.SwitchHideViewPassword.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SwitchHideViewPassword_MouseUp);
            // 
            // FormEditorEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1094, 614);
            this.Controls.Add(this.SwitchHideViewPassword);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Date);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.TextNumberPassport);
            this.Controls.Add(this.TextSeriosPassport);
            this.Controls.Add(this.guna2Panel1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TextPassword);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.TextLogin);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.TextPhone);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.guna2TextBox4);
            this.Controls.Add(this.TextLastname);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.TextName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.ButtonAddEmployee);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ComboBoxRole);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LabelService);
            this.Controls.Add(this.guna2TextBox2);
            this.Controls.Add(this.TextSurname);
            this.Controls.Add(this.label3);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormEditorEmployee";
            this.Padding = new System.Windows.Forms.Padding(90, 40, 90, 40);
            this.Text = "FormEditorEmployee";
            ((System.ComponentModel.ISupportInitialize)(this.SwitchHideViewPassword)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Button ButtonAddEmployee;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label LabelService;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox2;
        private Guna.UI2.WinForms.Guna2TextBox TextSurname;
        private System.Windows.Forms.Label label3;
        private Guna.UI2.WinForms.Guna2TextBox TextName;
        private System.Windows.Forms.Label label4;
        private Guna.UI2.WinForms.Guna2ComboBox ComboBoxRole;
        private Guna.UI2.WinForms.Guna2TextBox TextPhone;
        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox4;
        private Guna.UI2.WinForms.Guna2TextBox TextLastname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private Guna.UI2.WinForms.Guna2TextBox TextLogin;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private Guna.UI2.WinForms.Guna2TextBox TextPassword;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private System.Windows.Forms.Label label6;
        private Guna.UI2.WinForms.Guna2DateTimePicker Date;
        private System.Windows.Forms.Label label5;
        private Guna.UI2.WinForms.Guna2TextBox TextNumberPassport;
        private Guna.UI2.WinForms.Guna2TextBox TextSeriosPassport;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.PictureBox SwitchHideViewPassword;
    }
}