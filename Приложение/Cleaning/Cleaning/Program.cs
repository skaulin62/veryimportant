﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.ThreadException += new System.Threading.ThreadExceptionEventHandler(ThrowAllError);
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new FormSignIn());
            
        }

        private static void ThrowAllError(object sender, ThreadExceptionEventArgs e)
        {
            MessageBox.Show("Что-то пошло не так, повторите действие чуть позже", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
        }
    }
}
