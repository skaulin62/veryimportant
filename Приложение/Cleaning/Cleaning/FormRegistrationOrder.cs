﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class FormRegistrationOrder : Form
    {
        public FormRegistrationOrder(string id, string name, string cost, string account)
        {
            InitializeComponent();
            DateOrder.MinDate = DateTime.Now.Date.AddDays(1);
            DateOrder.MaxDate = DateTime.Now.Date.AddDays(15);
            DateOrder.Value = DateOrder.MinDate;
            LabelOrderId.Text = "Заказ: " + order;
            TextServiceOrder.Text = name;
            TextCostOrder.Text = cost;
            this.account = account;
            id_service = id;
        }
        string account;
        string order = new PhoneVerificationAndValidateOtherFun().GenerateOrderId();
        string id_service = null;
        private void ButtonCloseForm_Click(object sender, EventArgs e)
        {
            this.Hide();
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if(TextCountOrder.Text == "" || ComboBoxTimeOrder.Text == "" || int.Parse(TextCountOrder.Text) <= 2)
            {
                MessageBox.Show("Выберите дату и введите площадь превышающая 2 кв. метров!", "Предупреждение");
            
            } else
            {
                string datetimeWork = DateOrder.Value.Date.ToShortDateString() + " " + ComboBoxTimeOrder.Text;
                DataSet checkOrders = new DataSet();
                try {
                    checkOrders = Connection.GetTable($"Select * from Заказы Where Код_клиента = '{account}' and Дата_уборки = convert(datetime, '{datetimeWork}', 105)");

                }
                catch
                {
                    MessageBox.Show("Данные не были подгружены!", "Ошибка");
                }
              
               
                if(checkOrders.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("Нельзя повторно заказать услугу на эту же дату и время", "Предупреждение");
                } else
                {
                    DialogResult dialogResult = MessageBox.Show("Вы уверены, что хотите оформить заказ?", "Подтверждение заказа " + order, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (dialogResult == DialogResult.Yes)
                    {
                        using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                        {

                            SqlCommand sqlCommand = new SqlCommand("insert into Заказы(Номер_заказа, Код_клиента, Код_услуги, Общая_стоимость, Количество, Примечание, Дата_уборки, Статус)"
                                + $"values('{order}', '{account}','{id_service}','{cost}', '{TextCountOrder.Text}','{TextDescription.Text}', convert(datetime, '{datetimeWork}', 105), 'В обработке')", connection);  // дописать
                            try
                            {
                                connection.Open();
                                sqlCommand.ExecuteNonQuery();
                                MessageBox.Show("Заказ оформлен, ожидайте исполнения.", "Уведомление");
                                this.Hide();
                            }
                            catch 
                            {
                                MessageBox.Show("Не получилось оформить заказ!", "Ошибка");
                            }
                        }
                    }
                }

              
            }
        }

        private void TextCountOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), @"[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        int cost = 0;
        private void TextCountOrder_TextChanged(object sender, EventArgs e)
        {
            if(TextCountOrder.Text == "" || TextCountOrder.Text == null)
            {
                 cost = 0;
            } else
            {
                cost = Convert.ToInt32(TextCountOrder.Text) * Convert.ToInt32(TextCostOrder.Text);
            }

            LabelTotalCost.Text = "Общая стоимость: " + cost.ToString() + " руб.";
        }
    }
}
