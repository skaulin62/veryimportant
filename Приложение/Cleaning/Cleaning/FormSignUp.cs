﻿using Dadata;
using Dadata.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class FormSignUp : Form
    {
        public FormSignUp()
        {
            InitializeComponent();
            TextPassword.UseSystemPasswordChar = true;
            SwitchHideViewPassword.MouseDown += new MouseEventHandler(ShowPassword);
            SwitchHideViewPassword.MouseUp += new MouseEventHandler(HidePassword);
            Date.MaxDate = DateTime.Now.Date.AddYears(-16);
            Date.Value = Date.MaxDate;
        }

        /// необходимые переменные
        bool confirmCode = false;
        string codeConfirm = null;
        string phone = null;

        bool blockBtnGetCode = false;

        private void HidePassword(object sender, EventArgs e)
        {
            TextPassword.UseSystemPasswordChar = true;
            SwitchHideViewPassword.Image = Properties.Resources.hidden;
        }
        private void ShowPassword(object sender, EventArgs e)
        {
            TextPassword.UseSystemPasswordChar = false;
            SwitchHideViewPassword.Image = Properties.Resources.view;
        }

        private void splitContainer1_Panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormSignIn formSignIn = new FormSignIn();
            formSignIn.Show();
           
        }
        
        private void LabelCaption_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void ButtonCloseForm_Click(object sender, EventArgs e)
        {
            this.Hide();
            FormSignIn formSignIn = new FormSignIn();
            formSignIn.Show();
            
        }

        private void ButtonToFormSignIn_Click(object sender, EventArgs e)
        {
            FormSignIn formSignIn = new FormSignIn();
            formSignIn.Show();
            this.Hide();
        }

        private void ButtonSignIn_Click(object sender, EventArgs e)
        {

        }

        private void guna2Button1_Click_1(object sender, EventArgs e)
        {

            if (TextName.Text == "" || TextSurname.Text == "" || TextSeriosPassport.Text == "" || TextNumberPassport.Text == "" || TextPassword.Text == "" || comboBox1.Text == "" || TextSeriosPassport.Text.Length != 4 || TextNumberPassport.Text.Length != 6 || comboBox1.Text.Length < 10 || TextPhone.Text == "")
            {

                MessageBox.Show("Заполните все поля корректно!", "Предупреждение");

            } else if (new PhoneVerificationAndValidateOtherFun().IsValidatePassword(TextPassword.Text) == false)
            {
                MessageBox.Show("Пароль не соответствует требованиям!", "Предупреждение");
            } else if (!confirmCode){
                MessageBox.Show("Подтвердите номер телефона!", "Предупреждение");
            } else if (ButtonAgree.Checked == false)
            {
                MessageBox.Show("Дайте согласие на обработку данных!", "Предупреждение");
            } else {
                using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                {
                    string date = Date.Value.Date.ToShortDateString();
                    SqlCommand sqlCommand = new SqlCommand($"exec AddClient '{TextSurname.Text}', '{TextName.Text}', '{TextLastName.Text}'," +
                        $" '{TextSeriosPassport.Text + TextNumberPassport.Text}','{phone}', '{date}', '{PhoneVerificationAndValidateOtherFun.GetHash(TextPassword.Text)}', '{comboBox1.Text}'", connection);
                    connection.Open();
                    try
                    {

                        sqlCommand.ExecuteNonQuery();
                        MessageBox.Show("Вы успешно зарегистрировались, и сейчас будете перенаправление к форме авторизации", "Успех");
                        FormSignIn formSignIn = new FormSignIn();
                        formSignIn.Show();
                        this.Hide();
                    }
                    catch 
                    {
                        MessageBox.Show("Не получилось связаться с сервером!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }

                }
            }
        }

        private int RegistrationCheck()
        {
            return 0;
        }

        private void ButtonToFormSignIn_Click_1(object sender, EventArgs e)
        {
            this.Hide();
            FormSignIn formSignIn = new FormSignIn();
            formSignIn.Show();
        }

        private void listBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
         
        }

        private void TextSeriesPassport_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextNumberPassport_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextLastName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[а-яА-Я]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextSurname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[а-яА-Я]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[а-яА-Я]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextLastName_Scroll(object sender, ScrollEventArgs e)
        {
            
        }

      

  
        private void TextAddress_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), @"[а-яА-Я,.0-9\-]") || (Keys)e.KeyChar == Keys.Back || (Keys)e.KeyChar == Keys.Space) return;
            e.Handled = true;
        }

        private void ButtonGetCode_Click(object sender, EventArgs e)
        {
            LabelConfirmCode.Visible = true;
            TextCodeConfirmPhone.Visible = true;
            ButtonSendCode.Visible = true;
            TextPhone.Enabled = false;
            ButtonGetCode.Enabled = false;

            PhoneVerificationAndValidateOtherFun pnv = new PhoneVerificationAndValidateOtherFun();
            codeConfirm = pnv.GetAndSendCodeConfirm("");
        }

        private void ButtonSendCode_Click(object sender, EventArgs e)
        {
            TextCodeConfirmPhone.Enabled = false;
            ButtonSendCode.Enabled = false;
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
          
        }

        private void SwitchHideViewPassword_Click(object sender, EventArgs e)
        {

        }

        private void TextSurname_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextPhone_TextChanged(object sender, EventArgs e)
        {

        }

        private void ButtonGetCode_Click_1(object sender, EventArgs e)
        {

        }

        private  void ButtonGetCode_Click_2(object sender, EventArgs e)
        {
            DataSet tablePhone = new DataSet();
            try
            {
                tablePhone = Connection.GetTable($"select * from Клиенты where Номер_телефона = '{TextPhone.Text}'");
            } catch
            {
                MessageBox.Show("Не удалось подключиться к базе!");
            }
     
                if(TextPhone.Text != "")
                {
                    if (tablePhone.Tables[0].Rows.Count < 1)
                    {
                        codeConfirm = new PhoneVerificationAndValidateOtherFun().GetAndSendCodeConfirm(TextPhone.Text);
                       if(codeConfirm == "-1")
                    {
                        MessageBox.Show("Проверьте подключение к интернету!", "Ошибка");
                    } else
                    {
                        if (codeConfirm is null)
                        {
                            MessageBox.Show("Неверный формат или неправильный номер телефона!", "Предупреждение");
                        }
                        else
                        {

                            phone = TextPhone.Text;

                            ss = 30;
                            timer1.Start();
                            //blockBtnGetCode = true;
                            //ButtonGetCode.Cursor = Cursors.AppStarting;
                            ButtonGetCode.Enabled = false;
                            LabelConfirmCode.Visible = true;
                            ButtonSendCode.Visible = true;
                            TextCodeConfirmPhone.Visible = true;
                            label8.Visible = true;
                            label8.Text = "Проверьте код подтверждения \nОтправленный на " + TextPhone.Text.Substring(0, 7) + "****";
                        }
                    }
                          
                        

                       
                    } else
                    {
                    MessageBox.Show("Номер телефона уже зарегистрирован, используйте другой!", "Предупреждение");
                }
                   
                } else
                {
                    MessageBox.Show("Введите номер телефона", "Предупреждение");
                }
               
             
            
           
        }

        private void ButtonSendCode_Click_1(object sender, EventArgs e)
        {
            if(TextCodeConfirmPhone.Text == "")
            {
                MessageBox.Show("Введите код, отправленный на указанный номер", "Предупреждение");
            } else if(TextCodeConfirmPhone.Text == codeConfirm)
            {
                MessageBox.Show("Номер телефона подтвержден!", "Оповещение");
                TextPhone.Enabled = false;
                TextCodeConfirmPhone.Enabled = false;
                ButtonGetCode.Enabled = false;
                ButtonSendCode.Enabled = false;
                label8.Text = "Номер телефона подтвержден!";
                timer1.Stop();
                ButtonGetCode.Text = "Получить код";
                confirmCode = true;
            } else
            {
                MessageBox.Show("Неверный код подтверждения!", "Предупреждение");
            }
        }
        int ss = 30;
        private void timer1_Tick(object sender, EventArgs e)
        {
            ss--;
            ButtonGetCode.Text = "Получить код (" + ss + ")";
        
            if(ss < 1)
            {
                ButtonGetCode.Text = "Получить код";
                //ButtonGetCode.Cursor = Cursors.Hand;
                ButtonGetCode.Enabled = true;
                //blockBtnGetCode = false;
                timer1.Stop();
            }
        }

        private void guna2TextBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextSeriosPassport_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextPhone_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextCodeConfirmPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextSurname_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[а-яА-Я]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextName_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[а-яА-Я]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextLastName_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[а-яА-Я]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private async void TextAddress_KeyPress_1(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), @"[а-яА-Я,.0-9\-/]") || (Keys)e.KeyChar == Keys.Back || (Keys)e.KeyChar == Keys.Space) return;
            var address = await PhoneVerificationAndValidateOtherFun.FindAddress2(TextAddress.Text);



            comboBox1.Items.Clear();
            foreach (var item in address.suggestions)
            {
                guna2ComboBox1.Items.Insert(0, item.value.ToString());

            }

            guna2ComboBox1.DroppedDown = true;

            e.Handled = true;
        }

        private void FormSignUp_Load(object sender, EventArgs e)
        {

        }

        private  void TextAddress_Leave(object sender, EventArgs e)
        {
            guna2ComboBox1.DroppedDown = false;
        }

        private  void comboBox1_TextUpdate(object sender, EventArgs e)
        {
           
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private  void guna2ComboBox1_TextUpdate(object sender, EventArgs e)
        {
         
        }

        private  void TextAddress_TextChanged(object sender, EventArgs e)
        {
            //var address = await PhoneVerificationAndValidateOtherFun.FindAddress(TextAddress.Text);
            //TextAddress.Text = address.result;
            //TextAddress.SelectionStart = TextAddress.Text.Length;
            //var address = await PhoneVerificationAndValidateOtherFun.FindAddress2(TextAddress.Text);
           
            //TextAddress.SelectionStart = TextAddress.Text.Length;
        }

      

        private  void comboBox1_TextChanged(object sender, EventArgs e)
        {
           
        }

        private void guna2ComboBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            return;
        }

        private  void comboBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
           
            if (Regex.IsMatch(e.KeyChar.ToString(), @"[а-яА-Я,.0-9\-/]") || (Keys)e.KeyChar == Keys.Back || (Keys)e.KeyChar == Keys.Space) return;


            e.Handled = true;
        }

        private void guna2ComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            TextAddress.Text = guna2ComboBox1.SelectedItem.ToString();
        }

        private async void comboBox1_TextUpdate_1(object sender, EventArgs e)
        {

          
            var address = await PhoneVerificationAndValidateOtherFun.FindAddress2(comboBox1.Text);


            if(address != null)
            {
            comboBox1.Items.Clear();
            foreach (var item in address.suggestions)
            {
                comboBox1.Items.Insert(0, item.value.ToString());

            }
            comboBox1.DroppedDown = true;
            comboBox1.SelectionStart = comboBox1.Text.Length;
            }   

          
        }
    }
}
