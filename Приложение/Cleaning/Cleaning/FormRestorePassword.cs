﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class FormRestorePassword : Form
    {
        public FormRestorePassword()
        {
            InitializeComponent();
            HideAllStepRestore();
            InputPhone1.Visible = true;
            TextNewPassword.UseSystemPasswordChar = true;
            SwitchHideViewPassword.MouseDown += new MouseEventHandler(ShowPassword);
            SwitchHideViewPassword.MouseUp += new MouseEventHandler(HidePassword);
        }
        string codeVerification = null; // для верификации номера телефона
        bool checkStartRestore = false;
      
    

  

        private void HidePassword(object sender, EventArgs e)
        {
            TextNewPassword.UseSystemPasswordChar = true;
            SwitchHideViewPassword.Image = Properties.Resources.hidden;
        }
        private void ShowPassword(object sender, EventArgs e)
        {
            TextNewPassword.UseSystemPasswordChar = false;
            SwitchHideViewPassword.Image = Properties.Resources.view;
        }

        private void ButtonToFormSignIn_Click(object sender, EventArgs e)
        {
            FormSignIn formSignIn = new FormSignIn();
            if (checkStartRestore)
            {
                DialogResult dialogResult = MessageBox.Show("Вы точно хотите прервать процесс восстановления?", "Предупреждение", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (dialogResult == DialogResult.Yes)
                {
                 
                    formSignIn.Show();
                    this.Hide();
                }
            } else
            {
                formSignIn.Show();
                this.Hide();
            }
        
        }

        private void ButtonCloseForm_Click(object sender, EventArgs e)
        {
            FormSignIn formSignIn = new FormSignIn();
            if (checkStartRestore)
            {
                DialogResult dialogResult = MessageBox.Show("Вы точно хотите прервать процесс восстановления!", "Предупреждение", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                if (dialogResult == DialogResult.Yes)
                {

                    formSignIn.Show();
                    this.Hide();
                }
            }
            else
            {
                formSignIn.Show();
                this.Hide();
            }

        }

        private void TextPhone_TextChanged(object sender, EventArgs e)
        {

        }

        private void ButtonNextRestore1_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }
        bool enableSendAgain = false;
         private void guna2Button1_Click(object sender, EventArgs e)
        {
            
            if(TextPhoneRestore.Text == "")
            {
                MessageBox.Show("Введите номер телефона");
            } else
            {
                DataSet dataSet;
                try
                {
                    dataSet = Connection.GetTable($"select * from Клиенты where Номер_телефона = '{TextPhoneRestore.Text}'");
                    if (dataSet.Tables[0].Rows.Count < 1)
                    {
                        MessageBox.Show("Номер телефона не найден!");
                    }
                    else
                    {

                        HideAllStepRestore();
                        InputCodeConfirm2.Visible = true;
                        ButtonAgainCodeRestore.Visible = true;
                        ButtonAgainCodeRestore.Cursor = Cursors.No;
                        checkStartRestore = true;
                        codeVerification = new PhoneVerificationAndValidateOtherFun().GetAndSendCodeConfirm(TextPhoneRestore.Text);
                        label1.Text = $"Введите 4-значный код, отправленный на +{TextPhoneRestore.Text.Substring(0, 7)}****.";
                        timer1.Start();
                        ButtonAgainCodeRestore.Cursor = Cursors.AppStarting;

                    }
                } catch
                {
                    MessageBox.Show("Не удалось подключиться к базе!", "Ошибка");
                }
             
               
            }
        

        }

        private void HideAllStepRestore()
        {
            InputPhone1.Visible = false;
            InputCodeConfirm2.Visible = false;
            InputNewPassword3.Visible = false;
        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            if (TextCodeRestore.Text == "") MessageBox.Show("Введите код, отправленный на указанный номер телефона");
            else
            {
                if(TextCodeRestore.Text == codeVerification)
                {
                    HideAllStepRestore();
                    InputNewPassword3.Visible = true;
                } else
                {
                    MessageBox.Show("Неверный код подтверждения");
                }
            }
       
        }

        private void ButtonAgainCodeRestore_Click(object sender, EventArgs e)
        {
            if(enableSendAgain)
            {
                codeVerification = new PhoneVerificationAndValidateOtherFun().GetAndSendCodeConfirm(TextPhoneRestore.Text);
                ButtonAgainCodeRestore.Text = "Код отправлен повторно!";
                ButtonAgainCodeRestore.Cursor = Cursors.No;
                enableSendAgain = false;
            }
          

        }
        int second = 30; // для отсчета повторной отправки
        private void timer1_Tick(object sender, EventArgs e)
        {
         
            second--;
            ButtonAgainCodeRestore.Text = "Отправить код повторно через " + second;
            if (second == 0)
            {
                enableSendAgain = true;
                ButtonAgainCodeRestore.Text = "Отправить код повторно";
                ButtonAgainCodeRestore.Enabled = true;
                ButtonAgainCodeRestore.Cursor = Cursors.Hand;
                timer1.Stop();
            }
           
          

        }

        private void ButtonChangePassword_Click(object sender, EventArgs e)
        {
            if (TextNewPassword.Text == "")
            {
                MessageBox.Show("Придумайте новый пароль!", "Предупреждение");

            }
            else
            {
                if ((new PhoneVerificationAndValidateOtherFun().IsValidatePassword(TextNewPassword.Text)) == false)
                {
                    MessageBox.Show("Пароль не соответствует требованиям надежности!", "Предупреждение");
                }
                else
                {
                    using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                    {
                        SqlCommand command = new SqlCommand($"Update Клиенты set Пароль = '{PhoneVerificationAndValidateOtherFun.GetHash(TextNewPassword.Text)}' Where Номер_телефона = '{TextPhoneRestore.Text}'", connection);

                        try
                        {
                            connection.Open();
                            command.ExecuteNonQuery();
                            MessageBox.Show("Пароль изменен! Теперь вы можете зайти в свой аккаунт по новому паролю!", "Оповещение");
                            FormSignIn formSignIn = new FormSignIn();
                            formSignIn.Show();
                            this.Hide();
                        }
                        catch
                        {
                            MessageBox.Show("Не удалось изменить пароль!", "Ошибка");
                        }

                    }
                }
            }
        }
    }
}
