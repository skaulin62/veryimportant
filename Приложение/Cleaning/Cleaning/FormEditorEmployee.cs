﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class FormEditorEmployee : Form
    {
        public FormEditorEmployee()
        {
            InitializeComponent();
            try
            {
                FillComboBox(ComboBoxRole, "select Наименование from Роли where Наименование in ('Уборщик', 'Оператор')");
                FillEmployee(guna2Panel1, "select * from EmployeeRole where [delete] = 0");
            } catch
            {
                MessageBox.Show("Данные не были подгружены!", "Ошибка");
            }
      
            TextPassword.UseSystemPasswordChar = false;
            Date.MaxDate = DateTime.Now.Date.AddYears(-18);
            Date.Value = Date.MaxDate;
            TextPassword.UseSystemPasswordChar = true;
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
        private void FillComboBox(ComboBox list, string query)
        {
            DataSet table = Connection.GetTable(query);
            list.Items.Clear();
            for (int i = 0; i < table.Tables[0].Rows.Count; i++)
            {
                list.Items.Add(table.Tables[0].Rows[i][0].ToString());
            }
        }
        private void FillEmployee(Panel panel, string query)
        {
            DataSet table = Connection.GetTable(query);
            panel.Controls.Clear();
            for (int i = 0; i < table.Tables[0].Rows.Count; i++)
            {
                Panel card = new Panel() { Height = 10, Dock = DockStyle.Top, BackColor = Color.White };
                CardEmployee cardEmployee = new CardEmployee(
                    table.Tables[0].Rows[i][0].ToString(),
                    table.Tables[0].Rows[i][1].ToString(),
                    table.Tables[0].Rows[i][2].ToString(),
                    table.Tables[0].Rows[i][3].ToString(),
                    table.Tables[0].Rows[i][4].ToString(),
                    table.Tables[0].Rows[i][5].ToString()
                    );
                Guna.UI2.WinForms.Guna2Button btn = cardEmployee.Controls.Find(table.Tables[0].Rows[i][0].ToString(), true).FirstOrDefault() as Guna.UI2.WinForms.Guna2Button;
                btn.Click += new EventHandler(ButtonDelete);

                cardEmployee.Dock = DockStyle.Top;
                panel.Controls.Add(card);
                panel.Controls.Add(cardEmployee);
            }
        }
        private void ButtonDelete(object sender, EventArgs e)
        {
            DataSet checkCleanerOnOrder;
            try
            {
                checkCleanerOnOrder = Connection.GetTable($"Select * from Заказы Where Код_уборщика = {(sender as Guna.UI2.WinForms.Guna2Button).Name} and (Дата_уборки between convert(datetime,'{DateTime.Now.Date.AddDays(-1)}',105) and convert(datetime,'{DateTime.Now.Date.AddDays(15)}',105)) and Статус = 'Ожидание выполнения'"); ;

                if (checkCleanerOnOrder.Tables[0].Rows.Count > 0)
                {
                    MessageBox.Show("У данного сотрудника остались невыполненные заказы!");
                }
                else
                {
                    DialogResult dialogResult = MessageBox.Show("Подтверждение удаления, вы точно хотите удалить этого сотрудника?", "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (dialogResult == DialogResult.Yes)
                    {
                        using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                        {
                            SqlCommand sqlCommand = new SqlCommand($"update Сотрудники set удален = 1 where Код_сотрудника = {(sender as Guna.UI2.WinForms.Guna2Button).Name}", connection);

                            try
                            {
                                connection.Open();
                                sqlCommand.ExecuteNonQuery();
                                MessageBox.Show("Сотрудник удален!", "Уведомление");
                                FillEmployee(guna2Panel1, "select * from EmployeeRole where [delete] = 0");
                            }
                            catch
                            {
                                MessageBox.Show("Не удалось удалить сотрудника", "Ошибка");
                            }
                        }
                    }
                }
            }
            catch 
            {
                MessageBox.Show("Данные не были подгружены, невозможно выполнить данное действие!", "Ошибка");
            }

            
            }
        

        private void ButtonAddEmployee_Click(object sender, EventArgs e)
        {
            if(TextSurname.Text == "" || TextName.Text == "" || TextPhone.Text == "" || TextNumberPassport.Text == "" || TextSeriosPassport.Text == "" || TextLogin.Text == "" || TextPassword.Text == "" || TextNumberPassport.Text.Length != 6 || TextSeriosPassport.Text.Length != 4 || ComboBoxRole.Text == "")
            {
                MessageBox.Show("Заполните все поля правильно!", "Предупреждение");
            } else
            {
                if (new PhoneVerificationAndValidateOtherFun().IsValidatePassword(TextPassword.Text) == false)
                {
                    MessageBox.Show("Пароль не соответствует требованиям!", "Предупреждение");
                } else
                {
                    DialogResult dialogResult = MessageBox.Show("Вы уверены, что хотите добавить сотрудника?", "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dialogResult == DialogResult.Yes)
                    {
                        using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                        {
                            string date = Date.Value.Date.ToString().Split(new char[] { ' ' })[0];
                            SqlCommand sqlCommand = new SqlCommand($"insert into Сотрудники(Фамилия, Имя, Отчество, Паспортные_данные, Номер_телефона, Дата_рождения, Логин, Пароль, Код_роли) values('{TextSurname.Text}','{TextName.Text}', '{TextLastname.Text}','{TextSeriosPassport.Text + TextNumberPassport.Text}'," +
                                $"'{TextPhone.Text}', convert(date,'{date}',105), '{TextLogin.Text}', '{PhoneVerificationAndValidateOtherFun.GetHash(TextPassword.Text)}', '{Connection.GetIdRowOfObject($"select Код_роли from Роли where Наименование = '{ComboBoxRole.Text}'")}')", connection);
                            try
                            {
                                connection.Open();
                                sqlCommand.ExecuteNonQuery();
                                MessageBox.Show("Сотрудник добавлен!", "Уведомление");
                                TextSurname.Text = "";
                                TextName.Text = "";
                                TextLastname.Text = "";
                                TextPhone.Text = "";
                                TextLogin.Text = "";
                                TextSeriosPassport.Text = "";
                                TextNumberPassport.Text = "";
                                TextPassword.Text = "";
                                ComboBoxRole.Text = "";
                                FillEmployee(guna2Panel1, "select * from EmployeeRole where [delete] = 0");
                            }
                            catch 
                            {
                                MessageBox.Show("Не удалось добавить сотрудника", "Ошибка");
                            }
                        }
                    }
                    // дописать
                }
            }
        }

        private void TextService_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[а-яА-Я]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextName_TextChanged(object sender, EventArgs e)
        {

        }

        private void TextName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[а-яА-Я]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextLastname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[а-яА-Я]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextSeriosPassport_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void TextNumberPassport_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), "[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void SwitchHideViewPassword_MouseUp(object sender, MouseEventArgs e)
        {
            TextPassword.UseSystemPasswordChar = true;
            SwitchHideViewPassword.Image = Properties.Resources.hidden;
        }

        private void SwitchHideViewPassword_MouseDown(object sender, MouseEventArgs e)
        {
            TextPassword.UseSystemPasswordChar = false;
            SwitchHideViewPassword.Image = Properties.Resources.view;
        }
    }
}
