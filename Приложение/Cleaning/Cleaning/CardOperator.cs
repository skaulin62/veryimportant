﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class CardOperator : UserControl
    {
        public CardOperator(string order, string client, string number, string service, string count, string cost, string datetime, string comment, string address)
        {
            InitializeComponent();
            ButtonSetCleaner.Name = order;
            ButtonSetCleaner.Tag = datetime;
            label1.Text = "Клиент: " + client + "\n" +
                "Номер телефона: " + number + "\n" +
                "Услуга: " + service + "\n" +
                "Количество: " + count + "\n" +
                "Стоимость: " + cost + "\n" +
                "Дата и время: " + datetime.Substring(0, 16) + "\n" +
                "Адрес: " + address;
            TextComment.Text = comment;
        }

        private void TextComment_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void ButtonSetCleaner_Click(object sender, EventArgs e)
        {

        }
    }
}
