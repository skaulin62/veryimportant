﻿
namespace Cleaning
{
    partial class FormReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Title title1 = new System.Windows.Forms.DataVisualization.Charting.Title();
            this.LabelService = new System.Windows.Forms.Label();
            this.ReportTable = new Guna.UI2.WinForms.Guna2DataGridView();
            this.ButtonReport = new Guna.UI2.WinForms.Guna2Button();
            this.label7 = new System.Windows.Forms.Label();
            this.DateStart = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.diagramm = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            this.label16 = new System.Windows.Forms.Label();
            this.LabelSummCost = new System.Windows.Forms.Label();
            this.guna2Panel3 = new Guna.UI2.WinForms.Guna2Panel();
            this.Label = new System.Windows.Forms.Label();
            this.LabelCountClients = new System.Windows.Forms.Label();
            this.guna2Panel4 = new Guna.UI2.WinForms.Guna2Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.LabelCountOrderDay = new System.Windows.Forms.Label();
            this.guna2Panel5 = new Guna.UI2.WinForms.Guna2Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.LabelCountOrderWeek = new System.Windows.Forms.Label();
            this.guna2Panel6 = new Guna.UI2.WinForms.Guna2Panel();
            this.label5 = new System.Windows.Forms.Label();
            this.LabelCountOrderMonth = new System.Windows.Forms.Label();
            this.DateEnd = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.LabelAvgCost = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.guna2Panel2 = new Guna.UI2.WinForms.Guna2Panel();
            ((System.ComponentModel.ISupportInitialize)(this.ReportTable)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagramm)).BeginInit();
            this.guna2Panel1.SuspendLayout();
            this.guna2Panel3.SuspendLayout();
            this.guna2Panel4.SuspendLayout();
            this.guna2Panel5.SuspendLayout();
            this.guna2Panel6.SuspendLayout();
            this.guna2Panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // LabelService
            // 
            this.LabelService.AutoSize = true;
            this.LabelService.Font = new System.Drawing.Font("Noto Sans", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelService.Location = new System.Drawing.Point(93, 40);
            this.LabelService.Name = "LabelService";
            this.LabelService.Size = new System.Drawing.Size(202, 41);
            this.LabelService.TabIndex = 148;
            this.LabelService.Text = "Статистика";
            // 
            // ReportTable
            // 
            this.ReportTable.AllowUserToAddRows = false;
            this.ReportTable.AllowUserToDeleteRows = false;
            this.ReportTable.AllowUserToResizeColumns = false;
            this.ReportTable.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(48)))), ((int)(((byte)(52)))));
            this.ReportTable.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.ReportTable.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ReportTable.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.DisplayedCells;
            this.ReportTable.BackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(66)))), ((int)(((byte)(62)))));
            this.ReportTable.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.ReportTable.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.ReportTable.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(16)))), ((int)(((byte)(18)))));
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Noto Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.Gray;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.ControlDark;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ReportTable.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.ReportTable.ColumnHeadersHeight = 30;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(37)))), ((int)(((byte)(41)))));
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Noto Sans", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(117)))), ((int)(((byte)(119)))));
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.ReportTable.DefaultCellStyle = dataGridViewCellStyle3;
            this.ReportTable.EnableHeadersVisualStyles = false;
            this.ReportTable.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(56)))), ((int)(((byte)(62)))));
            this.ReportTable.Location = new System.Drawing.Point(639, 323);
            this.ReportTable.Name = "ReportTable";
            this.ReportTable.ReadOnly = true;
            this.ReportTable.RowHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.ReportTable.RowHeadersVisible = false;
            this.ReportTable.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.ReportTable.RowTemplate.Height = 24;
            this.ReportTable.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.ReportTable.Size = new System.Drawing.Size(883, 319);
            this.ReportTable.TabIndex = 150;
            this.ReportTable.Theme = Guna.UI2.WinForms.Enums.DataGridViewPresetThemes.Dark;
            this.ReportTable.ThemeStyle.AlternatingRowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(48)))), ((int)(((byte)(52)))));
            this.ReportTable.ThemeStyle.AlternatingRowsStyle.Font = null;
            this.ReportTable.ThemeStyle.AlternatingRowsStyle.ForeColor = System.Drawing.Color.Empty;
            this.ReportTable.ThemeStyle.AlternatingRowsStyle.SelectionBackColor = System.Drawing.Color.Empty;
            this.ReportTable.ThemeStyle.AlternatingRowsStyle.SelectionForeColor = System.Drawing.Color.Empty;
            this.ReportTable.ThemeStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(60)))), ((int)(((byte)(66)))), ((int)(((byte)(62)))));
            this.ReportTable.ThemeStyle.GridColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(56)))), ((int)(((byte)(62)))));
            this.ReportTable.ThemeStyle.HeaderStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(15)))), ((int)(((byte)(16)))), ((int)(((byte)(18)))));
            this.ReportTable.ThemeStyle.HeaderStyle.BorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.ReportTable.ThemeStyle.HeaderStyle.Font = new System.Drawing.Font("Noto Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ReportTable.ThemeStyle.HeaderStyle.ForeColor = System.Drawing.Color.White;
            this.ReportTable.ThemeStyle.HeaderStyle.HeaightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.EnableResizing;
            this.ReportTable.ThemeStyle.HeaderStyle.Height = 30;
            this.ReportTable.ThemeStyle.ReadOnly = true;
            this.ReportTable.ThemeStyle.RowsStyle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(33)))), ((int)(((byte)(37)))), ((int)(((byte)(41)))));
            this.ReportTable.ThemeStyle.RowsStyle.BorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.SingleHorizontal;
            this.ReportTable.ThemeStyle.RowsStyle.Font = new System.Drawing.Font("Noto Sans", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ReportTable.ThemeStyle.RowsStyle.ForeColor = System.Drawing.Color.White;
            this.ReportTable.ThemeStyle.RowsStyle.Height = 24;
            this.ReportTable.ThemeStyle.RowsStyle.SelectionBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(114)))), ((int)(((byte)(117)))), ((int)(((byte)(119)))));
            this.ReportTable.ThemeStyle.RowsStyle.SelectionForeColor = System.Drawing.Color.White;
            // 
            // ButtonReport
            // 
            this.ButtonReport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonReport.Animated = true;
            this.ButtonReport.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonReport.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonReport.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonReport.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonReport.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.ButtonReport.Font = new System.Drawing.Font("Noto Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonReport.ForeColor = System.Drawing.Color.White;
            this.ButtonReport.Location = new System.Drawing.Point(1208, 667);
            this.ButtonReport.Name = "ButtonReport";
            this.ButtonReport.Size = new System.Drawing.Size(314, 38);
            this.ButtonReport.TabIndex = 0;
            this.ButtonReport.Text = "Сформировать отчет";
            this.ButtonReport.Click += new System.EventHandler(this.ButtonReport_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Noto Sans", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.Location = new System.Drawing.Point(632, 260);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(104, 38);
            this.label7.TabIndex = 152;
            this.label7.Text = "Отчет";
            // 
            // DateStart
            // 
            this.DateStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DateStart.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.DateStart.BorderStyle = System.Drawing.Drawing2D.DashStyle.Custom;
            this.DateStart.BorderThickness = 1;
            this.DateStart.Checked = true;
            this.DateStart.FillColor = System.Drawing.Color.White;
            this.DateStart.Font = new System.Drawing.Font("Noto Sans", 9F);
            this.DateStart.ForeColor = System.Drawing.SystemColors.ControlText;
            this.DateStart.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.DateStart.Location = new System.Drawing.Point(674, 667);
            this.DateStart.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.DateStart.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.DateStart.Name = "DateStart";
            this.DateStart.Size = new System.Drawing.Size(221, 36);
            this.DateStart.TabIndex = 153;
            this.DateStart.Value = new System.DateTime(2022, 2, 1, 3, 0, 0, 0);
            this.DateStart.ValueChanged += new System.EventHandler(this.guna2DateTimePicker1_ValueChanged);
            // 
            // label14
            // 
            this.label14.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Noto Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(634, 676);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(34, 27);
            this.label14.TabIndex = 155;
            this.label14.Text = "от";
            // 
            // label15
            // 
            this.label15.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Noto Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(901, 676);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(36, 27);
            this.label15.TabIndex = 156;
            this.label15.Text = "по";
            // 
            // diagramm
            // 
            this.diagramm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.diagramm.BackColor = System.Drawing.Color.Transparent;
            this.diagramm.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.diagramm.BorderlineColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.diagramm.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.diagramm.BorderlineWidth = 2;
            this.diagramm.BorderSkin.BorderWidth = 0;
            this.diagramm.BorderSkin.PageColor = System.Drawing.Color.Transparent;
            chartArea1.Name = "ChartArea1";
            this.diagramm.ChartAreas.Add(chartArea1);
            legend1.Font = new System.Drawing.Font("Noto Sans", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            legend1.IsTextAutoFit = false;
            legend1.Name = "Legend1";
            this.diagramm.Legends.Add(legend1);
            this.diagramm.Location = new System.Drawing.Point(1148, 104);
            this.diagramm.Name = "diagramm";
            this.diagramm.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.None;
            this.diagramm.PaletteCustomColors = new System.Drawing.Color[] {
        System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))))};
            series1.ChartArea = "ChartArea1";
            series1.Font = new System.Drawing.Font("Noto Sans", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            series1.Legend = "Legend1";
            series1.Name = "Услуги";
            series1.YValuesPerPoint = 2;
            this.diagramm.Series.Add(series1);
            this.diagramm.Size = new System.Drawing.Size(374, 194);
            this.diagramm.TabIndex = 2;
            this.diagramm.Text = "chart1";
            title1.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            title1.Name = "Title1";
            title1.Text = "Диаграмма: количество заказанных услуг";
            this.diagramm.Titles.Add(title1);
            // 
            // guna2Panel1
            // 
            this.guna2Panel1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.guna2Panel1.BorderThickness = 2;
            this.guna2Panel1.Controls.Add(this.label16);
            this.guna2Panel1.Controls.Add(this.LabelSummCost);
            this.guna2Panel1.Location = new System.Drawing.Point(100, 265);
            this.guna2Panel1.Name = "guna2Panel1";
            this.guna2Panel1.Size = new System.Drawing.Size(508, 105);
            this.guna2Panel1.TabIndex = 157;
            // 
            // label16
            // 
            this.label16.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Noto Sans", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(35, 19);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(201, 64);
            this.label16.TabIndex = 2;
            this.label16.Text = "Общая прибыль\r\nкомпании\r\n";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelSummCost
            // 
            this.LabelSummCost.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.LabelSummCost.AutoSize = true;
            this.LabelSummCost.Font = new System.Drawing.Font("Noto Sans", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelSummCost.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.LabelSummCost.Location = new System.Drawing.Point(259, 32);
            this.LabelSummCost.Name = "LabelSummCost";
            this.LabelSummCost.Size = new System.Drawing.Size(192, 41);
            this.LabelSummCost.TabIndex = 3;
            this.LabelSummCost.Text = "123123 руб.";
            this.LabelSummCost.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // guna2Panel3
            // 
            this.guna2Panel3.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.guna2Panel3.BorderThickness = 2;
            this.guna2Panel3.Controls.Add(this.Label);
            this.guna2Panel3.Controls.Add(this.LabelCountClients);
            this.guna2Panel3.Location = new System.Drawing.Point(100, 533);
            this.guna2Panel3.Name = "guna2Panel3";
            this.guna2Panel3.Size = new System.Drawing.Size(508, 105);
            this.guna2Panel3.TabIndex = 159;
            // 
            // Label
            // 
            this.Label.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.Label.AutoSize = true;
            this.Label.Font = new System.Drawing.Font("Noto Sans", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Label.Location = new System.Drawing.Point(35, 19);
            this.Label.Name = "Label";
            this.Label.Size = new System.Drawing.Size(146, 64);
            this.Label.TabIndex = 2;
            this.Label.Text = "Количество\r\nклиентов";
            this.Label.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelCountClients
            // 
            this.LabelCountClients.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.LabelCountClients.AutoSize = true;
            this.LabelCountClients.Font = new System.Drawing.Font("Noto Sans", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelCountClients.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.LabelCountClients.Location = new System.Drawing.Point(259, 32);
            this.LabelCountClients.Name = "LabelCountClients";
            this.LabelCountClients.Size = new System.Drawing.Size(143, 41);
            this.LabelCountClients.TabIndex = 3;
            this.LabelCountClients.Text = "215 чел.";
            this.LabelCountClients.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // guna2Panel4
            // 
            this.guna2Panel4.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.guna2Panel4.BorderThickness = 2;
            this.guna2Panel4.Controls.Add(this.label1);
            this.guna2Panel4.Controls.Add(this.LabelCountOrderDay);
            this.guna2Panel4.Location = new System.Drawing.Point(100, 104);
            this.guna2Panel4.Name = "guna2Panel4";
            this.guna2Panel4.Size = new System.Drawing.Size(319, 132);
            this.guna2Panel4.TabIndex = 158;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Noto Sans", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(29, 17);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(152, 96);
            this.label1.TabIndex = 2;
            this.label1.Text = "Количество \r\nзаказов\r\nза день\r\n";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelCountOrderDay
            // 
            this.LabelCountOrderDay.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.LabelCountOrderDay.AutoSize = true;
            this.LabelCountOrderDay.Font = new System.Drawing.Font("Noto Sans", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelCountOrderDay.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.LabelCountOrderDay.Location = new System.Drawing.Point(213, 44);
            this.LabelCountOrderDay.Name = "LabelCountOrderDay";
            this.LabelCountOrderDay.Size = new System.Drawing.Size(74, 45);
            this.LabelCountOrderDay.TabIndex = 3;
            this.LabelCountOrderDay.Text = "123";
            this.LabelCountOrderDay.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // guna2Panel5
            // 
            this.guna2Panel5.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.guna2Panel5.BorderThickness = 2;
            this.guna2Panel5.Controls.Add(this.label3);
            this.guna2Panel5.Controls.Add(this.LabelCountOrderWeek);
            this.guna2Panel5.Location = new System.Drawing.Point(448, 104);
            this.guna2Panel5.Name = "guna2Panel5";
            this.guna2Panel5.Size = new System.Drawing.Size(319, 132);
            this.guna2Panel5.TabIndex = 159;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Noto Sans", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(29, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(152, 96);
            this.label3.TabIndex = 2;
            this.label3.Text = "Количество \r\nзаказов\r\nза неделю\r\n";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelCountOrderWeek
            // 
            this.LabelCountOrderWeek.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.LabelCountOrderWeek.AutoSize = true;
            this.LabelCountOrderWeek.Font = new System.Drawing.Font("Noto Sans", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelCountOrderWeek.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.LabelCountOrderWeek.Location = new System.Drawing.Point(213, 44);
            this.LabelCountOrderWeek.Name = "LabelCountOrderWeek";
            this.LabelCountOrderWeek.Size = new System.Drawing.Size(74, 45);
            this.LabelCountOrderWeek.TabIndex = 3;
            this.LabelCountOrderWeek.Text = "123";
            this.LabelCountOrderWeek.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // guna2Panel6
            // 
            this.guna2Panel6.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.guna2Panel6.BorderThickness = 2;
            this.guna2Panel6.Controls.Add(this.label5);
            this.guna2Panel6.Controls.Add(this.LabelCountOrderMonth);
            this.guna2Panel6.Location = new System.Drawing.Point(796, 104);
            this.guna2Panel6.Name = "guna2Panel6";
            this.guna2Panel6.Size = new System.Drawing.Size(319, 132);
            this.guna2Panel6.TabIndex = 160;
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Noto Sans", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(29, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(152, 96);
            this.label5.TabIndex = 2;
            this.label5.Text = "Количество \r\nзаказов\r\nза месяц\r\n";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LabelCountOrderMonth
            // 
            this.LabelCountOrderMonth.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.LabelCountOrderMonth.AutoSize = true;
            this.LabelCountOrderMonth.Font = new System.Drawing.Font("Noto Sans", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelCountOrderMonth.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.LabelCountOrderMonth.Location = new System.Drawing.Point(213, 44);
            this.LabelCountOrderMonth.Name = "LabelCountOrderMonth";
            this.LabelCountOrderMonth.Size = new System.Drawing.Size(74, 45);
            this.LabelCountOrderMonth.TabIndex = 3;
            this.LabelCountOrderMonth.Text = "123";
            this.LabelCountOrderMonth.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // DateEnd
            // 
            this.DateEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.DateEnd.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.DateEnd.BorderThickness = 1;
            this.DateEnd.Checked = true;
            this.DateEnd.FillColor = System.Drawing.Color.White;
            this.DateEnd.Font = new System.Drawing.Font("Noto Sans", 9F);
            this.DateEnd.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.DateEnd.Location = new System.Drawing.Point(943, 667);
            this.DateEnd.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.DateEnd.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.DateEnd.Name = "DateEnd";
            this.DateEnd.Size = new System.Drawing.Size(221, 36);
            this.DateEnd.TabIndex = 161;
            this.DateEnd.Value = new System.DateTime(2022, 2, 1, 3, 0, 0, 0);
            this.DateEnd.ValueChanged += new System.EventHandler(this.DateEnd_ValueChanged);
            // 
            // LabelAvgCost
            // 
            this.LabelAvgCost.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.LabelAvgCost.AutoSize = true;
            this.LabelAvgCost.Font = new System.Drawing.Font("Noto Sans", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelAvgCost.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.LabelAvgCost.Location = new System.Drawing.Point(259, 32);
            this.LabelAvgCost.Name = "LabelAvgCost";
            this.LabelAvgCost.Size = new System.Drawing.Size(158, 41);
            this.LabelAvgCost.TabIndex = 3;
            this.LabelAvgCost.Text = "1512 руб.";
            this.LabelAvgCost.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Noto Sans", 13.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.Location = new System.Drawing.Point(35, 19);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(159, 64);
            this.label8.TabIndex = 2;
            this.label8.Text = "Средний чек\r\nпо заказам\r\n";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // guna2Panel2
            // 
            this.guna2Panel2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.guna2Panel2.BorderThickness = 2;
            this.guna2Panel2.Controls.Add(this.label8);
            this.guna2Panel2.Controls.Add(this.LabelAvgCost);
            this.guna2Panel2.Location = new System.Drawing.Point(100, 400);
            this.guna2Panel2.Name = "guna2Panel2";
            this.guna2Panel2.Size = new System.Drawing.Size(508, 105);
            this.guna2Panel2.TabIndex = 158;
            // 
            // FormReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1650, 759);
            this.Controls.Add(this.DateEnd);
            this.Controls.Add(this.guna2Panel6);
            this.Controls.Add(this.guna2Panel5);
            this.Controls.Add(this.guna2Panel4);
            this.Controls.Add(this.guna2Panel3);
            this.Controls.Add(this.guna2Panel2);
            this.Controls.Add(this.guna2Panel1);
            this.Controls.Add(this.diagramm);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.DateStart);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.ButtonReport);
            this.Controls.Add(this.ReportTable);
            this.Controls.Add(this.LabelService);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormReport";
            this.Padding = new System.Windows.Forms.Padding(90, 40, 90, 40);
            this.Text = "FormReport";
            ((System.ComponentModel.ISupportInitialize)(this.ReportTable)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.diagramm)).EndInit();
            this.guna2Panel1.ResumeLayout(false);
            this.guna2Panel1.PerformLayout();
            this.guna2Panel3.ResumeLayout(false);
            this.guna2Panel3.PerformLayout();
            this.guna2Panel4.ResumeLayout(false);
            this.guna2Panel4.PerformLayout();
            this.guna2Panel5.ResumeLayout(false);
            this.guna2Panel5.PerformLayout();
            this.guna2Panel6.ResumeLayout(false);
            this.guna2Panel6.PerformLayout();
            this.guna2Panel2.ResumeLayout(false);
            this.guna2Panel2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label LabelService;
        private Guna.UI2.WinForms.Guna2DataGridView ReportTable;
        private Guna.UI2.WinForms.Guna2Button ButtonReport;
        private System.Windows.Forms.Label label7;
        private Guna.UI2.WinForms.Guna2DateTimePicker DateStart;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.DataVisualization.Charting.Chart diagramm;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label LabelSummCost;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel3;
        private System.Windows.Forms.Label Label;
        private System.Windows.Forms.Label LabelCountClients;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel4;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label LabelCountOrderDay;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel5;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label LabelCountOrderWeek;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label LabelCountOrderMonth;
        private Guna.UI2.WinForms.Guna2DateTimePicker DateEnd;
        private System.Windows.Forms.Label LabelAvgCost;
        private System.Windows.Forms.Label label8;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel2;
    }
}