﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class CardCurrentOrder : UserControl
    {
        public CardCurrentOrder(string order, string cleaner, string phone, string service, string cost, string count, string datetime, string status)
        {
            InitializeComponent();
            label2.Text = order;
            label1.Text = "Услуга: " + service + "\n" +
                "Количество: " + count + " кв. м. \n" +
                "Стоимость: " + cost + " руб.";
            label3.Text = "Для связи с уборщиком: " + "\n" +
                "Номер телефона: " + phone + "\n" +
                "Фамилия и имя: " + cleaner;
            label5.Text = "Статус заказа:" + "\n" + status;
            label6.Text = datetime.Substring(0, 16);
        }
    }
}
