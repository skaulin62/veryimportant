﻿using Cleaning.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class FormEditingService : Form
    {
        public FormEditingService(string id)
        {
            InitializeComponent();
           
            try
            {
                FillComboBox(ComboBoxCategory, "select Наименование from Категории_услуг");
                FillDataEditor(id);
            } catch
            {
                MessageBox.Show("Данные не были подгружены", "Ошибка");
            }
      

            panel2.AllowDrop = true;
     
        }
        string imageOld;
        private void FillDataEditor(string id)
        {
            DataSet table = Connection.GetTable($"select * from ServicesCategories where id = {id} ");
            TextId.Text = table.Tables[0].Rows[0][0].ToString();
            TextNameService.Text = table.Tables[0].Rows[0][1].ToString();
            TextDescription.Text = table.Tables[0].Rows[0][2].ToString();
            TextCost.Text = (Convert.ToInt32(table.Tables[0].Rows[0][3])).ToString();
            ComboBoxCategory.Text = table.Tables[0].Rows[0][5].ToString();

            if (table.Tables[0].Rows[0][4].ToString() == "")
            {
                panel2.BackgroundImage = Resources.Group_4;

            }
            else
            {
                if(File.Exists(table.Tables[0].Rows[0][4].ToString()))
                {
                    panel2.BackgroundImage = Image.FromFile(table.Tables[0].Rows[0][4].ToString());
                    imagePath = table.Tables[0].Rows[0][4].ToString();
                    imageOld = table.Tables[0].Rows[0][4].ToString();
                } else
                {
                    panel2.BackgroundImage = Resources.Group_1;
                }
               
            }

        }
        private void FillComboBox(ComboBox list, string query)
        {
            DataSet table = Connection.GetTable(query);
            list.Items.Clear();
            for (int i = 0; i < table.Tables[0].Rows.Count; i++)
            {
                list.Items.Add(table.Tables[0].Rows[i][0].ToString());
            }
        }

        private void ButtonCloseForm_Click(object sender, EventArgs e)
        {
            this.Hide();
        }



        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if (TextNameService.Text == "" || TextDescription.Text == "" || TextCost.Text == "" || Convert.ToDouble(TextCost.Text) < 40 || ComboBoxCategory.Text == "" || TextNameService.Text.Length < 4 || TextDescription.Text.Length < 30)
            {
                MessageBox.Show("Заполните все поля правильно!", "Предупреждение");
            }
            else
            {
                DialogResult dialogResult = MessageBox.Show("Вы уверены, что хотите отредактировать данную услугу?", "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                    {
                        int id_cat = Connection.GetIdRowOfObject($"select Код_категории from Категории_услуг where Наименование = '{ComboBoxCategory.Text}'");
                        SqlCommand sqlCommand = new SqlCommand($"update Услуги set Наименование = '{TextNameService.Text}',Описание = '{TextDescription.Text}'," +
                            $"Стоимость = '{TextCost.Text}', Код_категории = '{id_cat.ToString()}', Изображение = '{imagePath}' where Код_услуги = '{TextId.Text}'", connection);
                        try
                        {
                            connection.Open();
                            sqlCommand.ExecuteNonQuery();
                            MessageBox.Show("Услуга изменена!", "Уведомление");
                            this.Hide();
                        }
                        catch 
                        {
                            MessageBox.Show("Не удалось изменить услугу", "Ошибка");
                        }
                    }
                }
            }
        }

        private void TextDescription_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), @"[а-яА-Я,.0-9()]") || (Keys)e.KeyChar == Keys.Back || (Keys)e.KeyChar == Keys.Space) return;
            e.Handled = true;
        }

        private void TextNameService_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), @"[а-яА-Я]") || (Keys)e.KeyChar == Keys.Back || (Keys)e.KeyChar == Keys.Space) return;
            e.Handled = true;
        }

        private void TextCost_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), @"[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }


        string imageName = "";
        string imagePath = "";

        private void CopyImage()
        {
            if (!imageName.Contains($"{Directory.GetCurrentDirectory()}\\services"))
            {
                FileInfo fileInfo = new FileInfo(imageName);
                if (fileInfo.Exists)
                {
                    try
                    {
                        fileInfo.CopyTo($"{Directory.GetCurrentDirectory()}\\{imagePath}", true);
                        panel2.BackgroundImage = Image.FromFile(imagePath);
                    } catch
                    {
                        MessageBox.Show("Файл уже используется, попробуйте поменять имя файла и повторить снова", "Ошибка");
                    }
                 
                }
            }
            else
            {
                panel2.BackgroundImage = Image.FromFile(imagePath);
            }
        }

      

        private void FormEditingService_Load(object sender, EventArgs e)
        {

        }

       

        private void pictureBox1_DragDrop(object sender, DragEventArgs e)
        {
            string[] files;
            files = null;
            files = (string[])e.Data.GetData(DataFormats.Bitmap);
            imageName = files[0];
            imagePath = $"services\\{Path.GetFileName(imageName)}";
            CopyImage();
            panel2.BackgroundImage = Image.FromFile(imagePath);
        }

        private void panel2_Click(object sender, EventArgs e)
        {
           
        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {
            float[] dashBoard = { 4, 4,4, 4 };
            Pen pen = new Pen(Color.Gray, 2);
            pen.DashPattern = dashBoard;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            e.Graphics.DrawRectangle(pen, new Rectangle(1, 1, panel2.Width - 3, panel2.Height - 3));
        }

        private void panel2_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                panel2.BackgroundImage = Resources.Group_5;
                e.Effect = DragDropEffects.Copy;
            }
            else
            {
                e.Effect = DragDropEffects.None;
            }
        }

        private void panel2_DragDrop(object sender, DragEventArgs e)
        {
            string[] files;
            files = null;
            files = (string[])e.Data.GetData(DataFormats.FileDrop);
            imageName = files[0];
            imagePath = $"services\\{Path.GetFileName(imageName)}";
            string path = Path.GetExtension(imagePath).ToString();
            try
            {
                
                if (path == ".jpg" || path == ".png" || path == ".jpeg")
                {
                    panel2.BackgroundImage = Image.FromFile(imageName);
                    imageOld = imagePath;
                    CopyImage();
                }
                else
                {
                    MessageBox.Show("Выберите изображение (jpg, jpeg, png)!", "Предупреждение");
                    panel2.BackgroundImage = Image.FromFile(imageOld);
                }

            } catch(Exception ex) 
            {
               
                panel2.BackgroundImage = Resources.Group_4;
            }
            
          


         
        }

        private void panel2_DragLeave(object sender, EventArgs e)
        {
            if (imagePath == "")
            {
                panel2.BackgroundImage = Resources.Group_4;

            }
            else
            {
                try
                {
                    panel2.BackgroundImage = Image.FromFile(imageName);

                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                    //panel2.BackgroundImage = Resources.Group_4;
                    panel2.BackgroundImage = Image.FromFile(imageOld);
                }

            }
        }

        private void label5_Paint(object sender, PaintEventArgs e)
        {
            float[] dashBoard = { 6, 6, 6, 6 };
            Pen pen = new Pen(Color.Gray, 1);
            pen.DashPattern = dashBoard;
            e.Graphics.SmoothingMode = System.Drawing.Drawing2D.SmoothingMode.AntiAlias;
            e.Graphics.DrawRectangle(pen, new Rectangle(1, 1, label5.Width - 3, label5.Height - 3));
        }

        private void label5_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog ofd = new OpenFileDialog())
            {
                ofd.Title = "Выберите изображение";
                ofd.InitialDirectory = $"{Directory.GetCurrentDirectory()}\\services";
                ofd.Filter = "Изображение|*.png; *.jpg";
                if (ofd.ShowDialog() == DialogResult.OK)
                {
                    imageName = ofd.FileName;
                    imagePath = $"services\\{Path.GetFileName(imageName)}";
                    CopyImage();

                }
            }
        }
    }
}
