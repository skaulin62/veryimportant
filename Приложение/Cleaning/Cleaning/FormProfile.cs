﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class FormProfile : Form
    {
        public FormProfile(Account account)
        {
            InitializeComponent();
            TextPassword.UseSystemPasswordChar = true;
            SwitchHideViewPassword.Image = Properties.Resources.hidden;
            TextSurname.Text = account.surname;
            TextName.Text = account.name;
            TextLastname.Text = account.lastname;
            TextLoginPhone.Text = account.login;
            TextSeriusPassport.Text = account.passport.Substring(0, 4);
            TextNumberPassport.Text = account.passport.Substring(4, 6);
            TextAddress.Text = Connection.GetTable($"select Адрес from Клиенты where Код_клиента = '{account.id}'").Tables[0].Rows[0][0].ToString();
            LabelDate.Text = "Дата регистрации: " + "22.05.2022";
            this.account = account;
            TextPassword.UseSystemPasswordChar = true;
        }
        Account account;
        private void guna2TextBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void guna2TextBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void SwitchHideViewPassword_MouseUp(object sender, MouseEventArgs e)
        {
            TextPassword.UseSystemPasswordChar = true;
            SwitchHideViewPassword.Image = Properties.Resources.hidden;
        }

        private void SwitchHideViewPassword_MouseDown(object sender, MouseEventArgs e)
        {
            TextPassword.UseSystemPasswordChar = false;
            SwitchHideViewPassword.Image = Properties.Resources.view;
        }

        private void FormProfile_Load(object sender, EventArgs e)
        {

        }

        private void guna2Button2_Click(object sender, EventArgs e)
        {
            if(TextPassword.Text == "")
            {
                MessageBox.Show("Введите новый пароль!", "Предупреждение");
            } else if(new PhoneVerificationAndValidateOtherFun().IsValidatePassword(TextPassword.Text) == false)
            {
                MessageBox.Show("Пароль не соответствует требованиям!", "Предупреждение");
            } else
            {
                using(SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand($"update Клиенты set Пароль = '{PhoneVerificationAndValidateOtherFun.GetHash(TextPassword.Text)}' where Код_клиента = '{account.id}'", connection);
                    try
                    {
                        connection.Open();
                        sqlCommand.ExecuteNonQuery();
                        MessageBox.Show("Пароль был изменён!", "Уведомление");
                        TextPassword.Text = "";
                    } catch
                    {
                        MessageBox.Show("Не удалось изменить пароль!", "Ошибка");
                    }
                } 
            }
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            if(TextAddress.Text == "")
            {
                MessageBox.Show("Строка адреса не может быть пустой!", "Предупреждение");
            } else
            {
                using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand($"update Клиенты set Адрес = '{TextAddress.Text}' where Код_клиента = '{account.id}' ", connection);
                    try
                    {
                        connection.Open();
                        sqlCommand.ExecuteNonQuery();
                        MessageBox.Show("Адрес был изменён!", "Уведомление");
                        
                    }
                    catch
                    {
                        MessageBox.Show("Не удалось изменить адрес!", "Ошибка");
                    }
                }
            }
        }
    }
}
