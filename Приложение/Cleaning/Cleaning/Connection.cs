﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    static public class Connection
    {
        static public string ConnectionString = ReadConnection(); 
        
        //функция возвращающая таблицу соответствующую отправленному запросу
         static public DataSet GetTable(string query)
        {
            DataSet Table = new DataSet();
            using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
            {
                connection.Open();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(query, connection);
                sqlDataAdapter.Fill(Table);

            }
            return Table;
        }
        
        // получить id конкретной записи
        static public int GetIdRowOfObject(string query)
        {
            DataSet Table = new DataSet();
            using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
            {

                connection.Open();
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(query, connection);
                sqlDataAdapter.Fill(Table);

            }

            int id = int.Parse(Table.Tables[0].Rows[0][0].ToString());

            return id;
        }

        // чтение строки подключения с файла
        static public string ReadConnection()
        {
            string path = "connection.txt";
            string connection = null;

            using (StreamReader streamReader = new StreamReader(path))
            {
                connection = streamReader.ReadToEnd();
            }



                return connection;
        }
    
    }


}
//DESKTOP-K4R3LSK\SQLEXPRESS