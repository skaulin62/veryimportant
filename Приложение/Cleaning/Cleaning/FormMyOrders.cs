﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Guna.UI2.WinForms;

namespace Cleaning
{
    public partial class FormMyOrders : Form
    {
        public FormMyOrders(Account account)
        {
            InitializeComponent();
            this.account = account;
        }
        Account account;
        DataSet table;

        /// <summary>
        /// В обработке
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void guna2Button1_Click(object sender, EventArgs e)
        {
            try
            {
                LabelService.Text = "Обзор заказов - В обработке";
                OrdersProceccing();
            } catch
            {
                MessageBox.Show("Данные не были подгружены!", "Ошибка");
            }
       
        }

        private void OrdersProceccing()
        {
            table = Connection.GetTable($"select * from orders_decline where id_client = {account.id} and [status] = 'В обработке' ");

            PanelOrders.Controls.Clear();
            if (table.Tables[0].Rows.Count < 1)
            {
                EmptyForm emptyForm = new EmptyForm("Нет заказов");
                emptyForm.Dock = DockStyle.Fill;
                PanelOrders.Controls.Add(emptyForm);
            } else
            {
                for (int i = 0; i < table.Tables[0].Rows.Count; i++)
                {
                    Panel panel = new Panel() { Height = 20, Dock = DockStyle.Top, BackColor = Color.White };
                    CardOrderProcessing cardOrderProcessing = new CardOrderProcessing(
                        table.Tables[0].Rows[i][0].ToString(),
                        table.Tables[0].Rows[i][3].ToString(),
                        Convert.ToInt32(table.Tables[0].Rows[i][4]).ToString(),
                        table.Tables[0].Rows[i][5].ToString(),
                        table.Tables[0].Rows[i][13].ToString(),
                        table.Tables[0].Rows[i][11].ToString()
                        );
                    Guna2Button guna2Button = cardOrderProcessing.Controls.Find(table.Tables[0].Rows[i][0].ToString(), true).FirstOrDefault() as Guna2Button;
                    guna2Button.Click += new EventHandler(ButtonDecline);
                    cardOrderProcessing.Dock = DockStyle.Top;
                    PanelOrders.Controls.Add(panel);
                    PanelOrders.Controls.Add(cardOrderProcessing);
                }
            }

           
        }

        private void ButtonDecline(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("Вы уверены что хотите отменить заказ?", "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if(dialogResult == DialogResult.Yes)
            {
                using(SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                {
                    SqlCommand sqlCommand = new SqlCommand($"update Заказы set Статус = 'Отменен' Where Номер_заказа = '{(sender as Guna2Button).Name}'", connection);
                    try
                    {
                        connection.Open();
                        sqlCommand.ExecuteNonQuery();
                        OrdersProceccing();

                    }
                    catch
                    {
                        MessageBox.Show("Не получилось отменить заявку!", "Ошибка");
                    }
                }
            }
        }
        /// <summary>
        /// ждут отзык
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void guna2Button3_Click(object sender, EventArgs e)
        {
            
            try
            {
                LabelService.Text = "Обзор заказов - Ожидающие отзыва";
                OrdersWaitComment();
            }
            catch
            {
                MessageBox.Show("Данные не были подгружены!", "Ошибка");
            }
        }

        private void OrdersWaitComment()
        {
            table = Connection.GetTable($"select * from orders_decline where id_client = {account.id} and [status] = 'Ждут отзыв' ");
            PanelOrders.Controls.Clear();
            if (table.Tables[0].Rows.Count < 1)
            {
                EmptyForm emptyForm = new EmptyForm("Нет заказов");
                emptyForm.Dock = DockStyle.Fill;
                PanelOrders.Controls.Add(emptyForm);
            } else
            {
               
                PanelOrders.Controls.Clear();
                for (int i = 0; i < table.Tables[0].Rows.Count; i++)
                {
                    Panel panel = new Panel() { Height = 20, Dock = DockStyle.Top, BackColor = Color.White };
                    CardOrderWaitComment cardOrderWaitComment = new CardOrderWaitComment(
                       table.Tables[0].Rows[i][0].ToString(),
                        table.Tables[0].Rows[i][3].ToString(),
                        table.Tables[0].Rows[i][5].ToString(),
                        Convert.ToInt32(table.Tables[0].Rows[i][4]).ToString(),
                        table.Tables[0].Rows[i][11].ToString()
                        );

                    cardOrderWaitComment.Dock = DockStyle.Top;
                    PanelOrders.Controls.Add(panel);
                    PanelOrders.Controls.Add(cardOrderWaitComment);
                }
            }
           
        }

        private void guna2Button4_Click(object sender, EventArgs e)
        {
             try
            {
                LabelService.Text = "Обзор заказов - Текущие";
                CurrentOrder();
            }
            catch
            {
                MessageBox.Show("Данные не были подгружены!", "Ошибка");
            }

        }

        private void CurrentOrder()
        {
            table = Connection.GetTable($"select * from currentOrder where [client] = '{account.id}' and [status] = 'Ожидание выполнения' ");
            PanelOrders.Controls.Clear();
            if (table.Tables[0].Rows.Count < 1)
            {
                EmptyForm emptyForm = new EmptyForm("Нет заказов");
                emptyForm.Dock = DockStyle.Fill;
                PanelOrders.Controls.Add(emptyForm);
            } else
            {
            
                for (int i = 0; i < table.Tables[0].Rows.Count; i++)
                {
                    Panel panel = new Panel() { Height = 20, Dock = DockStyle.Top, BackColor = Color.White };
                    CardCurrentOrder cardCurrentOrder = new CardCurrentOrder(
                       table.Tables[0].Rows[i][0].ToString(),
                        table.Tables[0].Rows[i][2].ToString(),
                        table.Tables[0].Rows[i][3].ToString(),
                        table.Tables[0].Rows[i][4].ToString(),
                         Convert.ToInt32(table.Tables[0].Rows[i][5]).ToString(),
                           table.Tables[0].Rows[i][6].ToString(),
                              table.Tables[0].Rows[i][7].ToString(),
                              table.Tables[0].Rows[i][8].ToString()
                        );

                    cardCurrentOrder.Dock = DockStyle.Top;
                    PanelOrders.Controls.Add(panel);
                    PanelOrders.Controls.Add(cardCurrentOrder);
                }
            }
           
        }
    }
}
