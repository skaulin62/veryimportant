﻿
namespace Cleaning
{
    partial class FormSignIn
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSignIn));
            this.ButtonSignIn = new System.Windows.Forms.Button();
            this.ButtonJumpRestorePassword = new System.Windows.Forms.Button();
            this.LabelCaption = new System.Windows.Forms.Label();
            this.LabelPhone = new System.Windows.Forms.Label();
            this.LabelPassword = new System.Windows.Forms.Label();
            this.ButtonForgotPassword = new System.Windows.Forms.Button();
            this.TextPhone = new Guna.UI2.WinForms.Guna2TextBox();
            this.TextPassword = new Guna.UI2.WinForms.Guna2TextBox();
            this.SwitchHideViewPassword = new System.Windows.Forms.PictureBox();
            this.ButtonCloseForm = new System.Windows.Forms.PictureBox();
            this.ImageLogoApp = new System.Windows.Forms.PictureBox();
            this.guna2BorderlessForm1 = new Guna.UI2.WinForms.Guna2BorderlessForm(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.SwitchHideViewPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCloseForm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageLogoApp)).BeginInit();
            this.SuspendLayout();
            // 
            // ButtonSignIn
            // 
            this.ButtonSignIn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonSignIn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.ButtonSignIn.FlatAppearance.BorderSize = 0;
            this.ButtonSignIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonSignIn.Font = new System.Drawing.Font("Oswald", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonSignIn.ForeColor = System.Drawing.Color.White;
            this.ButtonSignIn.Location = new System.Drawing.Point(119, 588);
            this.ButtonSignIn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonSignIn.Name = "ButtonSignIn";
            this.ButtonSignIn.Size = new System.Drawing.Size(283, 50);
            this.ButtonSignIn.TabIndex = 2;
            this.ButtonSignIn.Text = "ВОЙТИ";
            this.ButtonSignIn.UseVisualStyleBackColor = false;
            this.ButtonSignIn.Click += new System.EventHandler(this.ButtonSignIn_Click);
            // 
            // ButtonJumpRestorePassword
            // 
            this.ButtonJumpRestorePassword.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ButtonJumpRestorePassword.AutoSize = true;
            this.ButtonJumpRestorePassword.FlatAppearance.BorderSize = 0;
            this.ButtonJumpRestorePassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonJumpRestorePassword.Font = new System.Drawing.Font("Oswald", 13F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonJumpRestorePassword.Location = new System.Drawing.Point(159, 786);
            this.ButtonJumpRestorePassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonJumpRestorePassword.Name = "ButtonJumpRestorePassword";
            this.ButtonJumpRestorePassword.Size = new System.Drawing.Size(204, 50);
            this.ButtonJumpRestorePassword.TabIndex = 4;
            this.ButtonJumpRestorePassword.Text = "СОЗДАТЬ АККАУНТ";
            this.ButtonJumpRestorePassword.UseVisualStyleBackColor = true;
            this.ButtonJumpRestorePassword.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // LabelCaption
            // 
            this.LabelCaption.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelCaption.AutoSize = true;
            this.LabelCaption.Font = new System.Drawing.Font("Oswald", 20F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelCaption.Location = new System.Drawing.Point(111, 74);
            this.LabelCaption.Name = "LabelCaption";
            this.LabelCaption.Size = new System.Drawing.Size(299, 58);
            this.LabelCaption.TabIndex = 4;
            this.LabelCaption.Text = "А В Т О Р И З А Ц И Я";
            this.LabelCaption.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LabelPhone
            // 
            this.LabelPhone.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelPhone.AutoSize = true;
            this.LabelPhone.Font = new System.Drawing.Font("Bebas Neue", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelPhone.Location = new System.Drawing.Point(116, 366);
            this.LabelPhone.Name = "LabelPhone";
            this.LabelPhone.Size = new System.Drawing.Size(151, 22);
            this.LabelPhone.TabIndex = 6;
            this.LabelPhone.Text = "Номер телефона";
            // 
            // LabelPassword
            // 
            this.LabelPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelPassword.AutoSize = true;
            this.LabelPassword.Font = new System.Drawing.Font("Bebas Neue", 10.2F);
            this.LabelPassword.Location = new System.Drawing.Point(117, 455);
            this.LabelPassword.Name = "LabelPassword";
            this.LabelPassword.Size = new System.Drawing.Size(72, 22);
            this.LabelPassword.TabIndex = 7;
            this.LabelPassword.Text = "Пароль";
            this.LabelPassword.Click += new System.EventHandler(this.label3_Click);
            // 
            // ButtonForgotPassword
            // 
            this.ButtonForgotPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ButtonForgotPassword.AutoSize = true;
            this.ButtonForgotPassword.BackColor = System.Drawing.Color.Transparent;
            this.ButtonForgotPassword.FlatAppearance.BorderSize = 0;
            this.ButtonForgotPassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonForgotPassword.Font = new System.Drawing.Font("Noto Sans", 10F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonForgotPassword.Location = new System.Drawing.Point(105, 641);
            this.ButtonForgotPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonForgotPassword.Name = "ButtonForgotPassword";
            this.ButtonForgotPassword.Size = new System.Drawing.Size(311, 64);
            this.ButtonForgotPassword.TabIndex = 3;
            this.ButtonForgotPassword.Text = "Не можете вспомнить пароль?\r\nВосстановить";
            this.ButtonForgotPassword.UseVisualStyleBackColor = false;
            this.ButtonForgotPassword.Click += new System.EventHandler(this.ButtonForgotPassword_Click);
            // 
            // TextPhone
            // 
            this.TextPhone.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextPhone.Animated = true;
            this.TextPhone.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextPhone.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextPhone.DefaultText = "";
            this.TextPhone.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextPhone.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextPhone.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPhone.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPhone.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPhone.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextPhone.ForeColor = System.Drawing.Color.Black;
            this.TextPhone.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPhone.Location = new System.Drawing.Point(121, 400);
            this.TextPhone.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextPhone.MaxLength = 32;
            this.TextPhone.Name = "TextPhone";
            this.TextPhone.PasswordChar = '\0';
            this.TextPhone.PlaceholderText = "7...";
            this.TextPhone.SelectedText = "";
            this.TextPhone.Size = new System.Drawing.Size(283, 42);
            this.TextPhone.TabIndex = 0;
            this.TextPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextPhone_KeyPress);
            // 
            // TextPassword
            // 
            this.TextPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.TextPassword.Animated = true;
            this.TextPassword.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextPassword.DefaultText = "";
            this.TextPassword.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextPassword.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextPassword.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPassword.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPassword.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPassword.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextPassword.ForeColor = System.Drawing.Color.Black;
            this.TextPassword.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPassword.Location = new System.Drawing.Point(121, 490);
            this.TextPassword.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextPassword.MaxLength = 16;
            this.TextPassword.Name = "TextPassword";
            this.TextPassword.PasswordChar = '\0';
            this.TextPassword.PlaceholderText = "Введите пароль\r\n";
            this.TextPassword.SelectedText = "";
            this.TextPassword.Size = new System.Drawing.Size(283, 42);
            this.TextPassword.TabIndex = 1;
            // 
            // SwitchHideViewPassword
            // 
            this.SwitchHideViewPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SwitchHideViewPassword.Image = global::Cleaning.Properties.Resources.hidden;
            this.SwitchHideViewPassword.Location = new System.Drawing.Point(371, 500);
            this.SwitchHideViewPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SwitchHideViewPassword.Name = "SwitchHideViewPassword";
            this.SwitchHideViewPassword.Size = new System.Drawing.Size(21, 22);
            this.SwitchHideViewPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.SwitchHideViewPassword.TabIndex = 14;
            this.SwitchHideViewPassword.TabStop = false;
            this.SwitchHideViewPassword.Click += new System.EventHandler(this.SwitchHideViewPassword_Click);
            this.SwitchHideViewPassword.MouseDown += new System.Windows.Forms.MouseEventHandler(this.SwitchHideViewPassword_MouseDown);
            this.SwitchHideViewPassword.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SwitchHideViewPassword_MouseUp);
            // 
            // ButtonCloseForm
            // 
            this.ButtonCloseForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonCloseForm.Image = global::Cleaning.Properties.Resources.close1;
            this.ButtonCloseForm.Location = new System.Drawing.Point(483, 12);
            this.ButtonCloseForm.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonCloseForm.Name = "ButtonCloseForm";
            this.ButtonCloseForm.Size = new System.Drawing.Size(25, 25);
            this.ButtonCloseForm.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ButtonCloseForm.TabIndex = 9;
            this.ButtonCloseForm.TabStop = false;
            this.ButtonCloseForm.Click += new System.EventHandler(this.pictureBox1_Click_1);
            // 
            // ImageLogoApp
            // 
            this.ImageLogoApp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.ImageLogoApp.BackColor = System.Drawing.Color.Transparent;
            this.ImageLogoApp.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.ImageLogoApp.Image = global::Cleaning.Properties.Resources.IconAppCleaning2;
            this.ImageLogoApp.Location = new System.Drawing.Point(161, 190);
            this.ImageLogoApp.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ImageLogoApp.Name = "ImageLogoApp";
            this.ImageLogoApp.Size = new System.Drawing.Size(197, 98);
            this.ImageLogoApp.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.ImageLogoApp.TabIndex = 5;
            this.ImageLogoApp.TabStop = false;
            this.ImageLogoApp.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // guna2BorderlessForm1
            // 
            this.guna2BorderlessForm1.AnimationInterval = 100;
            this.guna2BorderlessForm1.AnimationType = Guna.UI2.WinForms.Guna2BorderlessForm.AnimateWindowType.AW_VER_POSITIVE;
            this.guna2BorderlessForm1.ContainerControl = this;
            this.guna2BorderlessForm1.DockIndicatorTransparencyValue = 1D;
            this.guna2BorderlessForm1.DragStartTransparencyValue = 1D;
            this.guna2BorderlessForm1.ResizeForm = false;
            this.guna2BorderlessForm1.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.guna2BorderlessForm1.TransparentWhileDrag = true;
            // 
            // FormSignIn
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(520, 839);
            this.Controls.Add(this.SwitchHideViewPassword);
            this.Controls.Add(this.TextPassword);
            this.Controls.Add(this.TextPhone);
            this.Controls.Add(this.ButtonCloseForm);
            this.Controls.Add(this.ButtonForgotPassword);
            this.Controls.Add(this.LabelPassword);
            this.Controls.Add(this.LabelPhone);
            this.Controls.Add(this.ImageLogoApp);
            this.Controls.Add(this.LabelCaption);
            this.Controls.Add(this.ButtonJumpRestorePassword);
            this.Controls.Add(this.ButtonSignIn);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximumSize = new System.Drawing.Size(520, 839);
            this.MinimumSize = new System.Drawing.Size(520, 839);
            this.Name = "FormSignIn";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Авторизация - клининг";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Shown += new System.EventHandler(this.FormSignIn_Shown);
            ((System.ComponentModel.ISupportInitialize)(this.SwitchHideViewPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCloseForm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ImageLogoApp)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button ButtonSignIn;
        private System.Windows.Forms.Button ButtonJumpRestorePassword;
        private System.Windows.Forms.Label LabelCaption;
        private System.Windows.Forms.PictureBox ImageLogoApp;
        private System.Windows.Forms.Label LabelPhone;
        private System.Windows.Forms.Label LabelPassword;
        private System.Windows.Forms.Button ButtonForgotPassword;
        private System.Windows.Forms.PictureBox ButtonCloseForm;
        private Guna.UI2.WinForms.Guna2TextBox TextPhone;
        private Guna.UI2.WinForms.Guna2TextBox TextPassword;
        private System.Windows.Forms.PictureBox SwitchHideViewPassword;
        private Guna.UI2.WinForms.Guna2BorderlessForm guna2BorderlessForm1;
    }
}

