﻿
namespace Cleaning
{
    partial class FormSignUp
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormSignUp));
            this.LabelCaption = new System.Windows.Forms.Label();
            this.guna2Button1 = new Guna.UI2.WinForms.Guna2Button();
            this.ButtonToFormSignIn = new System.Windows.Forms.Button();
            this.ButtonAgree = new Guna.UI2.WinForms.Guna2CustomCheckBox();
            this.label13 = new System.Windows.Forms.Label();
            this.guna2BorderlessForm1 = new Guna.UI2.WinForms.Guna2BorderlessForm(this.components);
            this.TextPassword = new Guna.UI2.WinForms.Guna2TextBox();
            this.TextSeriosPassport = new Guna.UI2.WinForms.Guna2TextBox();
            this.TextPhone = new Guna.UI2.WinForms.Guna2TextBox();
            this.ButtonSendCode = new Guna.UI2.WinForms.Guna2Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.Date = new Guna.UI2.WinForms.Guna2DateTimePicker();
            this.label12 = new System.Windows.Forms.Label();
            this.TextLastName = new Guna.UI2.WinForms.Guna2TextBox();
            this.TextName = new Guna.UI2.WinForms.Guna2TextBox();
            this.TextAddress = new Guna.UI2.WinForms.Guna2TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TextSurname = new Guna.UI2.WinForms.Guna2TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.LabelConfirmCode = new System.Windows.Forms.Label();
            this.TextCodeConfirmPhone = new Guna.UI2.WinForms.Guna2TextBox();
            this.ButtonGetCode = new Guna.UI2.WinForms.Guna2Button();
            this.label8 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.TextNumberPassport = new Guna.UI2.WinForms.Guna2TextBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.SwitchHideViewPassword = new System.Windows.Forms.PictureBox();
            this.ButtonCloseForm = new System.Windows.Forms.PictureBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.guna2ComboBox1 = new Guna.UI2.WinForms.Guna2ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SwitchHideViewPassword)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCloseForm)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelCaption
            // 
            this.LabelCaption.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.LabelCaption.AutoSize = true;
            this.LabelCaption.Font = new System.Drawing.Font("Oswald", 20F);
            this.LabelCaption.Location = new System.Drawing.Point(349, 71);
            this.LabelCaption.Name = "LabelCaption";
            this.LabelCaption.Size = new System.Drawing.Size(291, 58);
            this.LabelCaption.TabIndex = 11;
            this.LabelCaption.Text = "Р Е Г И С Т Р А Ц И Я";
            this.LabelCaption.Click += new System.EventHandler(this.LabelCaption_Click);
            // 
            // guna2Button1
            // 
            this.guna2Button1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.guna2Button1.Animated = true;
            this.guna2Button1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.guna2Button1.Font = new System.Drawing.Font("Oswald", 14F);
            this.guna2Button1.ForeColor = System.Drawing.Color.White;
            this.guna2Button1.Location = new System.Drawing.Point(353, 644);
            this.guna2Button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.guna2Button1.Name = "guna2Button1";
            this.guna2Button1.Size = new System.Drawing.Size(283, 50);
            this.guna2Button1.TabIndex = 14;
            this.guna2Button1.Text = "СОЗДАТЬ";
            this.guna2Button1.Click += new System.EventHandler(this.guna2Button1_Click_1);
            // 
            // ButtonToFormSignIn
            // 
            this.ButtonToFormSignIn.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ButtonToFormSignIn.AutoSize = true;
            this.ButtonToFormSignIn.FlatAppearance.BorderSize = 0;
            this.ButtonToFormSignIn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ButtonToFormSignIn.Font = new System.Drawing.Font("Oswald", 13F, System.Drawing.FontStyle.Underline);
            this.ButtonToFormSignIn.Location = new System.Drawing.Point(449, 703);
            this.ButtonToFormSignIn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonToFormSignIn.Name = "ButtonToFormSignIn";
            this.ButtonToFormSignIn.Size = new System.Drawing.Size(96, 50);
            this.ButtonToFormSignIn.TabIndex = 15;
            this.ButtonToFormSignIn.Text = "НАЗАД";
            this.ButtonToFormSignIn.UseVisualStyleBackColor = true;
            this.ButtonToFormSignIn.Click += new System.EventHandler(this.ButtonToFormSignIn_Click_1);
            // 
            // ButtonAgree
            // 
            this.ButtonAgree.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.ButtonAgree.Animated = true;
            this.ButtonAgree.BackColor = System.Drawing.Color.Transparent;
            this.ButtonAgree.CheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.ButtonAgree.CheckedState.BorderRadius = 0;
            this.ButtonAgree.CheckedState.BorderThickness = 1;
            this.ButtonAgree.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.ButtonAgree.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ButtonAgree.Location = new System.Drawing.Point(265, 601);
            this.ButtonAgree.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonAgree.Name = "ButtonAgree";
            this.ButtonAgree.Size = new System.Drawing.Size(20, 20);
            this.ButtonAgree.TabIndex = 13;
            this.ButtonAgree.Text = "guna2CustomCheckBox1";
            this.ButtonAgree.UncheckedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.ButtonAgree.UncheckedState.BorderRadius = 0;
            this.ButtonAgree.UncheckedState.BorderThickness = 1;
            this.ButtonAgree.UncheckedState.FillColor = System.Drawing.Color.White;
            this.ButtonAgree.UseTransparentBackground = true;
            // 
            // label13
            // 
            this.label13.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Bebas Neue", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(292, 602);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(430, 19);
            this.label13.TabIndex = 51;
            this.label13.Text = "Даю согласия на обработку и хранение персональных данных";
            // 
            // guna2BorderlessForm1
            // 
            this.guna2BorderlessForm1.AnimationInterval = 100;
            this.guna2BorderlessForm1.AnimationType = Guna.UI2.WinForms.Guna2BorderlessForm.AnimateWindowType.AW_VER_POSITIVE;
            this.guna2BorderlessForm1.ContainerControl = this;
            this.guna2BorderlessForm1.DockForm = false;
            this.guna2BorderlessForm1.DockIndicatorColor = System.Drawing.Color.Transparent;
            this.guna2BorderlessForm1.DockIndicatorTransparencyValue = 1D;
            this.guna2BorderlessForm1.DragStartTransparencyValue = 1D;
            this.guna2BorderlessForm1.ShadowColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.guna2BorderlessForm1.TransparentWhileDrag = true;
            // 
            // TextPassword
            // 
            this.TextPassword.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TextPassword.Animated = true;
            this.TextPassword.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextPassword.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextPassword.DefaultText = "";
            this.TextPassword.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextPassword.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextPassword.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPassword.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPassword.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPassword.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextPassword.ForeColor = System.Drawing.Color.Black;
            this.TextPassword.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPassword.Location = new System.Drawing.Point(657, 398);
            this.TextPassword.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextPassword.MaxLength = 32;
            this.TextPassword.Name = "TextPassword";
            this.TextPassword.PasswordChar = '\0';
            this.TextPassword.PlaceholderText = "";
            this.TextPassword.SelectedText = "";
            this.TextPassword.Size = new System.Drawing.Size(283, 38);
            this.TextPassword.TabIndex = 10;
            // 
            // TextSeriosPassport
            // 
            this.TextSeriosPassport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TextSeriosPassport.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextSeriosPassport.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSeriosPassport.DefaultText = "";
            this.TextSeriosPassport.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextSeriosPassport.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextSeriosPassport.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextSeriosPassport.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextSeriosPassport.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextSeriosPassport.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextSeriosPassport.ForeColor = System.Drawing.Color.Black;
            this.TextSeriosPassport.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextSeriosPassport.Location = new System.Drawing.Point(52, 310);
            this.TextSeriosPassport.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextSeriosPassport.MaxLength = 4;
            this.TextSeriosPassport.Name = "TextSeriosPassport";
            this.TextSeriosPassport.PasswordChar = '\0';
            this.TextSeriosPassport.PlaceholderText = "Серия";
            this.TextSeriosPassport.SelectedText = "";
            this.TextSeriosPassport.Size = new System.Drawing.Size(111, 38);
            this.TextSeriosPassport.TabIndex = 3;
            this.TextSeriosPassport.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextSeriosPassport_KeyPress);
            // 
            // TextPhone
            // 
            this.TextPhone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TextPhone.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextPhone.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextPhone.DefaultText = "";
            this.TextPhone.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextPhone.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextPhone.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPhone.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextPhone.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPhone.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextPhone.ForeColor = System.Drawing.Color.Black;
            this.TextPhone.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextPhone.Location = new System.Drawing.Point(52, 398);
            this.TextPhone.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextPhone.MaxLength = 11;
            this.TextPhone.Name = "TextPhone";
            this.TextPhone.PasswordChar = '\0';
            this.TextPhone.PlaceholderText = "7...";
            this.TextPhone.SelectedText = "";
            this.TextPhone.Size = new System.Drawing.Size(283, 38);
            this.TextPhone.TabIndex = 6;
            this.TextPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextPhone_KeyPress_1);
            // 
            // ButtonSendCode
            // 
            this.ButtonSendCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.ButtonSendCode.Animated = true;
            this.ButtonSendCode.BorderRadius = 2;
            this.ButtonSendCode.DefaultAutoSize = true;
            this.ButtonSendCode.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonSendCode.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonSendCode.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonSendCode.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonSendCode.FillColor = System.Drawing.Color.Transparent;
            this.ButtonSendCode.Font = new System.Drawing.Font("Bebas Neue", 10.2F);
            this.ButtonSendCode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.ButtonSendCode.Location = new System.Drawing.Point(156, 511);
            this.ButtonSendCode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonSendCode.Name = "ButtonSendCode";
            this.ButtonSendCode.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.ButtonSendCode.Size = new System.Drawing.Size(127, 34);
            this.ButtonSendCode.TabIndex = 9;
            this.ButtonSendCode.Text = "Отправить";
            this.ButtonSendCode.Visible = false;
            this.ButtonSendCode.Click += new System.EventHandler(this.ButtonSendCode_Click_1);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Bebas Neue", 10.2F);
            this.label10.Location = new System.Drawing.Point(656, 367);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(176, 22);
            this.label10.TabIndex = 124;
            this.label10.Text = "Придумайте пароль";
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Bebas Neue", 10.2F);
            this.label4.Location = new System.Drawing.Point(48, 367);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(222, 22);
            this.label4.TabIndex = 121;
            this.label4.Text = "Введите номер телефона";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Bebas Neue", 10.2F);
            this.label6.Location = new System.Drawing.Point(48, 281);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(251, 22);
            this.label6.TabIndex = 123;
            this.label6.Text = "Введите паспортные данные";
            // 
            // Date
            // 
            this.Date.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.Date.Animated = true;
            this.Date.BackColor = System.Drawing.Color.Transparent;
            this.Date.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.Date.BorderThickness = 1;
            this.Date.Checked = true;
            this.Date.FillColor = System.Drawing.Color.White;
            this.Date.Font = new System.Drawing.Font("Noto Sans", 10.2F);
            this.Date.Format = System.Windows.Forms.DateTimePickerFormat.Long;
            this.Date.Location = new System.Drawing.Point(657, 310);
            this.Date.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Date.MaxDate = new System.DateTime(9998, 12, 31, 0, 0, 0, 0);
            this.Date.MinDate = new System.DateTime(1753, 1, 1, 0, 0, 0, 0);
            this.Date.Name = "Date";
            this.Date.Size = new System.Drawing.Size(283, 38);
            this.Date.TabIndex = 5;
            this.Date.Value = new System.DateTime(2022, 2, 13, 0, 59, 13, 775);
            // 
            // label12
            // 
            this.label12.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Bebas Neue", 10.2F);
            this.label12.Location = new System.Drawing.Point(653, 281);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(210, 22);
            this.label12.TabIndex = 118;
            this.label12.Text = "Введите дату рождения";
            // 
            // TextLastName
            // 
            this.TextLastName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TextLastName.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextLastName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextLastName.DefaultText = "";
            this.TextLastName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextLastName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextLastName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextLastName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextLastName.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextLastName.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextLastName.ForeColor = System.Drawing.Color.Black;
            this.TextLastName.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextLastName.Location = new System.Drawing.Point(657, 222);
            this.TextLastName.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextLastName.MaxLength = 32;
            this.TextLastName.Name = "TextLastName";
            this.TextLastName.PasswordChar = '\0';
            this.TextLastName.PlaceholderText = "Иванович";
            this.TextLastName.SelectedText = "";
            this.TextLastName.Size = new System.Drawing.Size(283, 38);
            this.TextLastName.TabIndex = 2;
            this.TextLastName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextLastName_KeyPress_1);
            // 
            // TextName
            // 
            this.TextName.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TextName.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextName.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextName.DefaultText = "";
            this.TextName.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextName.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextName.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextName.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextName.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextName.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextName.ForeColor = System.Drawing.Color.Black;
            this.TextName.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextName.Location = new System.Drawing.Point(356, 222);
            this.TextName.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextName.MaxLength = 20;
            this.TextName.Name = "TextName";
            this.TextName.PasswordChar = '\0';
            this.TextName.PlaceholderText = "Иван";
            this.TextName.SelectedText = "";
            this.TextName.Size = new System.Drawing.Size(283, 38);
            this.TextName.TabIndex = 1;
            this.TextName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextName_KeyPress_1);
            // 
            // TextAddress
            // 
            this.TextAddress.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TextAddress.Animated = true;
            this.TextAddress.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextAddress.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextAddress.DefaultText = "";
            this.TextAddress.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextAddress.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextAddress.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextAddress.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextAddress.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextAddress.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextAddress.ForeColor = System.Drawing.Color.Black;
            this.TextAddress.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextAddress.Location = new System.Drawing.Point(357, 526);
            this.TextAddress.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextAddress.MaxLength = 100;
            this.TextAddress.Name = "TextAddress";
            this.TextAddress.PasswordChar = '\0';
            this.TextAddress.PlaceholderText = "Ул. Демьяна Бедного, д. 121, кв. 123";
            this.TextAddress.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.TextAddress.SelectedText = "";
            this.TextAddress.Size = new System.Drawing.Size(584, 38);
            this.TextAddress.TabIndex = 11;
            this.TextAddress.Visible = false;
            this.TextAddress.TextChanged += new System.EventHandler(this.TextAddress_TextChanged);
            this.TextAddress.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextAddress_KeyPress_1);
            this.TextAddress.Leave += new System.EventHandler(this.TextAddress_Leave);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Bebas Neue", 10.2F);
            this.label9.Location = new System.Drawing.Point(353, 494);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(241, 22);
            this.label9.TabIndex = 109;
            this.label9.Text = "Введите адрес проживания";
            // 
            // TextSurname
            // 
            this.TextSurname.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TextSurname.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextSurname.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextSurname.DefaultText = "";
            this.TextSurname.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextSurname.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextSurname.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextSurname.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextSurname.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextSurname.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextSurname.ForeColor = System.Drawing.Color.Black;
            this.TextSurname.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextSurname.Location = new System.Drawing.Point(52, 222);
            this.TextSurname.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextSurname.MaxLength = 32;
            this.TextSurname.Name = "TextSurname";
            this.TextSurname.PasswordChar = '\0';
            this.TextSurname.PlaceholderText = "Иванов";
            this.TextSurname.SelectedText = "";
            this.TextSurname.Size = new System.Drawing.Size(283, 38);
            this.TextSurname.TabIndex = 0;
            this.TextSurname.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextSurname_KeyPress_1);
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Ubuntu Light", 7F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label11.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.label11.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label11.Location = new System.Drawing.Point(820, 196);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(97, 16);
            this.label11.TabIndex = 114;
            this.label11.Text = "(дополнительно)";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Bebas Neue", 10.2F);
            this.label1.Location = new System.Drawing.Point(48, 191);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(164, 22);
            this.label1.TabIndex = 111;
            this.label1.Text = "Введите фамилию";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Bebas Neue", 10.2F);
            this.label3.Location = new System.Drawing.Point(653, 193);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(159, 22);
            this.label3.TabIndex = 113;
            this.label3.Text = "Введите отчество";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Bebas Neue", 10.2F);
            this.label2.Location = new System.Drawing.Point(355, 191);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 22);
            this.label2.TabIndex = 112;
            this.label2.Text = "Введите имя";
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label5.Font = new System.Drawing.Font("Ubuntu", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.label5.Location = new System.Drawing.Point(657, 438);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(283, 85);
            this.label5.TabIndex = 133;
            this.label5.Text = "Пароль должен иметь:\r\n- минимум 1 заглавный символ и цифру,\r\n- длину от 8 символо" +
    "в.\r\n\r\n";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label7.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.label7.Location = new System.Drawing.Point(353, 565);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(260, 18);
            this.label7.TabIndex = 134;
            this.label7.Text = "Введите корректный адрес проживания!";
            // 
            // LabelConfirmCode
            // 
            this.LabelConfirmCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.LabelConfirmCode.AutoSize = true;
            this.LabelConfirmCode.Font = new System.Drawing.Font("Bebas Neue", 10.2F);
            this.LabelConfirmCode.ForeColor = System.Drawing.Color.Black;
            this.LabelConfirmCode.Location = new System.Drawing.Point(48, 479);
            this.LabelConfirmCode.Name = "LabelConfirmCode";
            this.LabelConfirmCode.Size = new System.Drawing.Size(265, 22);
            this.LabelConfirmCode.TabIndex = 122;
            this.LabelConfirmCode.Text = "Код подтверждения телефона";
            this.LabelConfirmCode.Visible = false;
            // 
            // TextCodeConfirmPhone
            // 
            this.TextCodeConfirmPhone.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TextCodeConfirmPhone.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextCodeConfirmPhone.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextCodeConfirmPhone.DefaultText = "";
            this.TextCodeConfirmPhone.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextCodeConfirmPhone.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextCodeConfirmPhone.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextCodeConfirmPhone.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextCodeConfirmPhone.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextCodeConfirmPhone.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextCodeConfirmPhone.ForeColor = System.Drawing.Color.Black;
            this.TextCodeConfirmPhone.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextCodeConfirmPhone.Location = new System.Drawing.Point(52, 510);
            this.TextCodeConfirmPhone.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextCodeConfirmPhone.MaxLength = 4;
            this.TextCodeConfirmPhone.Name = "TextCodeConfirmPhone";
            this.TextCodeConfirmPhone.PasswordChar = '\0';
            this.TextCodeConfirmPhone.PlaceholderText = "";
            this.TextCodeConfirmPhone.SelectedText = "";
            this.TextCodeConfirmPhone.Size = new System.Drawing.Size(97, 38);
            this.TextCodeConfirmPhone.TabIndex = 8;
            this.TextCodeConfirmPhone.Visible = false;
            this.TextCodeConfirmPhone.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextCodeConfirmPhone_KeyPress);
            // 
            // ButtonGetCode
            // 
            this.ButtonGetCode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.ButtonGetCode.Animated = true;
            this.ButtonGetCode.BackColor = System.Drawing.Color.Transparent;
            this.ButtonGetCode.BorderRadius = 2;
            this.ButtonGetCode.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonGetCode.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonGetCode.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonGetCode.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonGetCode.FillColor = System.Drawing.Color.White;
            this.ButtonGetCode.Font = new System.Drawing.Font("Bebas Neue", 10.2F);
            this.ButtonGetCode.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.ButtonGetCode.Location = new System.Drawing.Point(52, 441);
            this.ButtonGetCode.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonGetCode.MinimumSize = new System.Drawing.Size(151, 34);
            this.ButtonGetCode.Name = "ButtonGetCode";
            this.ButtonGetCode.Size = new System.Drawing.Size(219, 34);
            this.ButtonGetCode.TabIndex = 7;
            this.ButtonGetCode.Text = "Получить код";
            this.ButtonGetCode.Click += new System.EventHandler(this.ButtonGetCode_Click_2);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 7.8F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.label8.Location = new System.Drawing.Point(49, 550);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(203, 36);
            this.label8.TabIndex = 135;
            this.label8.Text = "Проверьте код подтверждения\r\nОтправленный на ****\r\n";
            this.label8.Visible = false;
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // TextNumberPassport
            // 
            this.TextNumberPassport.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.TextNumberPassport.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextNumberPassport.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextNumberPassport.DefaultText = "";
            this.TextNumberPassport.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextNumberPassport.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextNumberPassport.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextNumberPassport.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextNumberPassport.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextNumberPassport.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextNumberPassport.ForeColor = System.Drawing.Color.Black;
            this.TextNumberPassport.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextNumberPassport.Location = new System.Drawing.Point(171, 310);
            this.TextNumberPassport.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextNumberPassport.MaxLength = 6;
            this.TextNumberPassport.Name = "TextNumberPassport";
            this.TextNumberPassport.PasswordChar = '\0';
            this.TextNumberPassport.PlaceholderText = "Номер";
            this.TextNumberPassport.SelectedText = "";
            this.TextNumberPassport.Size = new System.Drawing.Size(164, 38);
            this.TextNumberPassport.TabIndex = 4;
            this.TextNumberPassport.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.guna2TextBox1_KeyPress);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)));
            this.pictureBox1.BackColor = System.Drawing.Color.Transparent;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.pictureBox1.Image = global::Cleaning.Properties.Resources.IconAppCleaning2;
            this.pictureBox1.Location = new System.Drawing.Point(353, 283);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(283, 166);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 132;
            this.pictureBox1.TabStop = false;
            // 
            // SwitchHideViewPassword
            // 
            this.SwitchHideViewPassword.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SwitchHideViewPassword.Image = global::Cleaning.Properties.Resources.hidden;
            this.SwitchHideViewPassword.Location = new System.Drawing.Point(908, 406);
            this.SwitchHideViewPassword.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.SwitchHideViewPassword.Name = "SwitchHideViewPassword";
            this.SwitchHideViewPassword.Size = new System.Drawing.Size(21, 22);
            this.SwitchHideViewPassword.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.SwitchHideViewPassword.TabIndex = 120;
            this.SwitchHideViewPassword.TabStop = false;
            // 
            // ButtonCloseForm
            // 
            this.ButtonCloseForm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonCloseForm.Image = global::Cleaning.Properties.Resources.close1;
            this.ButtonCloseForm.Location = new System.Drawing.Point(953, 12);
            this.ButtonCloseForm.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonCloseForm.Name = "ButtonCloseForm";
            this.ButtonCloseForm.Size = new System.Drawing.Size(25, 25);
            this.ButtonCloseForm.SizeMode = System.Windows.Forms.PictureBoxSizeMode.CenterImage;
            this.ButtonCloseForm.TabIndex = 10;
            this.ButtonCloseForm.TabStop = false;
            this.ButtonCloseForm.Click += new System.EventHandler(this.ButtonCloseForm_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(357, 529);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(584, 35);
            this.comboBox1.TabIndex = 12;
            this.comboBox1.TextUpdate += new System.EventHandler(this.comboBox1_TextUpdate_1);
            this.comboBox1.TextChanged += new System.EventHandler(this.comboBox1_TextChanged);
            this.comboBox1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.comboBox1_KeyPress);
            // 
            // guna2ComboBox1
            // 
            this.guna2ComboBox1.BackColor = System.Drawing.Color.Transparent;
            this.guna2ComboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox1.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox1.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.guna2ComboBox1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox1.ItemHeight = 25;
            this.guna2ComboBox1.Location = new System.Drawing.Point(357, 526);
            this.guna2ComboBox1.Name = "guna2ComboBox1";
            this.guna2ComboBox1.Size = new System.Drawing.Size(583, 31);
            this.guna2ComboBox1.TabIndex = 136;
            this.guna2ComboBox1.Visible = false;
            this.guna2ComboBox1.SelectedIndexChanged += new System.EventHandler(this.guna2ComboBox1_SelectedIndexChanged);
            // 
            // FormSignUp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(989, 756);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.TextNumberPassport);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.ButtonGetCode);
            this.Controls.Add(this.SwitchHideViewPassword);
            this.Controls.Add(this.TextPassword);
            this.Controls.Add(this.TextSeriosPassport);
            this.Controls.Add(this.TextCodeConfirmPhone);
            this.Controls.Add(this.TextPhone);
            this.Controls.Add(this.ButtonSendCode);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.LabelConfirmCode);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.Date);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.TextLastName);
            this.Controls.Add(this.TextName);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.TextSurname);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.ButtonAgree);
            this.Controls.Add(this.ButtonToFormSignIn);
            this.Controls.Add(this.guna2Button1);
            this.Controls.Add(this.LabelCaption);
            this.Controls.Add(this.ButtonCloseForm);
            this.Controls.Add(this.TextAddress);
            this.Controls.Add(this.guna2ComboBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximumSize = new System.Drawing.Size(989, 756);
            this.MinimumSize = new System.Drawing.Size(989, 756);
            this.Name = "FormSignUp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormSignUp";
            this.Load += new System.EventHandler(this.FormSignUp_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SwitchHideViewPassword)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ButtonCloseForm)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.PictureBox ButtonCloseForm;
        private System.Windows.Forms.Label LabelCaption;
        private Guna.UI2.WinForms.Guna2Button guna2Button1;
        private System.Windows.Forms.Button ButtonToFormSignIn;
        private Guna.UI2.WinForms.Guna2CustomCheckBox ButtonAgree;
        private System.Windows.Forms.Label label13;
        private Guna.UI2.WinForms.Guna2BorderlessForm guna2BorderlessForm1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Guna.UI2.WinForms.Guna2Button ButtonGetCode;
        private System.Windows.Forms.PictureBox SwitchHideViewPassword;
        private Guna.UI2.WinForms.Guna2TextBox TextPassword;
        private Guna.UI2.WinForms.Guna2TextBox TextSeriosPassport;
        private Guna.UI2.WinForms.Guna2TextBox TextCodeConfirmPhone;
        private Guna.UI2.WinForms.Guna2TextBox TextPhone;
        private Guna.UI2.WinForms.Guna2Button ButtonSendCode;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LabelConfirmCode;
        private System.Windows.Forms.Label label6;
        private Guna.UI2.WinForms.Guna2DateTimePicker Date;
        private System.Windows.Forms.Label label12;
        private Guna.UI2.WinForms.Guna2TextBox TextLastName;
        private Guna.UI2.WinForms.Guna2TextBox TextName;
        private Guna.UI2.WinForms.Guna2TextBox TextAddress;
        private System.Windows.Forms.Label label9;
        private Guna.UI2.WinForms.Guna2TextBox TextSurname;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Timer timer1;
        private Guna.UI2.WinForms.Guna2TextBox TextNumberPassport;
        private System.Windows.Forms.ComboBox comboBox1;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox1;
    }
}