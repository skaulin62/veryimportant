﻿
namespace Cleaning
{
    partial class CardCleanerConfirm
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(CardCleanerConfirm));
            this.label1 = new System.Windows.Forms.Label();
            this.ButtonConfirm = new Guna.UI2.WinForms.Guna2Button();
            this.TextComment = new Guna.UI2.WinForms.Guna2TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.reCalc = new Guna.UI2.WinForms.Guna2Button();
            this.TextCountOrder = new Guna.UI2.WinForms.Guna2TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Noto Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(23, 20);
            this.label1.MaximumSize = new System.Drawing.Size(1000, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(715, 189);
            this.label1.TabIndex = 33;
            this.label1.Text = resources.GetString("label1.Text");
            // 
            // ButtonConfirm
            // 
            this.ButtonConfirm.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.ButtonConfirm.Animated = true;
            this.ButtonConfirm.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonConfirm.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonConfirm.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonConfirm.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonConfirm.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.ButtonConfirm.Font = new System.Drawing.Font("Noto Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ButtonConfirm.ForeColor = System.Drawing.Color.White;
            this.ButtonConfirm.Location = new System.Drawing.Point(820, 346);
            this.ButtonConfirm.Name = "ButtonConfirm";
            this.ButtonConfirm.Size = new System.Drawing.Size(236, 41);
            this.ButtonConfirm.TabIndex = 34;
            this.ButtonConfirm.Text = "Подтвердить";
            this.ButtonConfirm.Click += new System.EventHandler(this.ButtonConfirm_Click);
            // 
            // TextComment
            // 
            this.TextComment.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.TextComment.AutoScroll = true;
            this.TextComment.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextComment.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextComment.DefaultText = "";
            this.TextComment.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextComment.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextComment.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextComment.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextComment.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextComment.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.TextComment.ForeColor = System.Drawing.Color.Black;
            this.TextComment.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextComment.Location = new System.Drawing.Point(370, 56);
            this.TextComment.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.TextComment.MaxLength = 200;
            this.TextComment.Multiline = true;
            this.TextComment.Name = "TextComment";
            this.TextComment.PasswordChar = '\0';
            this.TextComment.PlaceholderText = "...";
            this.TextComment.SelectedText = "";
            this.TextComment.Size = new System.Drawing.Size(686, 117);
            this.TextComment.TabIndex = 36;
            this.TextComment.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextComment_KeyPress);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Noto Sans", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(365, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(228, 27);
            this.label2.TabIndex = 35;
            this.label2.Text = "Примечание к заказу:\r\n";
            // 
            // reCalc
            // 
            this.reCalc.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.reCalc.Animated = true;
            this.reCalc.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.reCalc.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.reCalc.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.reCalc.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.reCalc.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.reCalc.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.reCalc.ForeColor = System.Drawing.Color.White;
            this.reCalc.Location = new System.Drawing.Point(226, 346);
            this.reCalc.Name = "reCalc";
            this.reCalc.Size = new System.Drawing.Size(195, 38);
            this.reCalc.TabIndex = 37;
            this.reCalc.Text = "Перерасчет";
            this.reCalc.Click += new System.EventHandler(this.reCalc_Click);
            // 
            // TextCountOrder
            // 
            this.TextCountOrder.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.TextCountOrder.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.TextCountOrder.DefaultText = "";
            this.TextCountOrder.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.TextCountOrder.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.TextCountOrder.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextCountOrder.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.TextCountOrder.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextCountOrder.Font = new System.Drawing.Font("Noto Sans", 12F);
            this.TextCountOrder.ForeColor = System.Drawing.Color.Black;
            this.TextCountOrder.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.TextCountOrder.Location = new System.Drawing.Point(28, 289);
            this.TextCountOrder.Margin = new System.Windows.Forms.Padding(4, 2, 4, 2);
            this.TextCountOrder.MaxLength = 3;
            this.TextCountOrder.Name = "TextCountOrder";
            this.TextCountOrder.PasswordChar = '\0';
            this.TextCountOrder.PlaceholderText = "20";
            this.TextCountOrder.SelectedText = "";
            this.TextCountOrder.Size = new System.Drawing.Size(143, 38);
            this.TextCountOrder.TabIndex = 38;
            this.TextCountOrder.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TextCountOrder_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.label4.Location = new System.Drawing.Point(24, 257);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(397, 24);
            this.label4.TabIndex = 197;
            this.label4.Text = "Введите кол-во кв. метров для перерасчета";
            // 
            // CardCleanerConfirm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.label4);
            this.Controls.Add(this.TextCountOrder);
            this.Controls.Add(this.reCalc);
            this.Controls.Add(this.TextComment);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.ButtonConfirm);
            this.Controls.Add(this.label1);
            this.Name = "CardCleanerConfirm";
            this.Padding = new System.Windows.Forms.Padding(20);
            this.Size = new System.Drawing.Size(1079, 410);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Guna.UI2.WinForms.Guna2Button ButtonConfirm;
        private Guna.UI2.WinForms.Guna2TextBox TextComment;
        private System.Windows.Forms.Label label2;
        private Guna.UI2.WinForms.Guna2Button reCalc;
        private Guna.UI2.WinForms.Guna2TextBox TextCountOrder;
        private System.Windows.Forms.Label label4;
    }
}
