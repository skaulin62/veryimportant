﻿
namespace Cleaning
{
    partial class FormNavigation
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormNavigation));
            this.panel1 = new System.Windows.Forms.Panel();
            this.LanelNameCaption = new System.Windows.Forms.Label();
            this.PictureAppIcon = new System.Windows.Forms.PictureBox();
            this.guna2ImageButton3 = new Guna.UI2.WinForms.Guna2ImageButton();
            this.guna2ImageButton2 = new Guna.UI2.WinForms.Guna2ImageButton();
            this.guna2ImageButton1 = new Guna.UI2.WinForms.Guna2ImageButton();
            this.guna2Button1 = new Guna.UI2.WinForms.Guna2Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.PanelTop = new System.Windows.Forms.Panel();
            this.PanelMenu = new System.Windows.Forms.Panel();
            this.ButtonCleanerConfirmOrder = new Guna.UI2.WinForms.Guna2Button();
            this.ButtonOperatorSetCleaner = new Guna.UI2.WinForms.Guna2Button();
            this.ButtonAdminStatisticAndReport = new Guna.UI2.WinForms.Guna2Button();
            this.ButtonAdminEmployees = new Guna.UI2.WinForms.Guna2Button();
            this.ButtonAdminServices = new Guna.UI2.WinForms.Guna2Button();
            this.ButtonClientOrders = new Guna.UI2.WinForms.Guna2Button();
            this.ButtonClientServices = new Guna.UI2.WinForms.Guna2Button();
            this.PanelSplitMenuContent = new System.Windows.Forms.Panel();
            this.LabelProfile = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.LabelWelcome = new System.Windows.Forms.Label();
            this.PanelContent = new System.Windows.Forms.Panel();
            this.guna2BorderlessForm1 = new Guna.UI2.WinForms.Guna2BorderlessForm(this.components);
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureAppIcon)).BeginInit();
            this.PanelTop.SuspendLayout();
            this.PanelMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.panel1.Controls.Add(this.LanelNameCaption);
            this.panel1.Controls.Add(this.PictureAppIcon);
            this.panel1.Controls.Add(this.guna2ImageButton3);
            this.panel1.Controls.Add(this.guna2ImageButton2);
            this.panel1.Controls.Add(this.guna2ImageButton1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Padding = new System.Windows.Forms.Padding(3, 0, 0, 0);
            this.panel1.Size = new System.Drawing.Size(1553, 39);
            this.panel1.TabIndex = 0;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            // 
            // LanelNameCaption
            // 
            this.LanelNameCaption.AutoSize = true;
            this.LanelNameCaption.BackColor = System.Drawing.Color.Transparent;
            this.LanelNameCaption.Dock = System.Windows.Forms.DockStyle.Left;
            this.LanelNameCaption.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LanelNameCaption.ForeColor = System.Drawing.Color.White;
            this.LanelNameCaption.Location = new System.Drawing.Point(40, 0);
            this.LanelNameCaption.Name = "LanelNameCaption";
            this.LanelNameCaption.Padding = new System.Windows.Forms.Padding(5, 9, 5, 0);
            this.LanelNameCaption.Size = new System.Drawing.Size(96, 32);
            this.LanelNameCaption.TabIndex = 3;
            this.LanelNameCaption.Text = "Клининг";
            // 
            // PictureAppIcon
            // 
            this.PictureAppIcon.Dock = System.Windows.Forms.DockStyle.Left;
            this.PictureAppIcon.Image = global::Cleaning.Properties.Resources.IconAppCleaning;
            this.PictureAppIcon.Location = new System.Drawing.Point(3, 0);
            this.PictureAppIcon.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PictureAppIcon.Name = "PictureAppIcon";
            this.PictureAppIcon.Size = new System.Drawing.Size(37, 39);
            this.PictureAppIcon.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.PictureAppIcon.TabIndex = 3;
            this.PictureAppIcon.TabStop = false;
            // 
            // guna2ImageButton3
            // 
            this.guna2ImageButton3.BackColor = System.Drawing.Color.Transparent;
            this.guna2ImageButton3.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.guna2ImageButton3.Dock = System.Windows.Forms.DockStyle.Right;
            this.guna2ImageButton3.HoverState.ImageSize = new System.Drawing.Size(18, 2);
            this.guna2ImageButton3.Image = global::Cleaning.Properties.Resources.hide1;
            this.guna2ImageButton3.ImageOffset = new System.Drawing.Point(0, 0);
            this.guna2ImageButton3.ImageRotate = 0F;
            this.guna2ImageButton3.ImageSize = new System.Drawing.Size(18, 2);
            this.guna2ImageButton3.Location = new System.Drawing.Point(1421, 0);
            this.guna2ImageButton3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.guna2ImageButton3.Name = "guna2ImageButton3";
            this.guna2ImageButton3.PressedState.ImageSize = new System.Drawing.Size(18, 2);
            this.guna2ImageButton3.Size = new System.Drawing.Size(44, 39);
            this.guna2ImageButton3.TabIndex = 4;
            this.guna2ImageButton3.Click += new System.EventHandler(this.guna2ImageButton3_Click);
            // 
            // guna2ImageButton2
            // 
            this.guna2ImageButton2.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.guna2ImageButton2.Dock = System.Windows.Forms.DockStyle.Right;
            this.guna2ImageButton2.HoverState.ImageSize = new System.Drawing.Size(18, 18);
            this.guna2ImageButton2.Image = global::Cleaning.Properties.Resources.expend1;
            this.guna2ImageButton2.ImageOffset = new System.Drawing.Point(0, 0);
            this.guna2ImageButton2.ImageRotate = 0F;
            this.guna2ImageButton2.ImageSize = new System.Drawing.Size(18, 18);
            this.guna2ImageButton2.Location = new System.Drawing.Point(1465, 0);
            this.guna2ImageButton2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.guna2ImageButton2.Name = "guna2ImageButton2";
            this.guna2ImageButton2.PressedState.ImageSize = new System.Drawing.Size(18, 18);
            this.guna2ImageButton2.Size = new System.Drawing.Size(44, 39);
            this.guna2ImageButton2.TabIndex = 3;
            this.guna2ImageButton2.Click += new System.EventHandler(this.guna2ImageButton2_Click);
            // 
            // guna2ImageButton1
            // 
            this.guna2ImageButton1.AnimatedGIF = true;
            this.guna2ImageButton1.CheckedState.ImageSize = new System.Drawing.Size(64, 64);
            this.guna2ImageButton1.Dock = System.Windows.Forms.DockStyle.Right;
            this.guna2ImageButton1.HoverState.Image = global::Cleaning.Properties.Resources.closeWhite;
            this.guna2ImageButton1.HoverState.ImageSize = new System.Drawing.Size(18, 18);
            this.guna2ImageButton1.Image = global::Cleaning.Properties.Resources.closeWhite;
            this.guna2ImageButton1.ImageOffset = new System.Drawing.Point(0, 0);
            this.guna2ImageButton1.ImageRotate = 0F;
            this.guna2ImageButton1.ImageSize = new System.Drawing.Size(18, 18);
            this.guna2ImageButton1.Location = new System.Drawing.Point(1509, 0);
            this.guna2ImageButton1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.guna2ImageButton1.Name = "guna2ImageButton1";
            this.guna2ImageButton1.PressedState.ImageSize = new System.Drawing.Size(18, 18);
            this.guna2ImageButton1.Size = new System.Drawing.Size(44, 39);
            this.guna2ImageButton1.TabIndex = 1;
            this.guna2ImageButton1.Click += new System.EventHandler(this.guna2ImageButton1_Click);
            // 
            // guna2Button1
            // 
            this.guna2Button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.guna2Button1.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.guna2Button1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.guna2Button1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.guna2Button1.FillColor = System.Drawing.Color.Transparent;
            this.guna2Button1.Font = new System.Drawing.Font("Noto Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2Button1.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(69)))), ((int)(((byte)(69)))));
            this.guna2Button1.Location = new System.Drawing.Point(1435, 10);
            this.guna2Button1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.guna2Button1.Name = "guna2Button1";
            this.guna2Button1.Size = new System.Drawing.Size(107, 36);
            this.guna2Button1.TabIndex = 1;
            this.guna2Button1.Text = "Выйти";
            this.guna2Button1.Click += new System.EventHandler(this.guna2Button1_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(183)))), ((int)(((byte)(183)))), ((int)(((byte)(183)))));
            this.panel2.Location = new System.Drawing.Point(1081, 178);
            this.panel2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(472, 2);
            this.panel2.TabIndex = 2;
            // 
            // PanelTop
            // 
            this.PanelTop.Controls.Add(this.PanelMenu);
            this.PanelTop.Controls.Add(this.PanelSplitMenuContent);
            this.PanelTop.Controls.Add(this.LabelProfile);
            this.PanelTop.Controls.Add(this.pictureBox1);
            this.PanelTop.Controls.Add(this.LabelWelcome);
            this.PanelTop.Controls.Add(this.panel2);
            this.PanelTop.Controls.Add(this.guna2Button1);
            this.PanelTop.Dock = System.Windows.Forms.DockStyle.Top;
            this.PanelTop.Location = new System.Drawing.Point(0, 39);
            this.PanelTop.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelTop.Name = "PanelTop";
            this.PanelTop.Size = new System.Drawing.Size(1553, 181);
            this.PanelTop.TabIndex = 3;
            // 
            // PanelMenu
            // 
            this.PanelMenu.AutoScroll = true;
            this.PanelMenu.Controls.Add(this.ButtonCleanerConfirmOrder);
            this.PanelMenu.Controls.Add(this.ButtonOperatorSetCleaner);
            this.PanelMenu.Controls.Add(this.ButtonAdminStatisticAndReport);
            this.PanelMenu.Controls.Add(this.ButtonAdminEmployees);
            this.PanelMenu.Controls.Add(this.ButtonAdminServices);
            this.PanelMenu.Controls.Add(this.ButtonClientOrders);
            this.PanelMenu.Controls.Add(this.ButtonClientServices);
            this.PanelMenu.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelMenu.Location = new System.Drawing.Point(0, 130);
            this.PanelMenu.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelMenu.Name = "PanelMenu";
            this.PanelMenu.Size = new System.Drawing.Size(1553, 47);
            this.PanelMenu.TabIndex = 8;
            // 
            // ButtonCleanerConfirmOrder
            // 
            this.ButtonCleanerConfirmOrder.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonCleanerConfirmOrder.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonCleanerConfirmOrder.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonCleanerConfirmOrder.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonCleanerConfirmOrder.Dock = System.Windows.Forms.DockStyle.Right;
            this.ButtonCleanerConfirmOrder.FillColor = System.Drawing.Color.White;
            this.ButtonCleanerConfirmOrder.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Bold);
            this.ButtonCleanerConfirmOrder.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.ButtonCleanerConfirmOrder.Location = new System.Drawing.Point(-827, 0);
            this.ButtonCleanerConfirmOrder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonCleanerConfirmOrder.Name = "ButtonCleanerConfirmOrder";
            this.ButtonCleanerConfirmOrder.Size = new System.Drawing.Size(340, 47);
            this.ButtonCleanerConfirmOrder.TabIndex = 6;
            this.ButtonCleanerConfirmOrder.Text = "Мои уборки";
            this.ButtonCleanerConfirmOrder.Click += new System.EventHandler(this.ButtonCleanerConfirmOrder_Click);
            // 
            // ButtonOperatorSetCleaner
            // 
            this.ButtonOperatorSetCleaner.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonOperatorSetCleaner.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonOperatorSetCleaner.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonOperatorSetCleaner.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonOperatorSetCleaner.Dock = System.Windows.Forms.DockStyle.Right;
            this.ButtonOperatorSetCleaner.FillColor = System.Drawing.Color.White;
            this.ButtonOperatorSetCleaner.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Bold);
            this.ButtonOperatorSetCleaner.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.ButtonOperatorSetCleaner.Location = new System.Drawing.Point(-487, 0);
            this.ButtonOperatorSetCleaner.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonOperatorSetCleaner.Name = "ButtonOperatorSetCleaner";
            this.ButtonOperatorSetCleaner.Size = new System.Drawing.Size(340, 47);
            this.ButtonOperatorSetCleaner.TabIndex = 5;
            this.ButtonOperatorSetCleaner.Text = "Назначить уборщика";
            this.ButtonOperatorSetCleaner.Click += new System.EventHandler(this.ButtonOperatorSetCleaner_Click);
            // 
            // ButtonAdminStatisticAndReport
            // 
            this.ButtonAdminStatisticAndReport.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonAdminStatisticAndReport.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonAdminStatisticAndReport.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonAdminStatisticAndReport.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonAdminStatisticAndReport.Dock = System.Windows.Forms.DockStyle.Right;
            this.ButtonAdminStatisticAndReport.FillColor = System.Drawing.Color.White;
            this.ButtonAdminStatisticAndReport.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Bold);
            this.ButtonAdminStatisticAndReport.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.ButtonAdminStatisticAndReport.Location = new System.Drawing.Point(-147, 0);
            this.ButtonAdminStatisticAndReport.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonAdminStatisticAndReport.Name = "ButtonAdminStatisticAndReport";
            this.ButtonAdminStatisticAndReport.Size = new System.Drawing.Size(340, 47);
            this.ButtonAdminStatisticAndReport.TabIndex = 4;
            this.ButtonAdminStatisticAndReport.Text = "Статистика";
            this.ButtonAdminStatisticAndReport.Click += new System.EventHandler(this.ButtonAdminStatisticAndReport_Click);
            // 
            // ButtonAdminEmployees
            // 
            this.ButtonAdminEmployees.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonAdminEmployees.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonAdminEmployees.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonAdminEmployees.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonAdminEmployees.Dock = System.Windows.Forms.DockStyle.Right;
            this.ButtonAdminEmployees.FillColor = System.Drawing.Color.White;
            this.ButtonAdminEmployees.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Bold);
            this.ButtonAdminEmployees.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.ButtonAdminEmployees.Location = new System.Drawing.Point(193, 0);
            this.ButtonAdminEmployees.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonAdminEmployees.Name = "ButtonAdminEmployees";
            this.ButtonAdminEmployees.Size = new System.Drawing.Size(340, 47);
            this.ButtonAdminEmployees.TabIndex = 3;
            this.ButtonAdminEmployees.Text = "Сотрудники";
            this.ButtonAdminEmployees.Click += new System.EventHandler(this.ButtonAdminEmployees_Click);
            // 
            // ButtonAdminServices
            // 
            this.ButtonAdminServices.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonAdminServices.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonAdminServices.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonAdminServices.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonAdminServices.Dock = System.Windows.Forms.DockStyle.Right;
            this.ButtonAdminServices.FillColor = System.Drawing.Color.White;
            this.ButtonAdminServices.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Bold);
            this.ButtonAdminServices.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.ButtonAdminServices.Location = new System.Drawing.Point(533, 0);
            this.ButtonAdminServices.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonAdminServices.Name = "ButtonAdminServices";
            this.ButtonAdminServices.Size = new System.Drawing.Size(340, 47);
            this.ButtonAdminServices.TabIndex = 2;
            this.ButtonAdminServices.Text = "Услуги";
            this.ButtonAdminServices.Click += new System.EventHandler(this.ButtonAdminServices_Click);
            // 
            // ButtonClientOrders
            // 
            this.ButtonClientOrders.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonClientOrders.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonClientOrders.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonClientOrders.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonClientOrders.Dock = System.Windows.Forms.DockStyle.Right;
            this.ButtonClientOrders.FillColor = System.Drawing.Color.White;
            this.ButtonClientOrders.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Bold);
            this.ButtonClientOrders.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.ButtonClientOrders.Location = new System.Drawing.Point(873, 0);
            this.ButtonClientOrders.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonClientOrders.Name = "ButtonClientOrders";
            this.ButtonClientOrders.Size = new System.Drawing.Size(340, 47);
            this.ButtonClientOrders.TabIndex = 1;
            this.ButtonClientOrders.Text = "Мои заказы";
            this.ButtonClientOrders.Click += new System.EventHandler(this.ButtonClientOrders_Click);
            // 
            // ButtonClientServices
            // 
            this.ButtonClientServices.DisabledState.BorderColor = System.Drawing.Color.DarkGray;
            this.ButtonClientServices.DisabledState.CustomBorderColor = System.Drawing.Color.DarkGray;
            this.ButtonClientServices.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(169)))), ((int)(((byte)(169)))), ((int)(((byte)(169)))));
            this.ButtonClientServices.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(141)))), ((int)(((byte)(141)))), ((int)(((byte)(141)))));
            this.ButtonClientServices.Dock = System.Windows.Forms.DockStyle.Right;
            this.ButtonClientServices.FillColor = System.Drawing.Color.White;
            this.ButtonClientServices.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Bold);
            this.ButtonClientServices.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(92)))), ((int)(((byte)(90)))), ((int)(((byte)(90)))));
            this.ButtonClientServices.Location = new System.Drawing.Point(1213, 0);
            this.ButtonClientServices.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ButtonClientServices.Name = "ButtonClientServices";
            this.ButtonClientServices.Size = new System.Drawing.Size(340, 47);
            this.ButtonClientServices.TabIndex = 0;
            this.ButtonClientServices.Text = "Выбрать услуги";
            this.ButtonClientServices.Click += new System.EventHandler(this.ButtonClientServices_Click);
            // 
            // PanelSplitMenuContent
            // 
            this.PanelSplitMenuContent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(188)))), ((int)(((byte)(188)))), ((int)(((byte)(188)))));
            this.PanelSplitMenuContent.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.PanelSplitMenuContent.Location = new System.Drawing.Point(0, 177);
            this.PanelSplitMenuContent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelSplitMenuContent.Name = "PanelSplitMenuContent";
            this.PanelSplitMenuContent.Size = new System.Drawing.Size(1553, 4);
            this.PanelSplitMenuContent.TabIndex = 7;
            // 
            // LabelProfile
            // 
            this.LabelProfile.AutoSize = true;
            this.LabelProfile.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LabelProfile.Font = new System.Drawing.Font("Noto Sans", 10.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelProfile.Location = new System.Drawing.Point(28, 100);
            this.LabelProfile.Name = "LabelProfile";
            this.LabelProfile.Size = new System.Drawing.Size(91, 24);
            this.LabelProfile.TabIndex = 5;
            this.LabelProfile.Text = "Профиль";
            this.LabelProfile.Click += new System.EventHandler(this.LabelProfile_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Cleaning.Properties.Resources.profile;
            this.pictureBox1.Location = new System.Drawing.Point(41, 30);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(73, 66);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // LabelWelcome
            // 
            this.LabelWelcome.AutoSize = true;
            this.LabelWelcome.Font = new System.Drawing.Font("Noto Sans", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelWelcome.Location = new System.Drawing.Point(183, 39);
            this.LabelWelcome.Name = "LabelWelcome";
            this.LabelWelcome.Size = new System.Drawing.Size(257, 64);
            this.LabelWelcome.TabIndex = 3;
            this.LabelWelcome.Text = "Добро пожаловать,\r\nШарапов Дмитрий!";
            // 
            // PanelContent
            // 
            this.PanelContent.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelContent.Location = new System.Drawing.Point(0, 220);
            this.PanelContent.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PanelContent.Name = "PanelContent";
            this.PanelContent.Size = new System.Drawing.Size(1553, 682);
            this.PanelContent.TabIndex = 4;
            // 
            // guna2BorderlessForm1
            // 
            this.guna2BorderlessForm1.ContainerControl = this;
            this.guna2BorderlessForm1.DockIndicatorTransparencyValue = 1D;
            this.guna2BorderlessForm1.DragStartTransparencyValue = 1D;
            this.guna2BorderlessForm1.TransparentWhileDrag = true;
            // 
            // FormNavigation
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1553, 902);
            this.Controls.Add(this.PanelContent);
            this.Controls.Add(this.PanelTop);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimumSize = new System.Drawing.Size(1553, 902);
            this.Name = "FormNavigation";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormNavigation";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormNavigation_FormClosed);
            this.Load += new System.EventHandler(this.FormNavigation_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PictureAppIcon)).EndInit();
            this.PanelTop.ResumeLayout(false);
            this.PanelTop.PerformLayout();
            this.PanelMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Guna.UI2.WinForms.Guna2ImageButton guna2ImageButton1;
        private Guna.UI2.WinForms.Guna2Button guna2Button1;
        private System.Windows.Forms.Panel PanelTop;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox PictureAppIcon;
        private System.Windows.Forms.Label LanelNameCaption;
        private System.Windows.Forms.Label LabelWelcome;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label LabelProfile;
        private System.Windows.Forms.Panel PanelContent;
        private System.Windows.Forms.Panel PanelMenu;
        private Guna.UI2.WinForms.Guna2Button ButtonClientOrders;
        private Guna.UI2.WinForms.Guna2Button ButtonClientServices;
        private System.Windows.Forms.Panel PanelSplitMenuContent;
        private Guna.UI2.WinForms.Guna2Button ButtonAdminEmployees;
        private Guna.UI2.WinForms.Guna2Button ButtonAdminServices;
        private Guna.UI2.WinForms.Guna2Button ButtonAdminStatisticAndReport;
        private Guna.UI2.WinForms.Guna2ImageButton guna2ImageButton3;
        private Guna.UI2.WinForms.Guna2ImageButton guna2ImageButton2;
        private Guna.UI2.WinForms.Guna2Button ButtonCleanerConfirmOrder;
        private Guna.UI2.WinForms.Guna2Button ButtonOperatorSetCleaner;
        private Guna.UI2.WinForms.Guna2BorderlessForm guna2BorderlessForm1;
    }
}