﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class CardCleanerConfirm : UserControl
    {
        string order, client, number, service, count, cost, datetime, address;
        public CardCleanerConfirm(string order, string client, string number, string service, string count, string cost, string datetime, string comment, string address)
        {
            InitializeComponent();
            ButtonConfirm.Name = order;
           
            TextComment.Text = comment;
            this.order = order;
            this.client = client;
            this.number = number;
            this.service = service;
            this.count = count;
            this.cost = cost;
            this.datetime = datetime;
            this.address = address;
            SetLabelInfo();
        }
        private void SetLabelInfo()
        {
            label1.Text = "Клиент: " + client + "\n" +
              "Номер телефона: " + number + "\n" +
              "Услуга: " + service + "\n" +
              "Количество: " + count + "\n" +
              "Стоимость: " + cost + "\n" +
              "Дата и время: " + datetime.Substring(0, 16) + "\n" +
              "Адрес: " + address;
        }
        

        private void TextComment_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void ButtonConfirm_Click(object sender, EventArgs e)
        {
           
        }

        private void TextCountOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Regex.IsMatch(e.KeyChar.ToString(), @"[0-9]") || (Keys)e.KeyChar == Keys.Back) return;
            e.Handled = true;
        }

        private void reCalc_Click(object sender, EventArgs e)
        {
            if (TextCountOrder.Text != "" || Convert.ToDouble(TextCountOrder.Text) > 0)
            {


                using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                {
                    SqlCommand comm = new SqlCommand($"update Заказы set Количество = {TextCountOrder.Text} where Номер_заказа = {order}", connection);
                    connection.Open();
                    try
                    {
                        comm.ExecuteNonQuery();
                        count = TextCountOrder.Text;
                        DataSet getTotalCost = Connection.GetTable($"select Общая_стоимость from Заказы Where Номер_заказа = {order}");
                        cost = getTotalCost.Tables[0].Rows[0][0].ToString();
                        SetLabelInfo();
                        MessageBox.Show("Перерасчет произведен!", "Уведомление");

                    }
                    catch
                    {
                        MessageBox.Show("Не получилось пересчитать общую стоимость!", "Ошибка");
                    }


                }
            } else
            {
                MessageBox.Show("Для перерасчета необходимо ввести от 1 кв.м", "Предупреждение");
            }
        }
    }
}
