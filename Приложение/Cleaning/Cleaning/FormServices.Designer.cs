﻿
namespace Cleaning
{
    partial class FormServices
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelService = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.ComboBoxCategoryServices = new Guna.UI2.WinForms.Guna2ComboBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.SuspendLayout();
            // 
            // LabelService
            // 
            this.LabelService.AutoSize = true;
            this.LabelService.Font = new System.Drawing.Font("Noto Sans", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.LabelService.Location = new System.Drawing.Point(93, 40);
            this.LabelService.Name = "LabelService";
            this.LabelService.Size = new System.Drawing.Size(144, 45);
            this.LabelService.TabIndex = 0;
            this.LabelService.Text = "Уборка";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(195)))), ((int)(((byte)(73)))));
            this.panel1.Location = new System.Drawing.Point(101, 102);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(65, 3);
            this.panel1.TabIndex = 1;
            // 
            // ComboBoxCategoryServices
            // 
            this.ComboBoxCategoryServices.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ComboBoxCategoryServices.BackColor = System.Drawing.Color.Transparent;
            this.ComboBoxCategoryServices.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(166)))), ((int)(((byte)(166)))), ((int)(((byte)(166)))));
            this.ComboBoxCategoryServices.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.ComboBoxCategoryServices.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.ComboBoxCategoryServices.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.ComboBoxCategoryServices.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.ComboBoxCategoryServices.Font = new System.Drawing.Font("Noto Sans", 10.8F);
            this.ComboBoxCategoryServices.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.ComboBoxCategoryServices.ItemHeight = 30;
            this.ComboBoxCategoryServices.Location = new System.Drawing.Point(839, 69);
            this.ComboBoxCategoryServices.Name = "ComboBoxCategoryServices";
            this.ComboBoxCategoryServices.Size = new System.Drawing.Size(288, 36);
            this.ComboBoxCategoryServices.TabIndex = 0;
            this.ComboBoxCategoryServices.SelectedIndexChanged += new System.EventHandler(this.ComboBoxCategoryServices_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.AutoScroll = true;
            this.panel2.Location = new System.Drawing.Point(90, 152);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(1040, 431);
            this.panel2.TabIndex = 3;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // FormServices
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1220, 623);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.ComboBoxCategoryServices);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.LabelService);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormServices";
            this.Padding = new System.Windows.Forms.Padding(90, 40, 90, 40);
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "FormServices";
            this.Load += new System.EventHandler(this.FormServices_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LabelService;
        private System.Windows.Forms.Panel panel1;
        private Guna.UI2.WinForms.Guna2ComboBox ComboBoxCategoryServices;
        private System.Windows.Forms.Panel panel2;
    }
}