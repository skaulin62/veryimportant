﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class FormCleanerConfirm : Form
    {
        public FormCleanerConfirm(Account account)
        {
            InitializeComponent();
            this.account = account;
            try
            {
                FillCardSetCleaner($"select * from orders_decline where [status] = 'Ожидание выполнения' and id_cleaner = '{account.id}' ");
            } catch
            {
                MessageBox.Show("Данные не были подгружены!", "Ошибка");
            }
           
        }
        Account account;


        private void FillCardSetCleaner(string query)
        {
            DataSet table = Connection.GetTable(query);
            if (table.Tables[0].Rows.Count > 0)
            {


                panel1.Controls.Clear();
                for (int i = 0; i < table.Tables[0].Rows.Count; i++)
                {
                    Panel panel = new Panel() { Height = 20, Dock = DockStyle.Top, BackColor = Color.White };
                    CardCleanerConfirm cardCleanerConfirm = new CardCleanerConfirm(
                        table.Tables[0].Rows[i][0].ToString(),
                        table.Tables[0].Rows[i][2].ToString(),
                        table.Tables[0].Rows[i][14].ToString(),
                        table.Tables[0].Rows[i][3].ToString(),
                        table.Tables[0].Rows[i][5].ToString(),
                        Convert.ToInt32(table.Tables[0].Rows[i][4]).ToString(),
                        table.Tables[0].Rows[i][11].ToString(),
                        table.Tables[0].Rows[i][10].ToString(),
                        table.Tables[0].Rows[i][15].ToString()
                        );
                    Guna.UI2.WinForms.Guna2Button guna2Button = cardCleanerConfirm.Controls.Find(table.Tables[0].Rows[i][0].ToString(), true).FirstOrDefault() as Guna.UI2.WinForms.Guna2Button;
                    guna2Button.Click += new EventHandler(ButtonConfirmOrder);
                    cardCleanerConfirm.Dock = DockStyle.Top;
                    panel1.Controls.Add(panel);
                    panel1.Controls.Add(cardCleanerConfirm);
                }
            } else
            {
                panel1.Controls.Clear();
                EmptyForm emptyForm = new EmptyForm("На данный момент нет заказов");
                emptyForm.Dock = DockStyle.Fill;
                panel1.Controls.Add(emptyForm);
            }
        }

        private void ButtonConfirmOrder(object sender, EventArgs e)
        {
           
                string order = (sender as Guna.UI2.WinForms.Guna2Button).Name;
                DialogResult dialogResult = MessageBox.Show("Уверены, что хотите закончить уборку?", "Подтверждение", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (dialogResult == DialogResult.Yes)
                {
                    using (SqlConnection connection = new SqlConnection(Connection.ConnectionString))
                    {
                    
                        SqlCommand sqlCommand = new SqlCommand($"update Заказы set Дата_подтверждения_уборки = convert(datetime, '{DateTime.Now}', 105) , Статус = 'Ждут отзыв' where Номер_заказа = '{order}'", connection);

                        try
                        {
                            connection.Open();
                            sqlCommand.ExecuteNonQuery();
                            MessageBox.Show("Уборка выполнена!", "Уведомление");
                        FillCardSetCleaner($"select * from orders_decline where [status] = 'Ожидание выполнения' and id_cleaner = '{account.id}' ");
                        }
                        catch
                        {
                            MessageBox.Show("Не получилось подтвердить заказ!", "Ошибка");
                        }
                    }
                }
            

        }
    }
}
