﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Cleaning
{
    public partial class CardServices : UserControl
    {
        public CardServices(string id, string name, string description, string image, string cost, string account)
        {
            InitializeComponent();
            this.account = account;
            if (image == "" || !File.Exists(image)) pictureBox1.Image = Image.FromFile(@"services\IconAppCleaning2.png");
            else pictureBox1.Image = Image.FromFile(image);
            label1.Text = name;
            label2.Text = description;
            label3.Text = cost;
            guna2Button1.Name = id.ToString();
        }
        string account;
        private void CardServices_Resize(object sender, EventArgs e)
        {
            if (this.Width > 1150) {

                this.label2.Font = new System.Drawing.Font("Noto Sans", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
                label2.MaximumSize = new Size(1000, 0);
            }
            if (this.Width < 1150) {
                label2.MaximumSize = new Size(700, 0);
                this.label2.Font = new System.Drawing.Font("Noto Sans", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            }
         
        }
        private void guna2Button1_Click(object sender, EventArgs e)
        {
            FormRegistrationOrder formRegistrationOrder = new FormRegistrationOrder((sender as Guna.UI2.WinForms.Guna2Button).Name, 
                label1.Text, label3.Text.Split(new char[] { ' '})[0], this.account);
            formRegistrationOrder.ShowDialog();
        }
    }
}
