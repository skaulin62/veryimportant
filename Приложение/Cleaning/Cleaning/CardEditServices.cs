﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Guna;

namespace Cleaning
{
    public partial class CardEditServices : UserControl
    {
        public CardEditServices(string id, string name, string category)
        {
            InitializeComponent();
            label1.Text = name;
            label2.Text = category;
            guna2Button1.Name = id;
        }

        private void guna2Button1_Click(object sender, EventArgs e)
        {
            Guna.UI2.WinForms.Guna2Button btn = (Guna.UI2.WinForms.Guna2Button)sender;
            string id = btn.Name.ToString();
     
            FormEditingService formEditingService = new FormEditingService(id);
            formEditingService.ShowDialog();
            

        }
    }
}
